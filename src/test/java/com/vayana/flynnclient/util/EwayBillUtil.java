package com.vayana.flynnclient.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vayana.flynnclient.client.FlynnClient;

import java.text.SimpleDateFormat;
import java.util.Date;

public class EwayBillUtil {
  private static final String pattern = "dd/MM/yyyy";
  public static final SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);

  public static String getEwbRequestBody(
    String docNumber, String senderGstin, String receiverGstin, String transporterId, String vehicleNumber
  ) {
    return String.format("{\n" +
      "  \"actFromStateCode\": 27,\n" +
      "  \"actToStateCode\": 29,\n" +
      "  \"cessNonAdvolValue\": 1.00,\n" +
      "  \"cessValue\": 0.00,\n" +
      "  \"cgstValue\": 0.00,\n" +
      "  \"dispatchFromGSTIN\": \"27AAAPI3182M002\",\n" +
      "  \"dispatchFromTradeName\": \"ABC Traders\",\n" +
      "  \"docDate\": \"%s\",\n" +
      "  \"docNo\": \"%s\",\n" +
      "  \"docType\": \"INV\",\n" +
      "  \"fromAddr1\": \"2ND CROSS NO 59  19  A\",\n" +
      "  \"fromAddr2\": \"GROUND FLOOR OSBORNE ROAD\",\n" +
      "  \"fromGstin\": \"%s\",\n" +
      "  \"fromPincode\": 411016,\n" +
      "  \"fromPlace\": \"FRAZER TOWN\",\n" +
      "  \"fromStateCode\": 27,\n" +
      "  \"fromTrdName\": \"welton\",\n" +
      "  \"igstValue\": 673186.68,\n" +
      "  \"itemList\": [\n" +
      "    {\n" +
      "      \"productName\": \"Wheat\",\n" +
      "      \"productDesc\": \"Wheat\",\n" +
      "      \"hsnCode\": 1001,\n" +
      "      \"quantity\": 4,\n" +
      "      \"qtyUnit\": \"BOX\",\n" +
      "      \"cgstRate\": 0.00,\n" +
      "      \"sgstRate\": 0.00,\n" +
      "      \"igstRate\": 12.00,\n" +
      "      \"cessRate\": 0.00,\n" +
      "      \"taxableAmount\": 5609889.00\n" +
      "    }\n" +
      "  ],\n" +
      "  \"otherValue\": -1.00,\n" +
      "  \"sgstValue\": 0.00,\n" +
      "  \"shipToGSTIN\": \"%s\",\n" +
      "  \"shipToTradeName\": \"XYZ Traders\",\n" +
      "  \"subSupplyDesc\": \"Sub Supply Desc\",\n" +
      "  \"subSupplyType\": \"1\",\n" +
      "  \"supplyType\": \"O\",\n" +
      "  \"toAddr1\": \"Shree Nilaya\",\n" +
      "  \"toAddr2\": \"Dasarahosahalli\",\n" +
      "  \"toGstin\": \"29AAACW4202F1ZM\",\n" +
      "  \"toPincode\": 560008,\n" +
      "  \"toPlace\": \"Beml Nagar\",\n" +
      "  \"toStateCode\": 29,\n" +
      "  \"toTrdName\": \"sthuthya\",\n" +
      "  \"totInvValue\": 11556371.34,\n" +
      "  \"totalValue\": 5609889.00,\n" +
      "  \"transDistance\": \"0\",\n" +
      "  \"transDocDate\": \"%s\",\n" +
      "  \"transDocNo\": \"\",\n" +
      "  \"transMode\": \"1\",\n" +
      "  \"transactionType\": 1,\n" +
      "  \"transporterId\": \"%s\",\n" +
      "  \"transporterName\": \"\",\n" +
      "  \"vehicleNo\": \"%s\",\n" +
      "  \"vehicleType\": \"R\"\n" +
      "}", dateFormat.format(new Date()), docNumber, senderGstin, receiverGstin, "", transporterId, vehicleNumber);
  }

  public static String getEwayBillNumber(FlynnClient client, String createEwbResponse) throws JsonProcessingException {
    ObjectMapper mapper = client.getServices().getMapper();
    String res = mapper.readTree(createEwbResponse).get("data").textValue();
    return mapper.readTree(res).get("ewayBillNo").textValue();
  }

  public static String getEwayBillNumberV3(FlynnClient client, String createEwbResponse) throws JsonProcessingException {
        ObjectMapper mapper = client.getServices().getMapper();
        return mapper.readTree(createEwbResponse).get("data").get("ewayBillNo").textValue();
  }

}
