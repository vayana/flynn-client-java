package com.vayana.flynnclient.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vayana.flynnclient.client.FlynnClient;

public class GstnUtil {

    public static String getDownloadGstrRequest(String fromRetPeriod, String toRetPeriod) {
        return String.format(" {\n" +
                "            \"fromRetPeriod\":\"%s\",\n" +
                "            \"toRetPeriod\":\"%s\",\n" +
                "            \"returns\":{\n" +
                "               \"gstr1\":{\n" +
                "                  \"b2b\":true,\n" +
                "                  \"b2ba\":true,\n" +
                "                  \"b2cl\":true,\n" +
                "                  \"b2cla\":true,\n" +
                "                  \"b2cs\":true,\n" +
                "                  \"b2csa\":true,\n" +
                "                  \"cdnr\":true,\n" +
                "                  \"cdnra\":true,\n" +
                "                  \"cdnur\":true,\n" +
                "                  \"cdnura\":true,\n" +
                "                  \"exp\":true,\n" +
                "                  \"expa\":true,\n" +
                "                  \"at\":true,\n" +
                "                  \"ata\":true,\n" +
                "                  \"txp\":true,\n" +
                "                  \"txpa\":true,\n" +
                "                  \"e_invoices_b2b\":true,\n" +
                "                  \"e_invoices_cdnr\":true,\n" +
                "                  \"e_invoices_cdnur\":true,\n" +
                "                  \"e_invoices_exp\":true,\n" +
                "                  \"nil_rated\":true,\n" +
                "                  \"hsn_summary\":true,\n" +
                "                  \"doc_issued\":true,\n" +
                "                  \"summary\":true\n" +
                "               },\n" +
                "               \"gstr2a\":{\n" +
                "                  \"b2b\":true,\n" +
                "                  \"b2ba\":true,\n" +
                "                  \"cdn\":true,\n" +
                "                  \"cdna\":true,\n" +
                "                  \"isd\":true,\n" +
                "                  \"impg\":true,\n" +
                "                  \"impgsez\":true,\n" +
                "                  \"amdhist\":false\n" +
                "               },\n" +
                "               \"gstr2b\":{\n" +
                "                  \"alldetails\":true\n" +
                "               },\n" +
                "               \"gstr3b\":{\n" +
                "                  \"details\":true\n" +
                "               }\n" +
                "            },\n" +
                "            \"parameters\":{\n" +
                "            }\n" +
                "          }", fromRetPeriod, toRetPeriod);
    }

    public static String getTaskIdV3(FlynnClient client, String response) throws JsonProcessingException {
        ObjectMapper mapper = client.getServices().getMapper();
        return mapper.readTree(response).get("data").get("task-id").textValue();
    }
}
