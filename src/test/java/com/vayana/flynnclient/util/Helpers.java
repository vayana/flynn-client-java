package com.vayana.flynnclient.util;

import com.vayana.flynnclient.Constants;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Helpers {
  public static String getStateCode(String gstin) {
    return gstin.substring(0, 2);
  }

  public static int getPincode(String gstin) {
    switch(getStateCode(gstin)) {
      case "29" : return 560037;
      case "27" : return 400049;
      case "24" : return 382424;
      case "19" : return 722144;
      case "33" : return 608703;
      case "03" : return 143001;
      default : throw new IllegalArgumentException("err-statecode-not-known");
    }
  }

  public static void insertTaskIdInFile(String fileName, String taskId) throws IOException {
    File file = new File(Constants.TRANSIENT_DIR + "/" + fileName);

    FileWriter fr = new FileWriter(file, true);
    fr.write(taskId + "\n");
    fr.close();
  }
}
