package com.vayana.flynnclient.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vayana.flynnclient.client.FlynnClient;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import static com.vayana.flynnclient.util.Helpers.getPincode;
import static com.vayana.flynnclient.util.Helpers.getStateCode;

public class EinvoiceUtil {
  private static final String pattern = "dd/MM/yyyy";
  private static final SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);

  public static String getEnrichedEinvoiceRequestBody(
    String sellerGstin, String buyerGstin, boolean generatePdf
  ) {
    return String.format("{\n" +
        "  \"payload\": [\n" +
        "    {\n" +
        "      \"Version\": \"1.1\",\n" +
        "      \"Irn\": \"\",\n" +
        "      \"TranDtls\": {\n" +
        "        \"TaxSch\": \"GST\",\n" +
        "        \"SupTyp\": \"B2B\",\n" +
        "        \"RegRev\": \"N\",\n" +
        "        \"EcmGstin\": null\n" +
        "      },\n" +
        "      \"DocDtls\": {\n" +
        "        \"Typ\": \"INV\",\n" +
        "        \"No\": \"%s\",\n" +
        "        \"Dt\": \"%s\"\n" +
        "      },\n" +
        "      \"SellerDtls\": {\n" +
        "        \"Gstin\": \"%s\",\n" +
        "        \"LglNm\": \"Acme Widgets Private Limited\",\n" +
        "        \"TrdNm\": null,\n" +
        "        \"Addr1\": \"2345\",\n" +
        "        \"Addr2\": null,\n" +
        "        \"Loc\": \"Maharashtra\",\n" +
        "        \"Pin\": 411016,\n" +
        "        \"Stcd\":\"27\",\n" +
        "        \"Ph\": null,\n" +
        "        \"Em\": null\n" +
        "      },\n" +
        "      \"BuyerDtls\": {\n" +
        "        \"Gstin\": \"%s\",\n" +
        "        \"LglNm\": \"Long Term Enterprises LLP\",\n" +
        "        \"TrdNm\": null,\n" +
        "        \"Pos\": \"27\",\n" +
        "        \"Addr1\": \"1234\",\n" +
        "        \"Addr2\": null,\n" +
        "        \"Loc\": \"Pune\",\n" +
        "        \"Pin\": 411036,\n" +
        "        \"Stcd\":\"27\",\n" +
        "        \"Ph\": null,\n" +
        "        \"Em\": null\n" +
        "      },\n" +
        "      \"ValDtls\": {\n" +
        "        \"AssVal\": 100,\n" +
        "        \"TotInvVal\": 100,\n" +
        "        \"SgstVal\": 0,\n" +
        "        \"CgstVal\": 0,\n" +
        "        \"IgstVal\": 0,\n" +
        "        \"CesVal\": 0,\n" +
        "        \"StCesVal\": 0,\n" +
        "        \"RndOffAmt\": 0\n" +
        "      },\n" +
        "      \"DispDtls\": null,\n" +
        "      \"ShipDtls\": null,\n" +
        "      \"PayDtls\": null,\n" +
        "      \"RefDtls\": null,\n" +
        "      \"ExpDtls\": null,\n" +
        "      \"EwbDtls\": {\n" +
        "    \"TransId\": \"27AAAPI3182M002\",\n" +
        "    \"TransName\": \"Driver\",\n" +
        "    \"TransMode\": \"1\",\n" +
        "    \"Distance\": 5,\n" +
        "    \"TransDocNo\": \"20/22\",\n" +
        "    \"VehNo\": \"KA51ES1122\",\n" +
        "    \"VehType\": \"O\"\n" +
        "  },\n" +
        "      \"AddlDocDtls\": null,\n" +
        "      \"ItemList\": [\n" +
        "        {\n" +
        "        \"ItemNo\": 1,\n" +
        "        \"SlNo\": \"1\",\n" +
        "        \"PrdDesc\": \"Acme product\",\n" +
        "        \"IsServc\": \"N\",\n" +
        "        \"HsnCd\": \"1001\",\n" +
        "        \"UnitPrice\": 100,\n" +
        "        \"TotAmt\": 100,\n" +
        "        \"AssAmt\": 100,\n" +
        "        \"GstRt\": 0,\n" +
        "        \"TotItemVal\": 100,\n" +
        "        \"Qty\": \"1\",\n" +
        "        \"Unit\": \"BAG\",\n" +
        "        \"Discount\": 0,\n" +
        "        \"SgstAmt\": 0,\n" +
        "        \"CgstAmt\": 0,\n" +
        "        \"IgstAmt\": 0 \n" +
        "      }" +
        "      ]\n" +
        "    }\n" +
        "  ],\n" +
        "  \"meta\": {\n" +
        "    \"generatePdf\": \"%s\",\n" +
        "    \"emailRecipientList\": []\n" +
        "  }\n" +
        "}\n",
      "EINV" + UUID.randomUUID().toString().replace("-", "").substring(0, 12).toUpperCase(),
      dateFormat.format(new Date()),
      sellerGstin, buyerGstin, (generatePdf) ? "Y" : "N");
  }

  public static String getSingleEnrichedEinvoiceRequestBody(
    String sellerGstin, String buyerGstin
  ) {
    return String.format("{\n" +
        "  \"payload\": {\n" +
        "    \"Version\": \"1.1\",\n" +
        "    \"Irn\": \"\",\n" +
        "    \"TranDtls\": {\n" +
        "      \"TaxSch\": \"GST\",\n" +
        "      \"SupTyp\": \"B2B\",\n" +
        "      \"RegRev\": \"N\",\n" +
        "      \"IgstOnIntra\": \"N\"\n" +
        "    },\n" +
        "    \"DocDtls\": {\n" +
        "      \"Typ\": \"INV\",\n" +
        "      \"No\": \"%s\",\n" +
        "      \"Dt\": \"%s\"\n" +
        "    },\n" +
        "    \"SellerDtls\": {\n" +
        "      \"Gstin\": \"%s\",\n" +
        "      \"LglNm\": \"Acme Widgets Private Limited\",\n" +
        "      \"Addr1\": \"Long Term Enterprises\",\n" +
        "      \"Loc\": \"Uttar Pradesh\",\n" +
        "      \"Pin\": 400049,\n" +
        "      \"Stcd\": \"27\"\n" +
        "    },\n" +
        "    \"BuyerDtls\": {\n" +
        "      \"Gstin\": \"%s\",\n" +
        "      \"LglNm\": \"Long Term Enterprises LLP\",\n" +
        "      \"Pos\": \"27\",\n" +
        "      \"Addr1\": \"Long Term Enterprises\",\n" +
        "      \"Loc\": \"Pune\",\n" +
        "      \"Pin\": 400049,\n" +
        "      \"Stcd\": \"27\"\n" +
        "    },\n" +
        "    \"ValDtls\": {\n" +
        "      \"AssVal\": 0,\n" +
        "      \"CgstVal\": 0,\n" +
        "      \"SgstVal\": 0,\n" +
        "      \"IgstVal\": 0,\n" +
        "      \"TotInvVal\": 0\n" +
        "    },\n" +
        "    \"DispDtls\": {\n" +
        "      \"Nm\": \"Acme Widgets Private Limited\",\n" +
        "      \"Addr1\": \"112\",\n" +
        "      \"Addr2\": \"Acme Building\",\n" +
        "      \"Loc\": \"ABC\",\n" +
        "      \"Pin\": 400049,\n" +
        "      \"Stcd\": \"27\"\n" +
        "    },\n" +
        "    \"ShipDtls\": {\n" +
        "      \"Gstin\": \"27AAFCV6085L1ZO\",\n" +
        "      \"LglNm\": \"Acme Widgets Private Limited\",\n" +
        "      \"TrdNm\": \"Acme Widgets Private Limited\",\n" +
        "      \"Addr1\": \"112\",\n" +
        "      \"Addr2\": \"Acme Building\",\n" +
        "      \"Loc\": \"ABC\",\n" +
        "      \"Pin\": 400049,\n" +
        "      \"Stcd\": \"27\"\n" +
        "    },\n" +
        "    \"PayDtls\": {\n" +
        "      \"Nm\": \"Acme Widgets Private Limited\",\n" +
        "      \"Mode\": \"Cash\",\n" +
        "      \"PayTerm\": \"100\",\n" +
        "      \"PayInstr\": \"100\",\n" +
        "      \"CrTrn\": \"100\",\n" +
        "      \"DirDr\": \"100\",\n" +
        "      \"CrDay\": 100,\n" +
        "      \"PaymtDue\": 900,\n" +
        "      \"PaidAmt\": 100\n" +
        "    },\n" +
        "    \"RefDtls\": {\n" +
        "      \"InvRm\": \"Remarks\",\n" +
        "      \"DocPerdDtls\": {\n" +
        "        \"InvStDt\": \"23/07/2021\",\n" +
        "        \"InvEndDt\": \"23/07/2021\"\n" +
        "      }\n" +
        "    },\n" +
        "    \"ExpDtls\": {\n" +
        "      \"ShipBNo\": \"shpbill123\",\n" +
        "      \"ShipBDt\": \"23/07/2021\",\n" +
        "      \"Port\": \"INYNL6\",\n" +
        "      \"RefClm\": \"N\",\n" +
        "      \"ForCur\": \"INR\",\n" +
        "      \"CntCode\": \"IN\",\n" +
        "      \"ExpDuty\": 0\n" +
        "    },\n" +
        "    \"EwbDtls\": {\n" +
        "      \"TransId\": \"27AAAPI3182M002\",\n" +
        "      \"TransName\": \"Driver\",\n" +
        "      \"TransMode\": \"1\",\n" +
        "      \"Distance\": \"5\",\n" +
        "      \"TransDocNo\": \"15\",\n" +
        "      \"TransDocDt\": \"23/07/2021 07:50:00 AM\",\n" +
        "      \"VehNo\": \"KA51ES1122\",\n" +
        "      \"VehType\": \"R\"\n" +
        "    },\n" +
        "    \"ItemList\": [\n" +
        "      {\n" +
        "        \"SlNo\": \"1\",\n" +
        "        \"PrdDesc\": \"Acme product\",\n" +
        "        \"IsServc\": \"N\",\n" +
        "        \"HsnCd\": \"33052000\",\n" +
        "        \"UnitPrice\": 1631.36,\n" +
        "        \"TotAmt\": 48940.8,\n" +
        "        \"AssAmt\": 0,\n" +
        "        \"GstRt\": 18,\n" +
        "        \"TotItemVal\": 0,\n" +
        "        \"Qty\": 0,\n" +
        "        \"FreeQty\": 30,\n" +
        "        \"Unit\": \"OTH\",\n" +
        "        \"Discount\": 48940.8,\n" +
        "        \"PreTaxVal\": 0,\n" +
        "        \"SgstAmt\": 0,\n" +
        "        \"CgstAmt\": 0,\n" +
        "        \"IgstAmt\": 0,\n" +
        "        \"CesRt\": 0,\n" +
        "        \"CesAmt\": 0,\n" +
        "        \"StateCesRt\": 0,\n" +
        "        \"StateCesAmt\": 0,\n" +
        "        \"StateCesNonAdvlAmt\": 0,\n" +
        "        \"CesNonAdvlAmt\": 0,\n" +
        "        \"OthChrg\": 0\n" +
        "      }\n" +
        "    ]\n" +
        "  },\n" +
        "  \"meta\": {\n" +
        "    \"email\": {\n" +
        "      \"seller\": {\n" +
        "        \"recipient\": \"vishnu@vayana.com\",\n" +
        "        \"notify-on-success\": true,\n" +
        "        \"notify-on-error\": true,\n" +
        "        \"send-json\": true,\n" +
        "        \"send-pdf\": true\n" +
        "      },\n" +
        "      \"buyer\": {\n" +
        "        \"recipient\": \"shelkeva@gmail.com\",\n" +
        "        \"send-json\": true,\n" +
        "        \"send-pdf\": true\n" +
        "      }\n" +
        "    }\n" +
        "  }\n" +
        "}\n", "EINV" + UUID.randomUUID().toString().replace("-", "").substring(0, 12).toUpperCase(),
      dateFormat.format(new Date()),
      sellerGstin, buyerGstin);
  }

  public static String getBasicEinvoiceRequestBody(
    String sellerGstin, String buyerGstin, boolean generateEwb
  ) {
    return String.format(
      "{\n" +
        "  \"Version\": \"1.1\",\n" +
        "  \"Irn\": \"\",\n" +
        "  \"TranDtls\": {\n" +
        "    \"TaxSch\": \"GST\",\n" +
        "    \"SupTyp\": \"DEXP\",\n" +
        "    \"RegRev\": \"N\",\n" +
        "    \"IgstOnIntra\": \"N\"\n" +
        "  },\n" +
        "  \"DocDtls\": {\n" +
        "    \"Typ\": \"INV\",\n" +
        "    \"No\": \"%s\",\n" +
        "    \"Dt\": \"%s\"\n" +
        "  },\n" +
        "  \"SellerDtls\": {\n" +
        "    \"Gstin\": \"%s\",\n" +
        "    \"LglNm\": \"Acme Widgets Private Limited\",\n" +
        "    \"Addr1\": \"2345\",\n" +
        "    \"Loc\": \"Uttar Pradesh\",\n" +
        "    \"Pin\": %s,\n" +
        "    \"Stcd\": \"%s\"\n" +
        "  },\n" +
        "  \"BuyerDtls\": {\n" +
        "    \"Gstin\": \"%s\",\n" +
        "    \"LglNm\": \"Long Term Enterprises LLP\",\n" +
        "    \"Pos\": \"%s\",\n" +
        "    \"Addr1\": \"1234\",\n" +
        "    \"Loc\": \"Pune\",\n" +
        "    \"Pin\": %s,\n" +
        "    \"Stcd\": \"%s\"\n" +
        "  },\n" +
        "  \"ValDtls\": {\n" +
        "    \"AssVal\": 100,\n" +
        "    \"CgstVal\": 0,\n" +
        "    \"SgstVal\": 0,\n" +
        "    \"IgstVal\": 0,\n" +
        "    \"TotInvVal\": 100\n" +
        "  },\n" +
        "  \"DispDtls\": {\n" +
        "    \"Nm\": \"Acme Widgets Private Limited\",\n" +
        "    \"Addr1\": \"112\",\n" +
        "    \"Addr2\": \"Acme Building\",\n" +
        "    \"Loc\": \"ABC\",\n" +
        "    \"Pin\": 400049,\n" +
        "    \"Stcd\": \"27\"\n" +
        "  },\n" +
        "  \"ShipDtls\": {\n" +
        "    \"Gstin\": \"29AEKPV7203E1Z9\",\n" +
        "    \"LglNm\": \"Acme Widgets Private Limited\",\n" +
        "    \"TrdNm\": \"Acme Widgets Private Limited\",\n" +
        "    \"Addr1\": \"112\",\n" +
        "    \"Addr2\": \"Acme Building\",\n" +
        "    \"Loc\": \"ABC\",\n" +
        "    \"Pin\": 560037,\n" +
        "    \"Stcd\": \"29\"\n" +
        "  },\n" +
        "  \"PayDtls\": {\n" +
        "    \"Nm\": \"Acme Widgets Private Limited\",\n" +
        "    \"Mode\": \"Cash\",\n" +
        "    \"PayTerm\": \"100\",\n" +
        "    \"PayInstr\": \"100\",\n" +
        "    \"CrTrn\": \"100\",\n" +
        "    \"DirDr\": \"100\",\n" +
        "    \"CrDay\": 100,\n" +
        "    \"PaymtDue\": 900,\n" +
        "    \"PaidAmt\": 100\n" +
        "  },\n" +
        "  \"RefDtls\": {\n" +
        "    \"InvRm\": \"Remarks\",\n" +
        "    \"DocPerdDtls\": {\n" +
        "      \"InvStDt\": \"01/10/2020\",\n" +
        "      \"InvEndDt\": \"01/10/2020\"\n" +
        "    }\n" +
        "  },\n" +
        "  \"ExpDtls\": {\n" +
        "    \"ShipBNo\": \"shpbill123\",\n" +
        "    \"ShipBDt\": \"01/10/2020\",\n" +
        "    \"Port\": \"INYNL6\",\n" +
        "    \"RefClm\": \"N\",\n" +
        "    \"ForCur\": \"INR\",\n" +
        "    \"CntCode\": \"IN\",\n" +
        "    \"ExpDuty\": 0\n" +
        "  },\n" +
        ((generateEwb) ? "  \"EwbDtls\": {\n" +
          "    \"TransId\": \"27AAAPI3182M002\",\n" +
          "    \"TransName\": \"Driver\",\n" +
          "    \"TransMode\": \"1\",\n" +
          "    \"Distance\": 0,\n" +
          "    \"TransDocNo\": \"20/22\",\n" +
          "    \"VehNo\": \"KA51ES1122\",\n" +
          "    \"VehType\": \"O\"\n" +
          "  },\n" : "") +
        "  \"ItemList\": [\n" +
        "    {\n" +
        "      \"SlNo\": \"1\",\n" +
        "      \"PrdDesc\": \"Acme product\",\n" +
        "      \"IsServc\": \"N\",\n" +
        "      \"HsnCd\": \"33052000\",\n" +
        "      \"UnitPrice\": 100,\n" +
        "      \"TotAmt\": 100,\n" +
        "      \"AssAmt\": 100,\n" +
        "      \"GstRt\": 0,\n" +
        "      \"TotItemVal\": 100,\n" +
        "      \"Qty\": 1,\n" +
        "      \"Unit\": \"BAG\",\n" +
        "      \"Discount\": 0,\n" +
        "      \"SgstAmt\": 0,\n" +
        "      \"CgstAmt\": 0 \n" +
        "    }\n" +
        "  ]\n" +
        "}",
      "EINV" + UUID.randomUUID().toString().replace("-", "").substring(0, 12).toUpperCase(),
      dateFormat.format(new Date()),
      sellerGstin,
      getPincode(sellerGstin),
      getStateCode(sellerGstin),
      buyerGstin,
      getStateCode(buyerGstin),
      getPincode(buyerGstin),
      getStateCode(buyerGstin)
    );
  }

  public static String getBasicEwbRequestBody(
    String irn, String transId, String transDocNo, String transDocDate
  ) {
    return String.format("{\n" +
      "  \"Irn\": \"%s\",\n" +
      "  \"TransId\": \"%s\",\n" +
      "  \"TransName\": \"Driver\",\n" +
      "  \"TransMode\": \"1\",\n" +
      "  \"Distance\": 0,\n" +
      "  \"TransDocNo\": \"%s\",\n" +
      "  \"TransDocDt\": \"%s\",\n" +
      "  \"VehNo\": \"KA51ES1122\",\n" +
      "  \"VehType\": \"R\"\n" +
      "}", irn, transId, transDocNo, transDocDate);
  }

  public static String getEnrichedEwbRequestBody(
    String irn, String transId, String transDocNo, String transDocDate
  ) {
    return String.format("{\n" +
      "  \"payload\": [\n" +
      "    {\n" +
      "      \"Irn\": \"%s\",\n" +
      "      \"TransId\": \"%s\",\n" +
      "      \"TransName\": \"Driver\",\n" +
      "      \"TransMode\": \"1\",\n" +
      "      \"Distance\": \"5\",\n" +
      "      \"TransDocNo\": \"%s\",\n" +
      "      \"TransDocDt\": \"%s\",\n" +
      "      \"VehNo\": \"KA51ES1122\",\n" +
      "      \"VehType\": \"R\"\n" +
      "    }\n" +
      "  ],\n" +
      "  \"meta\": {\n" +
      "    \"generatePdf\": \"Y\"\n" +
      "  }\n" +
      "}", irn, transId, transDocNo, transDocDate);
  }

  public static String getEinvoiceNumber(FlynnClient client, String createEinvResponse) throws JsonProcessingException {
    ObjectMapper mapper = client.getServices().getMapper();

    String res = mapper.readTree(createEinvResponse).get("data").textValue();

    return mapper.readTree(res).get("Irn").textValue();
  }

 public static String getEinvoiceNumberV3(FlynnClient client, String createEinvResponse) throws JsonProcessingException {
      ObjectMapper mapper = client.getServices().getMapper();
      return mapper.readTree(createEinvResponse).get("data").get("Irn").textValue();
 }

  public static Long getEwbNumber(FlynnClient client, String createEwbResponse) throws JsonProcessingException {
    ObjectMapper mapper = client.getServices().getMapper();

    String res = mapper.readTree(createEwbResponse).get("data").textValue();

    return mapper.readTree(res).get("EwbNo").longValue();
  }

  public static Long getEwbNumberV3(FlynnClient client, String createEwbResponse) throws JsonProcessingException {
      ObjectMapper mapper = client.getServices().getMapper();
      return mapper.readTree(createEwbResponse).get("data").get("EwbNo").longValue();
  }
}

