package com.vayana.flynnclient.tests.versionOne.einvV1;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vayana.flynnclient.context.ClientContext;
import com.vayana.flynnclient.core.TestBase;
import com.vayana.flynnclient.exception.ConfigurationException;
import com.vayana.flynnclient.exception.HttpBadRequestException;
import com.vayana.flynnclient.exception.SerializationException;
import com.vayana.flynnclient.request.*;
import com.vayana.flynnclient.request.urlbuilder.impl.FlynnGetEInvRequestBuilder;
import com.vayana.flynnclient.request.urlbuilder.impl.FlynnPostEInvRequestBuilder;
import com.vayana.flynnclient.types.api.response.FlynnResponse;
import com.vayana.flynnclient.types.api.response.StatusResponse;
import com.vayana.flynnclient.types.api.response.TaskIDResponse;
import com.vayana.flynnclient.types.config.EinvoiceConfig;
import com.vayana.flynnclient.util.EinvoiceUtil;
import com.vayana.flynnclient.util.Helpers;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.IntStream;

import static com.vayana.flynnclient.util.EinvoiceUtil.getBasicEinvoiceRequestBody;
import static com.vayana.flynnclient.util.FileUtil.getProjectRoot;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

/**
 * Test Einvoice unit test the 'Bulk Create Einvoice' functionality of Flynn Api Server.
 * You would need an {@link ClientContext} (which is provided by {@link TestBase} here)
 * <p>
 * For Creating Bulk E-invoices, You would need to call the three functions in this order
 * <p>
 * 1. {@link TestEnrichedEinvoiceV1#createEinvoice(String, FlynnVersion)}
 * Sends a request to create E-invoices. The return of this API is a {@link TaskIDResponse}.
 * 2. {@link TestBase#getStatus(UUID)}
 * Fetch the status of the Long running task. if the task has been completed, we could move with the subsequent api call.
 * 3. {@link TestBase#getDownloadResponse(UUID)}
 * Fetch the zip with the result of the initial create E-invoice call.
 */
public class TestEnrichedEinvoiceV1 extends TestBase {
  private static final Logger LOG = LoggerFactory.getLogger(TestEnrichedEinvoiceV1.class);
  private final EinvoiceConfig einvConfig = client.getClientContext().getEinvoiceConfig().orElseThrow(() -> new ConfigurationException("err-einv-config-missing"));
  private final ObjectMapper mapper = client.getServices().getMapper();

  private static ExecutorService executorService;

  @Before
  public void setup() {
  }

  @BeforeClass
  public static void classSetup() {
    executorService = Executors.newSingleThreadExecutor();
  }

  @AfterClass
  public static void tearDown() {
    if (!executorService.isShutdown())
      executorService.shutdown();
  }

  /**
   * Given an E-invoice request body, fetch the API response for Create E-invoice API
   */
  private String createBasicEinvoice(String body) throws IOException {
    return EntityUtils.toString(
      new FlynnPostEInvRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.ONE_OH)
        .setOperation(FlynnEInvoiceOperations.GenerateEInvoice)
        .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
        .setBody(body)
        .getResponse()
    );
  }

  public FlynnResponse<TaskIDResponse> createEinvoice(String body, FlynnVersion flynnVersion) throws IOException {
    String response = EntityUtils.toString(
      new FlynnPostEInvRequestBuilder(client, FlynnServiceType.ENRICHED, flynnVersion)
        .setOperation(FlynnEInvoiceOperations.GenerateEInvoice)
        .setBody(body)
        .getResponse()
    );

    return client.getServices().getMapper().readValue(response,
      new TypeReference<FlynnResponse<TaskIDResponse>>() {
      });
  }

  public FlynnResponse<TaskIDResponse> createEinvoiceWithNotification(String body) throws IOException {
    String response = EntityUtils.toString(
      new FlynnPostEInvRequestBuilder(client, FlynnServiceType.ENRICHED, FlynnVersion.ONE_OH)
        .setOperation(FlynnEInvoiceOperations.GenerateIrnWithNotification)
        .setBody(body)
        .getResponse()
    );

    return client.getServices().getMapper().readValue(response,
      new TypeReference<FlynnResponse<TaskIDResponse>>() {
      });
  }

  public FlynnResponse<TaskIDResponse> createEinvEwb(String body) throws IOException {
    String response = EntityUtils.toString(
      new FlynnPostEInvRequestBuilder(client, FlynnServiceType.ENRICHED, FlynnVersion.ONE_OH)
        .setOperation(FlynnEInvoiceOperations.GenerateEWB)
        .setBody(body)
        .getResponse()
    );

    return client.getServices().getMapper().readValue(response,
      new TypeReference<FlynnResponse<TaskIDResponse>>() {
      });
  }

  public FlynnResponse<TaskIDResponse> cancelEivoiceBulk(String body) throws IOException {
    String response = EntityUtils.toString(
      new FlynnPostEInvRequestBuilder(client, FlynnServiceType.ENRICHED, FlynnVersion.ONE_OH)
        .setOperation(FlynnEInvoiceOperations.CancelEInvoice)
        .setBody(body)
        .getResponse()
    );

    return client.getServices().getMapper().readValue(response,
      new TypeReference<FlynnResponse<TaskIDResponse>>() {
      });
  }

  @Test(expected = HttpBadRequestException.class)
  public void testCreateEinvoiceError() throws IOException {
    String body = String.format("{\"%s\": \"%s\"}", "invalid", "body");

    createEinvoice(body, FlynnVersion.ONE_OH);
  }

  @Test
  public void testMultipleCreateEinvoiceJobs() {
    int numJobs = 1;

//    String einvRequestBody = getFileContent(Paths.get(CONFIG_DIRECTORY, "test-einv-enriched-request-body.json"));
    String einvRequestBody = EinvoiceUtil.getEnrichedEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", true);

    IntStream.range(0, numJobs)
      .mapToObj(index -> String.format(einvRequestBody, index))
      .parallel()
      .forEach(node -> {
        FlynnResponse<TaskIDResponse> taskResponse = null;
        try {
          taskResponse = createEinvoice(node, FlynnVersion.ONE_OH);
        } catch (IOException e) {
          LOG.error("err-create-enriched-einv-response-failed");
        }
        long startTime = System.nanoTime();

        FlynnResponse<TaskIDResponse> finalTaskResponse = taskResponse;

        Callable<FlynnResponse<StatusResponse>> statusJob = () -> {
          FlynnResponse<StatusResponse> status = getStatus(finalTaskResponse.getData().getRefId());

          do {
            LOG.debug(String.format("There are %d pending jobs", status.getData().getJobs().getPending()));
            LOG.debug("Sleeping for 5 sec");
            Thread.sleep(5000);

            status = getStatus(finalTaskResponse.getData().getRefId());
          } while (status.getData().getJobs().getPending() != 0);

          return status;
        };

        // Future Task, which gives a Completed Status Response
        Future<FlynnResponse<StatusResponse>> statusResponse = executorService.submit(statusJob);

        while (!statusResponse.isDone()) {
          LOG.debug("Job is not completed yet. Sleeping for 500 ms");
          try {
            Thread.sleep(5000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }

          if ((System.nanoTime() - startTime) / 1000000000.0 > 60) {
            LOG.debug("Elapsed time is more than 1 Min. Cancelling the Call.");
            statusResponse.cancel(true);

            fail("Task failed. Time Limit Exceeded.");
          }
        }

        byte[] zipResponse = getDownloadResponse(taskResponse.getData().getRefId());

        assertNotNull(zipResponse);

        try {
          saveFiles(taskResponse.getData().getRefId().toString(), zipResponse);
        } catch (IOException e) {
          e.printStackTrace();
        }
      });
  }

  @Test
  public void testCreateEinvoiceVOneOh() throws IOException, InterruptedException {
    String einvRequestBody = EinvoiceUtil.getEnrichedEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", true);

    FlynnResponse<TaskIDResponse> taskResponse = createEinvoice(einvRequestBody, FlynnVersion.ONE_OH);
    long startTime = System.nanoTime();

    Callable<FlynnResponse<StatusResponse>> statusJob = () -> {
      FlynnResponse<StatusResponse> status = getStatus(taskResponse.getData().getRefId());

      do {
        LOG.debug(String.format("There are %d pending jobs", status.getData().getJobs().getPending()));
        LOG.debug("Sleeping for 5 sec");
        Thread.sleep(5000);

        status = getStatus(taskResponse.getData().getRefId());
      } while (status.getData().getJobs().getPending() != 0);

      return status;
    };

    // Future Task, which gives a Completed Status Response
    Future<FlynnResponse<StatusResponse>> statusResponse = executorService.submit(statusJob);

    while (!statusResponse.isDone()) {
      LOG.debug("Job is not completed yet. Sleeping for 500 ms");
      Thread.sleep(5000);

      if ((System.nanoTime() - startTime) / 1000000000.0 > 3000) {
        LOG.debug("Elapsed time is more than 1 Min. Cancelling the Call.");
        statusResponse.cancel(true);
        fail("Task failed. Time Limit Exceeded.");
      }
    }

    byte[] zipResponse = getDownloadResponse(taskResponse.getData().getRefId());
    assertNotNull(zipResponse);
    saveFiles(taskResponse.getData().getRefId().toString(), zipResponse);
  }

  @Test
  public void testCreateEinvoiceWithNotification() throws IOException, InterruptedException {
    String einvRequestBody = EinvoiceUtil.getSingleEnrichedEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO");
    FlynnResponse<TaskIDResponse> taskResponse = createEinvoiceWithNotification(einvRequestBody);
    long startTime = System.nanoTime();

    Callable<FlynnResponse<StatusResponse>> statusJob = () -> {
      FlynnResponse<StatusResponse> status = getStatus(taskResponse.getData().getRefId());

      do {
        LOG.debug(String.format("There are %d pending jobs", status.getData().getJobs().getPending()));
        LOG.debug("Sleeping for 5 sec");
        Thread.sleep(5000);

        status = getStatus(taskResponse.getData().getRefId());
      } while (status.getData().getJobs().getPending() != 0);

      return status;
    };

    // Future Task, which gives a Completed Status Response
    Future<FlynnResponse<StatusResponse>> statusResponse = executorService.submit(statusJob);

    while (!statusResponse.isDone()) {
      LOG.debug("Job is not completed yet. Sleeping for 500 ms");
      Thread.sleep(5000);

      if ((System.nanoTime() - startTime) / 1000000000.0 > 3000) {
        LOG.debug("Elapsed time is more than 1 Min. Cancelling the Call.");
        statusResponse.cancel(true);

        fail("Task failed. Time Limit Exceeded.");
      }
    }

    byte[] zipResponse = getDownloadResponse(taskResponse.getData().getRefId());
    assertNotNull(zipResponse);
    saveFiles(taskResponse.getData().getRefId().toString(), zipResponse);
  }

  @Test
  public void testGetEinvoiceAsPdf() throws IOException {
    //Generate IRN
    String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", false);
    String createResponse = createBasicEinvoice(einvRequestBody);
    assertNotNull(createResponse);

    // Reading IRN from response
    String response = (String) mapper.readValue(createResponse, FlynnResponse.class).getData();
    String IRN = (String) mapper.readValue(response, HashMap.class).get("Irn");

    Map<PathParams, String> params = new HashMap<>();
    params.put(PathParams.EInvoiceNumber, IRN);

    //Get pdf of Generated IRN
    HttpEntity result = new FlynnGetEInvRequestBuilder(client, FlynnServiceType.ENRICHED)
      .setOperation(FlynnEInvoiceOperations.GetEInvoiceAsPDF)
      .setParam(params)
      .getResponse();

    try {
      InputStream content = result.getContent();

      Paths.get(getProjectRoot(), "transient", "pdfDownload", "einv", IRN).toFile().mkdirs();

      File file = Paths.get(getProjectRoot(), "transient", "pdfDownload", "einv", IRN, "einv-pdf.pdf").toFile();
      FileOutputStream writer = new FileOutputStream(file);

      IOUtils.copy(content, writer);
      writer.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException cause) {
      LOG.error(String.format("err fetching create E-invoice response. %s", cause));
      throw new SerializationException(String.format("create e-invoice response failed. %s", cause.getMessage()), cause);
    }
  }

  @Test
  public void testGetEwbAsPdf() {
    String IRN = "4f06d6dbd5b3bc753f2f41b055e89ab1376ef55226452995d1b315beaf7ec9c2";

    Map<PathParams, String> params = new HashMap<>();
    params.put(PathParams.EInvoiceNumber, IRN);

    HttpEntity result = new FlynnGetEInvRequestBuilder(client, FlynnServiceType.ENRICHED)
      .setOperation(FlynnEInvoiceOperations.GetEwbAsPDF)
      .setParam(params)
      .getResponse();

    try {
      InputStream content = result.getContent();

      Paths.get(getProjectRoot(), "transient", "pdfDownload", "ewb", IRN).toFile().mkdirs();

      File file = Paths.get(getProjectRoot(), "transient", "pdfDownload", "ewb", IRN, "ewb-pdf.pdf").toFile();
      FileOutputStream writer = new FileOutputStream(file);

      IOUtils.copy(content, writer);
      writer.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException cause) {
      LOG.error(String.format("err fetching create E-invoice response. %s", cause));
      throw new SerializationException(String.format("create e-invoice response failed. %s", cause.getMessage()), cause);
    }
  }

  @Test
  public void testGenerateEwbAfterGeneratingIRN() throws IOException, InterruptedException {
    // first generate the IRN using generate IRN api and then run this api
    String irnString = ""; // paste the generated irn
    String ewbRequestBody =
      EinvoiceUtil.getEnrichedEwbRequestBody(irnString, "27AAAPI3182M002", "15", "09/04/2021 07:50:00 PM");
    FlynnResponse<TaskIDResponse> taskResponse = createEinvEwb(ewbRequestBody);
    long startTime = System.nanoTime();

    Callable<FlynnResponse<StatusResponse>> statusJob = () -> {
      FlynnResponse<StatusResponse> status = getStatus(taskResponse.getData().getRefId());

      do {
        LOG.debug(String.format("There are %d pending jobs", status.getData().getJobs().getPending()));
        LOG.debug("Sleeping for 5 sec");
        Thread.sleep(5000);

        status = getStatus(taskResponse.getData().getRefId());
      } while (status.getData().getJobs().getPending() != 0);

      return status;
    };

    // Future Task, which gives a Completed Status Response
    Future<FlynnResponse<StatusResponse>> statusResponse = executorService.submit(statusJob);

    while (!statusResponse.isDone()) {
      LOG.debug("Job is not completed yet. Sleeping for 500 ms");
      Thread.sleep(5000);

      if ((System.nanoTime() - startTime) / 1000000000.0 > 3000) {
        LOG.debug("Elapsed time is more than 1 Min. Cancelling the Call.");
        statusResponse.cancel(true);

        fail("Task failed. Time Limit Exceeded.");
      }
    }

    byte[] zipResponse = getDownloadResponse(taskResponse.getData().getRefId());
    assertNotNull(zipResponse);
    saveFiles(taskResponse.getData().getRefId().toString(), zipResponse);
  }

  @Test
  public void testGenerateIrnWithEWB() throws IOException, InterruptedException {
    String ewbRequestBody =
      EinvoiceUtil.getEnrichedEinvoiceRequestBody(einvConfig.getGstin(), "27AAAPI3182M002", true);
    FlynnResponse<TaskIDResponse> taskResponse = createEinvoice(ewbRequestBody, FlynnVersion.ONE_OH);
    long startTime = System.nanoTime();

    Callable<FlynnResponse<StatusResponse>> statusJob = () -> {
      FlynnResponse<StatusResponse> status = getStatus(taskResponse.getData().getRefId());

      do {
        LOG.debug(String.format("There are %d pending jobs", status.getData().getJobs().getPending()));
        LOG.debug("Sleeping for 5 sec");
        Thread.sleep(5000);

        status = getStatus(taskResponse.getData().getRefId());
      } while (status.getData().getJobs().getPending() != 0);

      return status;
    };

    // Future Task, which gives a Completed Status Response
    Future<FlynnResponse<StatusResponse>> statusResponse = executorService.submit(statusJob);

    while (!statusResponse.isDone()) {
      LOG.debug("Job is not completed yet. Sleeping for 500 ms");
      Thread.sleep(5000);

      if ((System.nanoTime() - startTime) / 1000000000.0 > 3000) {
        LOG.debug("Elapsed time is more than 1 Min. Cancelling the Call.");
        statusResponse.cancel(true);

        fail("Task failed. Time Limit Exceeded.");
      }
    }

    byte[] zipResponse = getDownloadResponse(taskResponse.getData().getRefId());
    assertNotNull(zipResponse);
    saveFiles(taskResponse.getData().getRefId().toString(), zipResponse);
  }

  @Test
  public void testCancelEinvoiceBulk() throws IOException, InterruptedException {
    //Generate IRN
    String eInvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", false);
    String createResponse = createBasicEinvoice(eInvRequestBody);
    assertNotNull(createResponse);

    // Reading IRN from response
    String response = (String) mapper.readValue(createResponse, FlynnResponse.class).getData();
    String IRN = (String) mapper.readValue(response, HashMap.class).get("Irn");
    String cancelEinvRequestBody =
      String.format("{\n" +
        "  \"payload\": [\n" +
        "    {\n" +
        "      \"Irn\": \"%s\",\n" +
        "      \"CnlRsn\": \"1\",\n" +
        "      \"CnlRem\": \"Cancelled the order\"\n" +
        "    }\n" +
        "  ],\n" +
        "  \"meta\": null\n" +
        "}", IRN);

    FlynnResponse<TaskIDResponse> taskResponse = cancelEivoiceBulk(cancelEinvRequestBody);
    long startTime = System.nanoTime();

    Callable<FlynnResponse<StatusResponse>> statusJob = () -> {
      FlynnResponse<StatusResponse> status = getStatus(taskResponse.getData().getRefId());

      do {
        LOG.debug(String.format("There are %d pending jobs", status.getData().getJobs().getPending()));
        LOG.debug("Sleeping for 5 sec");
        Thread.sleep(5000);

        status = getStatus(taskResponse.getData().getRefId());
      } while (status.getData().getJobs().getPending() != 0);

      return status;
    };

    // Future Task, which gives a Completed Status Response
    Future<FlynnResponse<StatusResponse>> statusResponse = executorService.submit(statusJob);

    while (!statusResponse.isDone()) {
      LOG.debug("Job is not completed yet. Sleeping for 500 ms");
      Thread.sleep(5000);

      if ((System.nanoTime() - startTime) / 1000000000.0 > 3000) {
        LOG.debug("Elapsed time is more than 1 Min. Cancelling the Call.");
        statusResponse.cancel(true);

        fail("Task failed. Time Limit Exceeded.");
      }
    }

    String result = getResultResponse(taskResponse.getData().getRefId());
    assertNotNull(result);
    LOG.debug(result);
  }

  @Test
  public void testGetEwbByDateVOneOh() {
    String flynnVersion = FlynnVersion.ONE_OH.getVersion();
    String sampleDateInEncodedFormat = "31%2F12%2F2020";
    String url = String.format("/enriched/einv/%s/nic/ewayapi/by-date/%s", flynnVersion, sampleDateInEncodedFormat);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  public void testGetEwbForTransporterByGstinAndDateVOneOh() {
    String flynnVersion = FlynnVersion.ONE_OH.getVersion();
    String sampleDateInEncodedFormat = "21%2F06%2F2021";
    String gstin = "24AAAPI3182M002";
    String url = String.format("/enriched/einv/%s/nic/ewayapi/for-transporter-by-gstin-and-date/%s/%s", flynnVersion, gstin, sampleDateInEncodedFormat);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  public void testGetEwbForTransporterByDateVOneOh() {
    String flynnVersion = FlynnVersion.ONE_OH.getVersion();
    String sampleDateInEncodedFormat = "31%2F12%2F2020";
    String url = String.format("/enriched/einv/%s/nic/ewayapi/for-transporter-by-date/%s", flynnVersion, sampleDateInEncodedFormat);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  public void testGetEwbForTransporterByStateCodeAndDateVOneOh() {
    String flynnVersion = FlynnVersion.ONE_OH.getVersion();
    String sampleDateInEncodedFormat = "21%2F06%2F2021";
    String stateCode = "24";
    String url = String.format("/enriched/einv/%s/nic/ewayapi/for-transporter-by-statecode-and-date/%s/%s", flynnVersion, stateCode, sampleDateInEncodedFormat);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }


  @Test
  public void testGetEwbByConsignorVOneOh() {
    String flynnVersion = FlynnVersion.ONE_OH.getVersion();
    String docType = "INV";
    String docNumber= "E17EEC39E7AA1471";
    String url = String.format("/enriched/einv/%s/nic/ewayapi/by-consigner/%s/%s", flynnVersion, docType, docNumber);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  public void testGetOtherPartyEwbVOneOh() {

    String flynnVersion = FlynnVersion.ONE_OH.getVersion();
    String date = "21%2F06%2F2021";
    String url = String.format("/enriched/einv/%s/nic/ewayapi/other-party/%s", flynnVersion, date);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }


  @Test
  public void testGetConsolidatedEwbVOneOh() {
    String flynnVersion = FlynnVersion.ONE_OH.getVersion();
    String tripsheetNum = "2010009529";
    String url = String.format("/enriched/einv/%s/nic/ewayapi/consolidated/%s", flynnVersion, tripsheetNum);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  public void testGetErrorListVOneOh() {
    String flynnVersion = FlynnVersion.ONE_OH.getVersion();
    String url = String.format("/enriched/einv/%s/nic/ewayapi/error-list", flynnVersion);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  public void testGetEwbVOneOh() {
    String flynnVersion = FlynnVersion.ONE_OH.getVersion();
    String ewb = "231009839827";
    String url = String.format("/enriched/einv/%s/nic/ewayapi/%s", flynnVersion, ewb);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  public void testGetGstinDetailsVOneOh() {
    String flynnVersion = FlynnVersion.ONE_OH.getVersion();
    String gstin= "05AAACG2140A1ZL";
    String url = String.format("/enriched/einv/%s/nic/ewayapi/gstin-details/%s", flynnVersion, gstin);
    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  @Ignore("216 - For valid HSN api give error")
  public void testGetHsnDetailsVOneOh() {
    String flynnVersion = FlynnVersion.ONE_OH.getVersion();
    String hsn= "01012100";
    String url = String.format("/enriched/einv/%s/nic/ewayapi/hsn-details/%s", flynnVersion, hsn);
    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  @Ignore("328 Could not retrieve transporter details from gstin")
  public void testGetTransporterDetailsVOneOh() {
    String flynnVersion = FlynnVersion.ONE_OH.getVersion();
    String transporterId= "29AAACW6288M1ZH";
    String url = String.format("/enriched/einv/%s/nic/ewayapi/transin-details/%s", flynnVersion, transporterId);
    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  @Ignore("Bulk call")
  public void testBulkEInvoiceGeneration() throws IOException {
    for (int i = 0; i < 40; i++) {
      String einvRequestBody = getBulkEInvoiceReqBody();

      FlynnResponse<TaskIDResponse> taskResponse = createEinvoice(einvRequestBody, FlynnVersion.ONE_OH);
      Helpers.insertTaskIdInFile("bulkEInvoiceGeneration.txt", ""+taskResponse.getData().getRefId());
    }
  }

  private String getBulkEInvoiceReqBody() {
    List<String> reqBodyList = new ArrayList<>();
    for (int i = 0; i < 50; i++) {
      String einvRequestBody = EinvoiceUtil.getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", false);
      reqBodyList.add(einvRequestBody);
    }

    return String.format("{\"payload\":%s}", reqBodyList);
  }
}
