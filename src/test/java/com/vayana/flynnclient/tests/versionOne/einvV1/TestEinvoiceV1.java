package com.vayana.flynnclient.tests.versionOne.einvV1;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.vayana.flynnclient.client.FlynnClient;
import com.vayana.flynnclient.core.TestBase;
import com.vayana.flynnclient.exception.ConfigurationException;
import com.vayana.flynnclient.exception.SerializationException;
import com.vayana.flynnclient.request.urlbuilder.FlynnEInvRequestBuilder;
import com.vayana.flynnclient.types.config.ApiServerConfig;
import com.vayana.flynnclient.types.config.EinvoiceConfig;
import com.vayana.flynnclient.util.EinvoiceUtil;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.vayana.flynnclient.util.EinvoiceUtil.getEinvoiceNumber;
import static com.vayana.flynnclient.util.FileUtil.getProjectRoot;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Test E-invoice APIs without using {@link FlynnEInvRequestBuilder}.
 * The test methods will use {@link FlynnClient} directly to fetch API response.
 */
public class TestEinvoiceV1 extends TestBase {

  private static final Logger LOG = LoggerFactory.getLogger(TestEinvoiceV1.class);

  private final EinvoiceConfig einvConfig = client.getClientContext().getEinvoiceConfig().orElseThrow(() -> new ConfigurationException("err-einv-config-missing"));

  @Before
  public void setup() {
    authenticate();
  }

  private void authenticate() {
    ApiServerConfig apiConfig = client.getClientContext().getApiServerConfig();

    String url = String.format("/basic/einv/%s/nic/eivital/v1.04/auth", apiConfig.getVersion());
    Map<String, String> headers = new HashMap<>();

    try {
      String authenticate = EntityUtils.toString(client.post(url, headers));
      assertEquals("", authenticate);
    } catch (IOException cause) {
      LOG.error(String.format("err authenticating to E-way Bill. %s", cause));
    }
  }

  /**
   * Given an E-invoice request body, fetch the API response for Create E-invoice API
   */
  private String createEinvoice(String body, String flynnVersion) {
    ApiServerConfig apiConfig = client.getClientContext().getApiServerConfig();

    String url = String.format("/basic/einv/%s/nic/eicore/v1.03/Invoice", flynnVersion);

    try {
      return EntityUtils.toString(client.post(url, Collections.emptyMap(), body));
    } catch (IOException cause) {
      LOG.error(String.format("err fetching create E-ewb response. %s", cause));
      throw new SerializationException(String.format("Error fetching create E-ewb response. %s", cause.getMessage()), cause);
    }
  }

  /**
   * Given an E-way bill number, fetch the API response for Get E-way bill details API
   */
  private String getBasicEinvoiceDetails(String ewbNumber) {
    ApiServerConfig apiConfig = client.getClientContext().getApiServerConfig();

    String url = String.format("/basic/einv/%s/nic/eicore/v1.03/Invoice/irn/%s", apiConfig.getVersion(), ewbNumber);

    try {
      return EntityUtils.toString(client.get(url));
    } catch (IOException cause) {
      LOG.error(String.format("err fetching get E-ewb response. %s", cause));
      throw new SerializationException(String.format("Error fetching get E-ewb response. %s", cause.getMessage()), cause);
    }
  }

  @Test
  public void testCreateBasicEinvoice() throws IOException {
    // If want to fetch the request body from a json file
//    String einvRequestBody = getFileContent(Paths.get(CONFIG_DIRECTORY, "test-einv-request-body.json"));

    String einvRequestBody = EinvoiceUtil.getBasicEinvoiceRequestBody(einvConfig.getGstin(), "29AAACW4202F1ZM", false);
    String createResponse = createEinvoice(einvRequestBody, "v1.0");
    assertNotNull(createResponse);
    LOG.info("Got Create EWB response " + createResponse);
    String getResponse = getBasicEinvoiceDetails(getEinvoiceNumber(client, createResponse));
    assertNotNull(getResponse);
    LOG.info("Got Get EWB response " + getResponse);
  }

  @Test
  public void testGetEinvoiceAsPdfVOneOh() throws JsonProcessingException {
    ApiServerConfig apiConfig = client.getClientContext().getApiServerConfig();

    String einvRequestBody = EinvoiceUtil.getBasicEinvoiceRequestBody(einvConfig.getGstin(), "29AAACW4202F1ZM", false);
    String createResponse = createEinvoice(einvRequestBody, "v1.0");
    String irn = getEinvoiceNumber(client, createResponse);
    String url = String.format("/enriched/einv/%s/%s/invoices/download-pdf/" + irn, apiConfig.getVersion(), einvConfig.getProvider());

    Map<String, String> header = new HashMap<>();
    header.put("accept", "application/pdf");
    try {
      HttpEntity response = client.get(url, header);
      InputStream content = response.getContent();
      Paths.get(getProjectRoot(), "transient", "pdfDownload", "einv", irn).toFile().mkdirs();
      File file = Paths.get(getProjectRoot(), "transient", "pdfDownload", "einv", irn, "einv-pdf.pdf").toFile();
      FileOutputStream writer = new FileOutputStream(file);

      IOUtils.copy(content, writer);
      writer.close();
    } catch (IOException cause) {
      LOG.error(String.format("err fetching create E-invoice response. %s", cause));
      throw new SerializationException(String.format("create e-invoice response failed. %s", cause.getMessage()), cause);
    }
  }

  @Test
  public void testAuth() {
    System.out.println("Authentication successful");
  }
}
