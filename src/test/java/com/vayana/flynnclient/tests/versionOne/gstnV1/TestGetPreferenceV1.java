package com.vayana.flynnclient.tests.versionOne.gstnV1;

import com.vayana.flynnclient.core.TestBase;
import com.vayana.flynnclient.request.*;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;


public class TestGetPreferenceV1 extends TestBase {
    private static final Logger LOG = LoggerFactory.getLogger(TestGetPreferenceV1.class);

    @Test
    public void testGetPreferenceVOneOh(){
        String gstin = "33GSPTN0791G1Z5";
        String financialYear = "2021-22";
        String action = "GETPREF";
        String apiVersion = GstnVersion.ONE_OH.getVersion();
        String flynnVersion = FlynnVersion.ONE_OH.getVersion();

        String url = String.format("/basic/gstn/%s/commonapi/%s/returns?gstin=%s&fy=%s&action=%s", flynnVersion, apiVersion, gstin, financialYear, action);

        try {
            LOG.debug(EntityUtils.toString(client.get(url)));
        } catch (IOException ioe) {
            LOG.error("Error : " + ioe.getMessage());
        }
    }
}
