package com.vayana.flynnclient.tests.versionOne.gstnV1;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.vayana.flynnclient.core.TestBase;
import com.vayana.flynnclient.exception.SerializationException;
import com.vayana.flynnclient.request.*;
import com.vayana.flynnclient.request.urlbuilder.impl.FlynnGetGstnRequestBuilder;
import com.vayana.flynnclient.types.api.response.FlynnResponse;
import com.vayana.flynnclient.types.api.response.StatusResponse;
import com.vayana.flynnclient.types.api.response.TaskIDResponse;
import com.vayana.flynnclient.types.config.ApiServerConfig;
import org.apache.http.util.EntityUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;
import static org.junit.Assert.fail;

public class TestSearchTP_V1 extends TestBase {
  private static final Logger LOG = LoggerFactory.getLogger(TestSearchTP_V1.class);

  private static ExecutorService executorService;

  @BeforeClass
  public static void classSetup() {
    executorService = Executors.newSingleThreadExecutor();
  }

  @AfterClass
  public static void tearDown() {
    if(!executorService.isShutdown())
      executorService.shutdown();
  }

  @Test
  public void testSearchTaxPayerVOneOh() throws IOException {
    String gstin = "27AOZPD0347J1ZL";
    Map<PathParams, String> params = new HashMap<>();
    params.put(PathParams.GSTINNumber, gstin);

    String searchTP = EntityUtils.toString(
      new FlynnGetGstnRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.ONE_OH)
        .setApiVersion(GstnVersion.OH_THREE)
        .setOperation(FlynnGstnOperations.SearchTaxPayer)
        .setParam(params)
        .getResponse()
    );
    LOG.debug("Search TP response {}", searchTP);
  }


  @Test
  public void testPanToGstinMapping() throws InterruptedException {
    String pan = "ALGPY3707P";

    FlynnResponse<TaskIDResponse> taskResponse = callPanToGstin(pan);

    long startTime = System.nanoTime();

    Callable<FlynnResponse<StatusResponse>> statusJob = () -> {
      FlynnResponse<StatusResponse> status = getStatus(taskResponse.getData().getRefId());

      do {
        LOG.debug(String.format("There are %d pending jobs", status.getData().getJobs().getPending()));
        LOG.debug("Sleeping for 5 sec");
        Thread.sleep(5000);

        status = getStatus(taskResponse.getData().getRefId());
      } while (status.getData().getJobs().getPending() != 0);

      return status;
    };

    // Future Task, which gives a Completed Status Response
    Future<FlynnResponse<StatusResponse>> statusResponse = executorService.submit(statusJob);

    while (!statusResponse.isDone()) {
      LOG.debug("Job is not completed yet. Sleeping for 500 ms");
      Thread.sleep(5000);

      if ((System.nanoTime() - startTime)/1000000000.0 > 60) {
        LOG.debug("Elapsed time is more than 1 Min. Cancelling the Call.");
        statusResponse.cancel(true);

        fail("Task failed. Time Limit Exceeded.");
      }
    }

    Map<String, String> gstins = getPanToGstinResult(taskResponse.getData().getRefId());
    LOG.debug("Search TP response {}", gstins);
  }

  public FlynnResponse<TaskIDResponse> callPanToGstin(String pan) {
    ApiServerConfig apiConfig = client.getClientContext().getApiServerConfig();
    String url = String.format("/enriched/gstn/%s/search-taxpayers-from-pan/%s", apiConfig.getVersion(), pan);

    Map<String, String> headers = new HashMap<>();
    headers.put("accept", "application/json");

    try {
      String response = EntityUtils.toString(client.get(url, headers));
      return client.getServices().getMapper().readValue(response,
        new TypeReference<FlynnResponse<TaskIDResponse>>() {});
    } catch (IOException cause) {
      LOG.error(String.format("err fetching search TP response. %s", cause));
      throw new SerializationException(String.format("search TP response failed. %s", cause.getMessage()), cause);
    }
  }

  /**
   * Given a ref ID for pan to GSTIN mapping task, this would convert the response to a Map<GSTIN, SearchTPResponseString>
   * @param refId
   *    ref id for task
   * @return
   *    Map of Gstin to SearchTP response
   */
  public Map<String, String> getPanToGstinResult(UUID refId) {
    ApiServerConfig apiServerConfig = client.getClientContext().getApiServerConfig();

    String url = String.format("/enriched/tasks/%s/result/%s", apiServerConfig.getVersion(), refId.toString());

    Map<String, String> headers = new HashMap<>();

    headers.put("accept", "application/json");

    try {
      String res = EntityUtils.toString(client.get(url, headers));

      JsonNode data = client.getServices().getMapper().readTree(res).get("data");

      Iterator<Map.Entry<String, JsonNode>> iter = data.fields();

      Map<String, String> gstinMap = new HashMap<>();

      while (iter.hasNext()) {
        Map.Entry<String, JsonNode> entry = iter.next();

        gstinMap.put(entry.getKey(), entry.getValue().textValue());
      }

      return gstinMap;
    } catch (IOException cause) {
      LOG.error(String.format("err fetching download response. %s", cause));
      throw new SerializationException(String.format("get download response failed. %s", cause.getMessage()), cause);
    }
  }
}
