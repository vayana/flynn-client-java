package com.vayana.flynnclient.tests.versionOne.einvV1;

import com.vayana.flynnclient.core.TestBase;
import com.vayana.flynnclient.exception.ConfigurationException;
import com.vayana.flynnclient.exception.SerializationException;
import com.vayana.flynnclient.request.*;
import com.vayana.flynnclient.request.urlbuilder.impl.FlynnGetEInvRequestBuilder;
import com.vayana.flynnclient.request.urlbuilder.impl.FlynnPostEInvRequestBuilder;
import com.vayana.flynnclient.types.api.request.GenerateIrnReq;
import com.vayana.flynnclient.types.config.EinvoiceConfig;
import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.vayana.flynnclient.request.FlynnVersion.ONE_OH;
import static com.vayana.flynnclient.util.EinvoiceUtil.*;
import static org.junit.Assert.*;

public class TestBasicEinvoiceV1 extends TestBase {

  private static final Logger LOG = LoggerFactory.getLogger(TestBasicEinvoiceV1.class);
  private final EinvoiceConfig einvConfig = client.getClientContext().getEinvoiceConfig().orElseThrow(() -> new ConfigurationException("err-einv-config-missing"));

  @Before
  public void setup() {
    authenticate();
  }

  /**
   * Given an E-invoice authentication request body, fetch the API response for E-invoice Authentication API
   */
  private void authenticate() {

    try {
      // Http Status 200 signifies successful authentication
      String authenticate = EntityUtils.toString(
        new FlynnPostEInvRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.ONE_OH)
          .setOperation(FlynnEInvoiceOperations.Authenticate)
          .setApiVersion(EInvoiceVersion.ONE_OH_FOUR)
          .getResponse()
      );
      assertEquals("", authenticate);
    } catch (IOException cause) {
      LOG.error(String.format("err authenticating to E-way Bill. %s", cause));
    }
  }

  /**
   * Given an E-invoice request body, fetch the API response for Create E-invoice API
   */
  private String createEinvoice(String body, FlynnVersion flynnVersion) throws IOException {
    return EntityUtils.toString(
      new FlynnPostEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
        .setOperation(FlynnEInvoiceOperations.GenerateEInvoice)
        .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
        .setBody(body)
        .getResponse()
    );
  }

  /**
   * Given an E-invoice number, fetch the API response for Get E-invoice details API
   */
  private String getEinvoiceByIrn(String irn, FlynnVersion flynnVersion) {
    Map<PathParams, String> params = new HashMap<>();
    params.put(PathParams.EInvoiceNumber, irn);

    HttpEntity response = new FlynnGetEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
      .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
      .setOperation(FlynnEInvoiceOperations.GetEInvoice)
      .setParam(params)
      .getResponse();

    try {
      return EntityUtils.toString(response);
    } catch (IOException cause) {
      LOG.error(String.format("err fetching get E-ewb response. %s", cause));
      throw new SerializationException(String.format("Error fetching get E-ewb response. %s", cause.getMessage()), cause);
    }
  }

  /**
   * Given an E-invoice number, fetch the API response for Get E-invoice By Doc Details API
   */
  private String getEinvoiceByDocDetails(String docType, String docNumber, String docDate, FlynnVersion flynnVersion ) {

    Map<PathParams, String> params = new LinkedHashMap<>();
    params.put(PathParams.DocType, docType);
    params.put(PathParams.DocNumber, docNumber);
    params.put(PathParams.Date, docDate);

    HttpEntity response = new FlynnGetEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
      .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
      .setOperation(FlynnEInvoiceOperations.GetEInvoiceByDocDetails)
      .setParam(params)
      .getResponse();

    try {
      return EntityUtils.toString(response);
    } catch (IOException cause) {
      LOG.error(String.format("err fetching get E-ewb response. %s", cause));
      throw new SerializationException(String.format("Error fetching get E-ewb response. %s", cause.getMessage()), cause);
    }
  }

  /**
   * Given a Date, fetches the API response for Get Ewb By Date API
   */
  private String getEwbByDate(String date) {
    Map<PathParams, String> params = new HashMap<>();
    params.put(PathParams.Date, date );

    HttpEntity response = new FlynnGetEInvRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.ONE_OH)
      .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
      .setOperation(FlynnEInvoiceOperations.GetEwbByDate)
      .setParam(params)
      .getResponse();

    try {
      return EntityUtils.toString(response);
    } catch (IOException cause) {
      LOG.error(String.format("err fetching get E-ewb response. %s", cause));
      throw new SerializationException(String.format("Error fetching get E-ewb response. %s", cause.getMessage()), cause);
    }
  }

  /**
   * Given a Date, fetches the API response for Get Ewb For Transporter By Date API
   */
  private String getEwbForTransporterByDate(String date) {
    Map<PathParams, String> params = new LinkedHashMap<>();
    params.put(PathParams.Date, date );

    HttpEntity response = new FlynnGetEInvRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.ONE_OH)
      .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
      .setOperation(FlynnEInvoiceOperations.GetEwbForTransporterByDate)
      .setParam(params)
      .getResponse();

    try {
      return EntityUtils.toString(response);
    } catch (IOException cause) {
      LOG.error(String.format("err fetching get E-ewb response. %s", cause));
      throw new SerializationException(String.format("Error fetching get E-ewb response. %s", cause.getMessage()), cause);
    }
  }

  /**
   * Given an Irn, fetches the API response for Get Ewb Details By Irn API
   */
  private String getEwbByIrn(String irn, FlynnVersion flynnVersion ) {
    Map<PathParams, String> params = new HashMap<>();
    params.put(PathParams.EInvoiceNumber, irn );

    HttpEntity response = new FlynnGetEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
      .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
      .setOperation(FlynnEInvoiceOperations.GetEwbDetailsByIrn)
      .setParam(params)
      .getResponse();

    try {
      return EntityUtils.toString(response);
    } catch (IOException cause) {
      LOG.error(String.format("err fetching get E-ewb response. %s", cause));
      throw new SerializationException(String.format("Error fetching get E-ewb response. %s", cause.getMessage()), cause);
    }
  }

  /**
   * Given an E-way bill number, fetch the API response for Get E-way bill details API
   */
  private String generateEwayBill(String body, FlynnVersion flynnVersion) {
    HttpEntity response = new FlynnPostEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
      .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
      .setOperation(FlynnEInvoiceOperations.GenerateEWB)
      .setBody(body)
      .getResponse();

    try {
      return EntityUtils.toString(response);
    } catch (IOException cause) {
      LOG.error(String.format("err fetching get E-ewb response. %s", cause));
      throw new SerializationException(String.format("Error fetching get E-ewb response. %s", cause.getMessage()), cause);
    }
  }

  /**
   * Given an E-way Bill cancel request body, fetch the API response for Cancel E-way Bill API
   */
  private String cancelEwb(String body, FlynnVersion flynnVer) throws IOException {

      FlynnEInvoiceOperations eInvoiceOperation = null;

      switch (flynnVer) {
          case ONE_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.CancelEwbVOneOh;
              break;
          case TWO_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.CancelEwbVTwoOh;
              break;
          case THREE_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.CancelEwbVThreeOh;
              break;
      }

    return EntityUtils.toString(
      new FlynnPostEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVer)
        .setOperation(eInvoiceOperation)
        .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
        .setBody(body)
        .getResponse()
    );
  }

  /**
   * Given an E-way bill number, fetch the API response for Get E-way bill details API
   */
  private String getEwb(String ewbNumber, FlynnVersion flynnVersion) throws IOException {
    return EntityUtils.toString(
      new FlynnGetEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
        .setOperation(FlynnEInvoiceOperations.GetIrpEwbDetails)
        .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
        .setParam(Map.of(PathParams.EwbNumber, ewbNumber))
        .getResponse()
    );
  }

  /**
   * Given an Update part-B request body, fetch the API response for Update part-B API
   */
  private String updatePartB(String body, FlynnVersion flynnVersion) throws IOException {

      FlynnEInvoiceOperations eInvoiceOperation = null;

      switch (flynnVersion) {
          case ONE_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.UpdatePartBvOneOh;
              break;
          case TWO_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.UpdatePartBvTwoOh;
              break;
          case THREE_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.UpdatePartBvThreeOh;
              break;
      }


    return EntityUtils.toString(
      new FlynnPostEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
        .setOperation(eInvoiceOperation)
        .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
        .setBody(body)
        .getResponse()
    );
  }

  /**
   * Given an Extend ewb request body, fetch the API response for Extend ewb API
   */
  private String extendEwayBill(String body, FlynnVersion flynnVersion) throws IOException {

      FlynnEInvoiceOperations eInvoiceOperation = null;

      switch (flynnVersion) {
          case ONE_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.ExtendEwbVOneOh;
              break;
          case TWO_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.ExtendEwbVTwoOh;
              break;
          case THREE_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.ExtendEwbVThreeOh;
              break;
      }


      return EntityUtils.toString(
      new FlynnPostEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
        .setOperation(eInvoiceOperation)
        .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
        .setBody(body)
        .getResponse()
    );
  }


  /**
   * Fetch the API response for E-invoice error list API
   */
  private String getErrorList(FlynnVersion flynnVer) throws IOException {
    FlynnEInvoiceOperations eInvoiceOperation = flynnVer == ONE_OH? FlynnEInvoiceOperations.GetErrorListVOneOh : FlynnEInvoiceOperations.GetErrorListVTwoOh;
    return EntityUtils.toString(
      new FlynnGetEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVer)
        .setOperation(eInvoiceOperation)
        .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
        .getResponse()
    );
  }

  /**
   * Given an E-invoice cancel request body, fetch the API response for Cancel E-invoice API
   */
  private String cancelIrn(String body, FlynnVersion flynnVersion) throws IOException {
    return EntityUtils.toString(
      new FlynnPostEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
        .setOperation(FlynnEInvoiceOperations.CancelEInvoice)
        .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
        .setBody(body)
        .getResponse()
    );
  }

  /**
   * Given a GSTIN number, fetch the API response for Get GSTIN details API
   */
  private String getGstinDetails(String gstin, FlynnVersion flynnVersion) throws IOException {
    Map<PathParams, String> params = new HashMap<>();
    params.put(PathParams.GSTINNumber, gstin);

    return EntityUtils.toString(
      new FlynnGetEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
        .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
        .setOperation(FlynnEInvoiceOperations.GetGstinDetails)
        .setParam(params)
        .getResponse()
    );
  }

  private String syncGstinDetails(String gstin, FlynnVersion flynnVersion) throws IOException {
    return EntityUtils.toString(
      new FlynnGetEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
        .setOperation(FlynnEInvoiceOperations.SyncGstin)
        .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
        .setParam(Map.of(PathParams.GSTINNumber, gstin))
        .getResponse()
    );
  }

  private String syncGstinDetails1OH4(String gstin) throws IOException {
    return EntityUtils.toString(
      new FlynnGetEInvRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.ONE_OH)
        .setOperation(FlynnEInvoiceOperations.SyncGstin)
        .setApiVersion(EInvoiceVersion.ONE_OH_FOUR)
        .setParam(Map.of(PathParams.GSTINNumber, gstin))
        .getResponse()
    );
  }

  @Test
  public void testCreateBasicEInvoiceVOneOh() throws IOException {
      String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", false);
      String createResponse = createEinvoice(einvRequestBody, ONE_OH);
      assertNotNull(createResponse);
      LOG.info("Got Create EInvoice response " + createResponse);
  }

  @Test
  public void testCancelEInvoiceVOneOh() throws IOException {
    String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", false);
    String createEinvResponse = createEinvoice(einvRequestBody, ONE_OH);
    assertNotNull(createEinvResponse);

    String einvNumber = getEinvoiceNumber(client, createEinvResponse);
    String cancelEinvRequestBody = String.format("{\n" +
      "  \"Irn\": \"%s\",\n" +
      "  \"CnlRsn\": \"1\",\n" +
      "  \"CnlRem\": \"Cancelled the order\"\n" +
      "}", einvNumber);

    String cancelResponse = cancelIrn(cancelEinvRequestBody, ONE_OH);
    assertNotNull(cancelResponse);

    LOG.info("Got Cancel EInvoice response " + cancelResponse);
  }


  @Test
  public void testCreateAndGetBasicEinvoiceWithEwbVOneOh() throws IOException {
    String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", true);
    String createResponse = createEinvoice(einvRequestBody, ONE_OH);
    assertNotNull(createResponse);
    LOG.info("Got Create EWB response " + createResponse);

    String getEwbResponse = getEwb(Long.toString(getEwbNumber(client, createResponse)), ONE_OH);
    assertNotNull(getEwbResponse);
    LOG.info("Got Cancel EWB response " + getEwbResponse);
  }

  @Test
  public void testGetEwbByDateVOneOh() throws IOException {
    String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", true);
    GenerateIrnReq req = client.getServices().getMapper().readValue(einvRequestBody, GenerateIrnReq.class);
    String createEinvResponse = createEinvoice(einvRequestBody, ONE_OH);
    assertNotNull(createEinvResponse);

    String date = req.getDocumentDetails().getDate().replace("/", "%2F");
    String getResponse = getEwbByDate(date);

    assertNotNull(getResponse);
    LOG.info("Got Get EInvoice response " + getResponse);

  }


  @Test
  public void testGetEwbForTransporterByDateVOneOh() throws IOException {
    String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", true);
    GenerateIrnReq req = client.getServices().getMapper().readValue(einvRequestBody, GenerateIrnReq.class);
    String createEinvResponse = createEinvoice(einvRequestBody, ONE_OH);
    assertNotNull(createEinvResponse);

    String flynnVersion = ONE_OH.getVersion();
    String apiVersion = EInvoiceVersion.ONE_OH_THREE.getVersion();
    String date = req.getDocumentDetails().getDate().replace("/", "%2F");

    String url = String.format("/basic/einv/%s/nic/%s/ewayapi/for-transporter-by-date/%s", flynnVersion, apiVersion, date);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }


  @Test
  public void testGetEInvoiceByDocDetailsVOneOh() throws IOException {
    String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", false);
    GenerateIrnReq req = client.getServices().getMapper().readValue(einvRequestBody, GenerateIrnReq.class);
    String createEinvResponse = createEinvoice(einvRequestBody, ONE_OH);
    assertNotNull(createEinvResponse);

    String docType = req.getDocumentDetails().getDocType();
    String docNumber = req.getDocumentDetails().getInvoiceNumber();
    String docDate = req.getDocumentDetails().getDate().replace("/", "%2F");
    String getResponse = getEinvoiceByDocDetails(docType,docNumber,docDate, ONE_OH);

    assertNotNull(getResponse);
    LOG.info("Got Get EInvoice response " + getResponse);
  }


  @Test
  public void testGetEwbByIrnVOneOh() throws IOException {

    String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", true);
    String createEinvResponse = createEinvoice(einvRequestBody, ONE_OH);
    assertNotNull(createEinvResponse);
    String eInvoiceNumber = getEinvoiceNumber(client, createEinvResponse);
    String getResponse = getEwbByIrn(eInvoiceNumber, ONE_OH);

    assertNotNull(getResponse);
    LOG.info("Got Get EInvoice response " + getResponse);
  }

  @Test
  public void testGetEInvoiceVOneOh() throws IOException {
    String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", false);
    String createEinvResponse = createEinvoice(einvRequestBody, ONE_OH);
    assertNotNull(createEinvResponse);
    String eInvoiceNumber = getEinvoiceNumber(client, createEinvResponse);
    String getResponse = getEinvoiceByIrn(eInvoiceNumber, ONE_OH);

    assertNotNull(getResponse);
    LOG.info("Got Get EInvoice response " + getResponse);
  }

  @Test
  public void testExtendEwayBillVOneOh() throws IOException {
    String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "29AEKPV7203E1Z9", true);
    String createEinvResponse = createEinvoice(einvRequestBody, ONE_OH);
    assertNotNull(createEinvResponse);
    LOG.info("Got Create EWB response " + createEinvResponse);

    String extendEwbReqBody = String.format("{\n" +
      "  \"ewbNo\" : %s,\n" +
      "  \"vehicleNo\" : \"PVC1239\",\n" +
      "  \"fromPlace\" : \"FRAZER TOWN\",\n" +
      "  \"fromState\" : 27,\n" +
      "  \"remainingDistance\" : 757,\n" +
      "  \"transDocNo\" : \"tr_doc_123\",\n" +
      "  \"transDocDate\" : \"02/05/2018\",\n" +
      "  \"transMode\" : \"1\",\n" +
      "  \"extnRsnCode\" : 1,\n" +
      "  \"extnRemarks\" : \"Flood\",\n" +
      "  \"fromPincode\" : \"413512\",\n" +
      "  \"consignmentStatus\" : \"M\",\n" +
      "  \"transitType\" : \"\"\n" +
      "}", getEwbNumber(client, createEinvResponse));
    try {
      String extendEwayBillResponse = extendEwayBill(extendEwbReqBody, ONE_OH);
      LOG.info("Got Extend Eway Bill response " + extendEwayBillResponse);
    } catch (Exception ex) {
      assertTrue(ex.toString().contains("382"));
    }
  }

  @Test
  public void testCreateGetBasicEinvoiceVOneOh() throws IOException {
    String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", false);
    String createEinvResponse = createEinvoice(einvRequestBody, ONE_OH);
    assertNotNull(createEinvResponse);
    LOG.info("Got Create EWB response " + createEinvResponse);

    String einvoiceNumber = getEinvoiceNumber(client, createEinvResponse);
    String getResponse = getEinvoiceByIrn(einvoiceNumber, ONE_OH);

    assertNotNull(getResponse);
    LOG.info("Got Get EWB response " + getResponse);

  }

  @Test
  public void testGetGstinDetailsVOneOh() throws IOException {
    String gstinResponse = getGstinDetails("24AAAPI3182M002", ONE_OH);
    assertNotNull(gstinResponse);
    LOG.debug("Got Get Gstin Response " + gstinResponse);
  }

  @Test
  public void testGetErrorListVOneOh() throws IOException {
    String errorList = getErrorList(ONE_OH);
    LOG.debug("Error List " + errorList);
  }

  @Test
  public void testGenerateEwbByIrnVOneOh() throws Exception {
    String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", false);
    String createEinvResponse = createEinvoice(einvRequestBody, ONE_OH);
    assertNotNull(createEinvResponse);
    LOG.info("Got Create EWB response " + createEinvResponse);

    String einvoiceNumber = getEinvoiceNumber(client, createEinvResponse);
    String requestBody = getBasicEwbRequestBody(einvoiceNumber, "19BIQPS4654H1ZI", null, null);

    String res = generateEwayBill(requestBody, ONE_OH);
    System.out.println(res);
  }


  @Test
  public void testCancelEwbVOneOh() throws Exception {
    String eInvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", false);
    String createEinvResponse = createEinvoice(eInvRequestBody, ONE_OH);
    assertNotNull(createEinvResponse);
    LOG.info("Got Create EWB response " + createEinvResponse);

    String eInvoiceNumber = getEinvoiceNumber(client, createEinvResponse);
    String requestBody = getBasicEwbRequestBody(eInvoiceNumber, "19BIQPS4654H1ZI", null, null);

    String genrateEwbResponse = generateEwayBill(requestBody, ONE_OH);

    String ewb = String.valueOf(getEwbNumber(client, genrateEwbResponse));
    String cancelEwbRequestBody = String.format("{\"ewbNo\":%s,\"cancelRsnCode\": 3,\"cancelRmrk\":\"Cancelled the order\"}", ewb);
    String cancelEwbResponse = cancelEwb(cancelEwbRequestBody, ONE_OH);

    LOG.info("Got Cancel Ewb response " + cancelEwbResponse);

  }

  @Test
  public void testCreateInvoiceAndUpdatePartBVOneOh() throws IOException {
    String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", true);
    String createEinvResponse = createEinvoice(einvRequestBody, ONE_OH);
    assertNotNull(createEinvResponse);
    LOG.info("Got Create EWB response " + createEinvResponse);

    String einvoiceNumber = String.valueOf(getEwbNumber(client, createEinvResponse));

    String updatedPartB = updatePartB(String.format("{\n" +
      "\"ewbNo\": \"%s\",\n" +
      "\"vehicleNo\": \"PQR1234\",\n" +
      "\"fromPlace\": \"BANGALORE\",\n" +
      "\"fromState\": 29,\n" +
      "\"reasonCode\": \"1\",\n" +
      "\"reasonRem\": \"vehicle broke down\",\n" +
      "\"transMode\": \"1\",\n" +
      "\"vehicleType\": null,\n" +
      "\"transDocDate\": null,\n" +
      "\"transDocNo\": null\n" +
      "}", einvoiceNumber), ONE_OH);

    LOG.info("Got Update Part B response " + updatedPartB);
  }


  @Test
  @Ignore("Api failed from NIC - err-irp-returned-error")
  public void testSyncGstinDetailsVersion1OH4() throws IOException {
    String syncGstinResponse = syncGstinDetails1OH4("33AOZPD0347J2ZR");
    assertNotNull(syncGstinResponse);
    LOG.debug("Got sync Gstin Response " + syncGstinResponse);
  }

  @Test
  @Ignore("Api failed from NIC - err-irp-returned-error")
  public void testSyncGstinDetailsVOneOh() throws IOException {
    String syncGstinResponse = syncGstinDetails("33AOZPD0347J2ZR", ONE_OH);
    assertNotNull(syncGstinResponse);
    LOG.debug("Got sync Gstin Response " + syncGstinResponse);
  }

  @Test
  public void testAuth() {
    System.out.println("Done");
  }
}
