package com.vayana.flynnclient.tests.versionOne.ewbV1;

import com.vayana.flynnclient.core.TestBase;
import com.vayana.flynnclient.exception.ConfigurationException;
import com.vayana.flynnclient.exception.SerializationException;
import com.vayana.flynnclient.request.FlynnEwbOperations;
import com.vayana.flynnclient.request.FlynnServiceType;
import com.vayana.flynnclient.request.FlynnVersion;
import com.vayana.flynnclient.request.PathParams;
import com.vayana.flynnclient.request.urlbuilder.impl.FlynnGetEwbReqBuilder;
import com.vayana.flynnclient.types.config.ApiServerConfig;
import com.vayana.flynnclient.types.config.EwbConfig;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static com.vayana.flynnclient.util.FileUtil.getProjectRoot;

public class TestEnrichedEwbV1 extends TestBase {

  private static final Logger LOG = LoggerFactory.getLogger(TestEnrichedEwbV1.class);

  private final EwbConfig ewbConfig = client.getClientContext().getEwbConfig().orElseThrow(() -> new ConfigurationException("err-ewb-config-missing"));

  @Before
  public void setup(){
  }

  @Test
  public void testGetEwbAsPdfFromBuilderVOneOh(){
    String ewbNumber = "231010025316";

    Map<PathParams, String> params = new HashMap<>();
    params.put(PathParams.EwbNumber, ewbNumber);

    HttpEntity response = new FlynnGetEwbReqBuilder(client, FlynnServiceType.ENRICHED, FlynnVersion.ONE_OH)
                    .setOperation(FlynnEwbOperations.GetEwbAsPdf)
                    .setParam(params)
                    .getResponse();
    try {
      InputStream content = response.getContent();

      Paths.get(getProjectRoot(), "transient", "pdfDownload", "ewb", ewbNumber).toFile().mkdirs();

      File file = Paths.get(getProjectRoot(), "transient", "pdfDownload", "ewb", ewbNumber, "ewb-pdf.pdf").toFile();
      FileOutputStream writer = new FileOutputStream(file);

      IOUtils.copy(content, writer);
      writer.close();
    }catch (IOException cause) {
      LOG.error(String.format("err fetching create E-invoice response. %s", cause));
      throw new SerializationException(String.format("create e-invoice response failed. %s", cause.getMessage()), cause);
    }
  }

  @Test
  public void testGetEwbAsPdfVOneOh() {
    ApiServerConfig apiConfig = client.getClientContext().getApiServerConfig();

    String ewbNumber = "231010025316";

    String url = String.format("/enriched/ewb/%s/eway-bills/download-pdf/%s", apiConfig.getVersion(), ewbNumber);

    try {
      HttpEntity response = client.get(url);

      InputStream content = response.getContent();

      Paths.get(getProjectRoot(), "transient", "pdfDownload", "ewb", ewbNumber).toFile().mkdirs();

      File file = Paths.get(getProjectRoot(), "transient", "pdfDownload", "ewb", ewbNumber, "ewb-pdf.pdf").toFile();
      FileOutputStream writer = new FileOutputStream(file);

      IOUtils.copy(content, writer);
      writer.close();
    } catch (IOException cause) {
      LOG.error(String.format("err fetching create E-invoice response. %s", cause));
      throw new SerializationException(String.format("create e-invoice response failed. %s", cause.getMessage()), cause);
    }
  }

}
