package com.vayana.flynnclient.tests.versionOne.gstnV1;

import com.vayana.flynnclient.core.TestBase;
import com.vayana.flynnclient.request.*;
import com.vayana.flynnclient.request.urlbuilder.impl.FlynnPostGstnRequestBuilder;
import com.vayana.flynnclient.util.DataMapper;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Map;
import java.io.IOException;

import static com.vayana.flynnclient.types.headers.FlynnApiHeaders.GSTIN_HEADER_NAME;

public class TestGstnAuthenticationV1 extends TestBase {
  private static final Logger LOG = LoggerFactory.getLogger(TestGstnAuthenticationV1.class);

  @Test
  public void testGstnAuthenticationApisVOneOh() throws IOException {

    //------------------------Request OTP------------------------

    String gstin = "33AOZPD0347J2ZR";
    String username = "TN_NT2.1228";

    String requestOTPResponse = EntityUtils.toString(
      new FlynnPostGstnRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.ONE_OH)
        .setApiVersion(GstnVersion.OH_TWO)
        .setOperation(FlynnGstnOperations.RequestOTP)
        .setBody(String.format("{\"gstin\":\"%s\",\"username\":\"%s\"}", gstin, username))
        .getResponse()
    );
    LOG.debug("Request OTP response {}", requestOTPResponse);

    //------------------------Auth token------------------------

   String reqId = DataMapper.mapperSlash.readTree(requestOTPResponse).get("data").get("otp-request-id").textValue();
   String otp  = "575757";

    String authTokenResponse = EntityUtils.toString(
      new FlynnPostGstnRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.ONE_OH)
        .setApiVersion(GstnVersion.OH_TWO)
        .setOperation(FlynnGstnOperations.AuthToken)
        .setBody(String.format("{\"otp-request-id\": \"%s\", \"otp\": \"%s\"}", reqId, otp))
        .getResponse()
    );
    LOG.debug("Auth token response {}", authTokenResponse);

    //------------------------Refresh token------------------------

    String sessionId = DataMapper.mapperSlash.readTree(authTokenResponse).get("data").get("gst-session-id").textValue();

    String refreshTokenResponse = EntityUtils.toString(
      new FlynnPostGstnRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.ONE_OH)
        .setApiVersion(GstnVersion.OH_TWO)
        .setOperation(FlynnGstnOperations.RefreshToken)
        .setHeaders(Map.of(GSTIN_HEADER_NAME, gstin))
        .setBody(String.format("{\"gst-session-id\": \"%s\"}", sessionId))
        .getResponse()
    );
    LOG.debug("Refresh token response {}", refreshTokenResponse);

    //------------------------Log Out------------------------

    String logOutResponse = EntityUtils.toString(
      new FlynnPostGstnRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.ONE_OH)
        .setApiVersion(GstnVersion.OH_TWO)
        .setOperation(FlynnGstnOperations.Logout)
        .setHeaders(Map.of(GSTIN_HEADER_NAME, gstin))
        .setBody(String.format("{\"gst-session-id\": \"%s\"}", sessionId))
        .getResponse()
    );
    LOG.debug("Log Out response {}", logOutResponse);
  }

}
