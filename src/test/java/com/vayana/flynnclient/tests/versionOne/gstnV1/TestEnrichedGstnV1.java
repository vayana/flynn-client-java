package com.vayana.flynnclient.tests.versionOne.gstnV1;

import com.vayana.flynnclient.Constants;
import com.vayana.flynnclient.core.TestBase;
import com.vayana.flynnclient.request.FlynnGstnOperations;
import com.vayana.flynnclient.request.FlynnServiceType;
import com.vayana.flynnclient.request.FlynnVersion;
import com.vayana.flynnclient.request.GstnVersion;
import com.vayana.flynnclient.request.urlbuilder.impl.FlynnPostGstnRequestBuilder;
import com.vayana.flynnclient.util.DataMapper;
import com.vayana.flynnclient.util.GstnUtil;
import org.apache.http.util.EntityUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.vayana.flynnclient.types.headers.FlynnApiHeaders.GSTIN_HEADER_NAME;

public class TestEnrichedGstnV1 extends TestBase {
  private static final Logger LOG = LoggerFactory.getLogger(TestEnrichedGstnV1.class);

  private static ExecutorService executorService;

  @BeforeClass
  public static void classSetup() {
    executorService = Executors.newSingleThreadExecutor();
  }

  @AfterClass
  public static void tearDown() {
    if (!executorService.isShutdown())
      executorService.shutdown();
  }

    private void logoutSession(String gstin, String sessionId) throws IOException {
        String logOutResponse = EntityUtils.toString(
                new FlynnPostGstnRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.ONE_OH)
                        .setApiVersion(GstnVersion.OH_TWO)
                        .setOperation(FlynnGstnOperations.Logout)
                        .setHeaders(Map.of(GSTIN_HEADER_NAME, gstin))
                        .setBody(String.format("{\"gst-session-id\": \"%s\"}", sessionId))
                        .getResponse()
        );
        LOG.debug("Log Out response {}", logOutResponse);
    }

    private String callDownloadGstrApi(String downGstrReq, String gstin, FlynnVersion flynnVersion) throws IOException {
        String downGstrRes = EntityUtils.toString(
                new FlynnPostGstnRequestBuilder(client, FlynnServiceType.ENRICHED, flynnVersion)
                        .setOperation(FlynnGstnOperations.DownloadGstr)
                        .setHeaders(Map.of(GSTIN_HEADER_NAME, gstin))
                        .setBody(downGstrReq)
                        .getResponse()
        );
        return downGstrRes;
    }

    private String authToken(String reqId) throws IOException {
        String otp  = "575757";

        String authTokenResponse = EntityUtils.toString(
                new FlynnPostGstnRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.ONE_OH)
                        .setApiVersion(GstnVersion.OH_TWO)
                        .setOperation(FlynnGstnOperations.AuthToken)
                        .setBody(String.format("{\"otp-request-id\": \"%s\", \"otp\": \"%s\"}", reqId, otp))
                        .getResponse()
        );
        LOG.debug("Auth token response {}", authTokenResponse);
        return authTokenResponse;
    }

    private String requestOtp(String gstin, String username) throws IOException {
        String requestOTPResponse = EntityUtils.toString(
                new FlynnPostGstnRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.ONE_OH)
                        .setApiVersion(GstnVersion.OH_TWO)
                        .setOperation(FlynnGstnOperations.RequestOTP)
                        .setBody(String.format("{\"gstin\":\"%s\",\"username\":\"%s\"}", gstin, username))
                        .getResponse()
        );
        LOG.debug("Request OTP response {}", requestOTPResponse);
        return requestOTPResponse;
    }

  @Test
  public void testSearchTaxPayerBulkVOneOh() throws IOException {
    String body = "[\"33GSPTN0791G1Z5\", \"27GSPMH0791G1ZI\", \"03AARFR1409D1ZJ\", \"27GSPMH0792G1ZH\"]";

    String searchTP = EntityUtils.toString(
      new FlynnPostGstnRequestBuilder(client, FlynnServiceType.ENRICHED, FlynnVersion.ONE_OH)
        .setApiVersion(GstnVersion.OH_THREE)
        .setOperation(FlynnGstnOperations.SearchTaxPayerDetailsBulk)
        .setBody(body)
        .getResponse()
    );
    LOG.debug("Search TP response {}", searchTP);
  }


  @Test
  public void testDownloadGstrApisVOneOh() throws IOException, InterruptedException {

    //------------------------Request OTP------------------------

    String gstin = "33GSPTN0791G1Z5";
    String username = "vayana.tn.1";
    String requestOTPResponse = requestOtp(gstin, username);

      //------------------------Auth token------------------------

    String reqId = DataMapper.mapperSlash.readTree(requestOTPResponse).get("data").get("otp-request-id").textValue();
    String authTokenResponse = authToken(reqId);
    String sessionId = DataMapper.mapperSlash.readTree(authTokenResponse).get("data").get("gst-session-id").textValue();

    //------------------Download GSTR v1.0----------------------------

    List<String> responseList=new ArrayList<String>();

    String downGstrReqV1 = "{\n" +
      "        \"fromRetPeriod\": \"012021\",\n" +
      "        \"toRetPeriod\": \"022021\",\n" +
      "        \"returns\": {\n" +
      "          \"gstr1\": null,\n" +
      "          \"gstr2a\": null,\n" +
      "          \"gstr2b\": {\n" +
      "            \"alldetails\": true\n" +
      "          },\n" +
      "          \"gstr3b\": null\n" +
      "        }\n" +
      "      }";

      String downGstrResV1 = callDownloadGstrApi(downGstrReqV1, gstin, FlynnVersion.ONE_OH);
      LOG.debug("Download GSTR V1.0 --> Response {}", downGstrResV1);

      String taskId1 = DataMapper.mapperSlash.readTree(downGstrResV1).get("data").get("task-id").textValue();
      responseList.add(taskId1);

      System.out.println(responseList);

      TimeUnit.SECONDS.sleep(15);


      //-----------------------Page API--------------------------
    for (String taskId: responseList)
    {
      String flynnVersion = FlynnVersion.ONE_OH.getVersion();
      String url = String.format("/enriched/tasks/%s/result/%s/page/1", flynnVersion, taskId);

      try {
        LOG.debug(EntityUtils.toString(client.get(url)));
      } catch (IOException ioe) {
        LOG.error("Error : " + ioe.getMessage());
      }
    }

    //------------------------Log Out------------------------
    logoutSession(gstin, sessionId);
  }

    @Ignore
    @Test
    public void testDownloadGstrApiFor3YearsVOneOh() throws IOException {
        File file = new File(Constants.TRANSIENT_DIR + "/gstrDownload.txt");

        //------------------------Request OTP------------------------
        String gstin = "33AOZPD0347J2ZR";
        String username = "TN_NT2.1228";
        String requestOTPResponse = requestOtp(gstin, username);

        //------------------------Auth token------------------------
        String reqId = DataMapper.mapperSlash.readTree(requestOTPResponse).get("data").get("otp-request-id").textValue();
        String authTokenResponse = authToken(reqId);
        String sessionId = DataMapper.mapperSlash.readTree(authTokenResponse).get("data").get("gst-session-id").textValue();

        //------------------Download GSTR v1.0----------------------------

        List<String> retPeriod = new ArrayList<String>();
        for (int i = 1; i <= 12; i++) {
            String period = String.format("%06d", Integer.parseInt((i + "2020")));
            String period1 = String.format("%06d", Integer.parseInt((i + "2021")));
            String period2 = String.format("%06d", Integer.parseInt((i + "2022")));
            retPeriod.add(period);
            retPeriod.add(period1);
            retPeriod.add(period2);
        }

        for (String ele : retPeriod) {
            String reqBody = GstnUtil.getDownloadGstrRequest(ele, ele);
            String downGstrRes = callDownloadGstrApi(reqBody, gstin, FlynnVersion.ONE_OH);
            LOG.debug("Download GSTR Response {}", downGstrRes);
            String taskId = DataMapper.mapperSlash.readTree(downGstrRes).get("data").get("task-id").textValue();

            FileWriter fr = new FileWriter(file, true);
            fr.write(taskId + "\n");
            fr.close();
        }
    }

}
