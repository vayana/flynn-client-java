package com.vayana.flynnclient.tests;

import com.fasterxml.jackson.core.type.TypeReference;
import com.vayana.flynnclient.core.TestBase;
import com.vayana.flynnclient.types.api.response.FlynnResponse;
import com.vayana.flynnclient.types.api.response.PingWithSecretResponse;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import static com.vayana.flynnclient.Constants.AES_CRYPTO_TRANSFORM;
import static com.vayana.flynnclient.util.CommonUtil.generateRek;
import static com.vayana.flynnclient.util.EncryptionUtil.getDecryptedString;
import static org.junit.Assert.*;

public class TestPing extends TestBase {
  private static final Logger LOG = LoggerFactory.getLogger(TestPing.class);

  @Test
  public void testAuthServerPing() throws IOException {
    String response = client.pingAuthServer();

    assertNotNull(response);
    assertFalse(response.isEmpty());
  }

  @Test
  public void testApiServerPing() throws IOException {
    String response = client.pingServer();

    assertNotNull(response);
    assertFalse(response.isEmpty());
    assertEquals("pong", response);
  }

  @Test
  public void testAuthenticatedServer() throws IOException {
    String response = client.authenticate();

    assertNotNull(response);
    assertFalse(response.isEmpty());
    assertEquals("{\"data\":{\"message\":\"pong\"}}", response);
  }

  @Test
  public void testAuthenticatedPingWithSensitiveData() throws IOException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
    String message = "encrypted-message";

    String rek = generateRek();
    String response = authenticateServerWithSensitiveData(rek, message);
    assertNotNull(response);

    FlynnResponse<PingWithSecretResponse> encodedResponse = client.getServices().getMapper().readValue(response,
      new TypeReference<FlynnResponse<PingWithSecretResponse>>() {
      });

    assertEquals(message, getDecryptedString(encodedResponse.getData().getClientMessage(), rek, AES_CRYPTO_TRANSFORM));
  }
}
