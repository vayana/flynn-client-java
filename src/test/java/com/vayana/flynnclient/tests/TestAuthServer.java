package com.vayana.flynnclient.tests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.vayana.flynnclient.core.TestBase;
import com.vayana.flynnclient.types.auth.response.AuthResponse;
import com.vayana.flynnclient.types.auth.response.AuthToken;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.vayana.flynnclient.util.JwtUtil.verifyAndReturnJwtToken;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TestAuthServer extends TestBase {
  private static final Logger LOG = LoggerFactory.getLogger(TestAuthServer.class);

  @Test
  public void testSendAuthRequest() throws JsonProcessingException {
    AuthResponse authResponse = client.getParsedAuthResponse();

    assertNotNull(authResponse);

    String signedToken = authResponse.getData().getToken();
    LOG.debug("Signed token is " + signedToken);
    AuthToken authToken = verifyAndReturnJwtToken(client, signedToken);

    assertEquals(client.getClientContext().getClientDetails().getOrgUser().getEmail(), authToken.getEmail());
  }
}
