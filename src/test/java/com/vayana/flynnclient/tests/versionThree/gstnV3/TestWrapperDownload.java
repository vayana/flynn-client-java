package com.vayana.flynnclient.tests.versionThree.gstnV3;

import com.vayana.flynnclient.core.TestBase;
import com.vayana.flynnclient.exception.SerializationException;
import com.vayana.flynnclient.request.FlynnGstnOperations;
import com.vayana.flynnclient.request.FlynnServiceType;
import com.vayana.flynnclient.request.FlynnVersion;
import com.vayana.flynnclient.request.GstnVersion;
import com.vayana.flynnclient.request.urlbuilder.impl.FlynnPostGstnRequestBuilder;
import com.vayana.flynnclient.util.DataMapper;
import org.apache.http.util.EntityUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static com.vayana.flynnclient.types.headers.FlynnApiHeaders.GSTIN_HEADER_NAME;

public class TestWrapperDownload extends TestBase {
//    private static final Logger LOG = LoggerFactory.getLogger(TestEnrichedGstnV3.class);

    private static ExecutorService executorService;

    @BeforeClass
    public static void classSetup() {
        executorService = Executors.newSingleThreadExecutor();
    }

    @AfterClass
    public static void tearDown() {
        if (!executorService.isShutdown()) executorService.shutdown();
    }

    private String authToken(String reqId) throws IOException {
        String otp = "575757";

        String authTokenResponse = EntityUtils.toString(new FlynnPostGstnRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.ONE_OH).setApiVersion(GstnVersion.OH_TWO).setOperation(FlynnGstnOperations.AuthToken).setBody(String.format("{\"otp-request-id\": \"%s\", \"otp\": \"%s\"}", reqId, otp)).getResponse());
        System.out.println("Auth token response {}" + authTokenResponse);
        return authTokenResponse;
    }

    private String requestOtp(String gstin, String username) throws IOException {
        String requestOTPResponse = EntityUtils.toString(new FlynnPostGstnRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.ONE_OH).setApiVersion(GstnVersion.OH_TWO).setOperation(FlynnGstnOperations.RequestOTP).setBody(String.format("{\"gstin\":\"%s\",\"username\":\"%s\"}", gstin, username)).getResponse());
        System.out.println("Request OTP response {}" + requestOTPResponse);
        return requestOTPResponse;
    }

    private void logoutSession(String gstin, String sessionId) throws IOException {
        String logOutResponse = EntityUtils.toString(new FlynnPostGstnRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.ONE_OH).setApiVersion(GstnVersion.OH_TWO).setOperation(FlynnGstnOperations.Logout).setHeaders(Map.of(GSTIN_HEADER_NAME, gstin)).setBody(String.format("{\"gst-session-id\": \"%s\"}", sessionId)).getResponse());
        System.out.println("Log Out response {}" + logOutResponse);
    }

    @Test
    public void testGstr1WithWrapperApi() throws IOException {
        //------------------------Request OTP------------------------

        String gstin = "33GSPTN0791G1Z5";
        String username = "vayana.tn.1";
        String requestOTPResponse = requestOtp(gstin, username);

        //------------------------Auth token------------------------

        String reqId = DataMapper.mapperSlash.readTree(requestOTPResponse).get("data").get("otp-request-id").textValue();
        String authTokenResponse = authToken(reqId);
        String sessionId = DataMapper.mapperSlash.readTree(authTokenResponse).get("data").get("gst-session-id").textValue();


        Set<TestData> docTypes = new HashSet<>();
        docTypes.add(new TestData(true, "B2B"));
        docTypes.add(new TestData(true, "B2BA"));
        docTypes.add(new TestData(true, "B2CL"));
        docTypes.add(new TestData(true, "B2CLA"));
        docTypes.add(new TestData(true, "B2CS"));
        docTypes.add(new TestData(true, "B2CSA"));
        docTypes.add(new TestData(true, "CDNR"));
        docTypes.add(new TestData(true, "CDNRA"));
        docTypes.add(new TestData(true, "CDNUR"));
        docTypes.add(new TestData(true, "CDNURA"));
        docTypes.add(new TestData(true, "EXP"));
        docTypes.add(new TestData(true, "EXPA"));
        docTypes.add(new TestData(true, "AT"));
        docTypes.add(new TestData(true, "ATA"));
        docTypes.add(new TestData(true, "TXP"));
        docTypes.add(new TestData(true, "TXPA"));
        docTypes.add(new TestData(true, "EINV&sec=B2B"));
        docTypes.add(new TestData(true, "EINV&sec=CDNR"));
        docTypes.add(new TestData(true, "EINV&sec=CDNUR"));
        docTypes.add(new TestData(true, "EINV&sec=EXP"));
        docTypes.add(new TestData(true, "NIL"));
        docTypes.add(new TestData(true, "VALIDATEHSNSUM"));
        docTypes.add(new TestData(true, "DOCISS"));
        docTypes.add(new TestData(true, "RETSUM"));

        Map<String, String> header = new HashMap<>();
        header.put("X-FLYNN-N-GSTIN", gstin);
        header.put("ret_period", "022022");
        Set<TestData> docTypes1 = docTypes.stream().filter(t -> t.includeInTest).collect(Collectors.toSet());
        for (TestData t : docTypes1) {
            String url = "/basic/gstn/v1.0/taxpayerapi/v3.0/returns?gstin=33GSPTN0791G1Z5&ret_period=022022&action=" + t.docType + "&gstn_ret_type=gstr1";

            try {
                System.out.println(EntityUtils.toString(client.get(url, header)));
            } catch (Exception cause) {
                System.out.println(String.format("err fetching get Gstr response. %s", cause));
            }
        }
        //------------------------Log Out------------------------
        logoutSession(gstin, sessionId);
    }
}

class TestData {
    boolean includeInTest;
    String docType;

    public TestData(boolean includeInTest, String docType) {
        this.includeInTest = includeInTest;
        this.docType = docType;
    }

    public boolean isIncludeInTest() {
        return includeInTest;
    }

    public void setIncludeInTest(boolean includeInTest) {
        this.includeInTest = includeInTest;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }
}
