package com.vayana.flynnclient.tests.versionThree.gstnV3;

import com.vayana.flynnclient.core.TestBase;
import com.vayana.flynnclient.request.*;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class TestUnregisteredApplicantsAPIV1 extends TestBase {
  private static final Logger LOG = LoggerFactory.getLogger(TestUnregisteredApplicantsAPIV1.class);

  @Test
  public void testUnregisteredApplicantsAPIVOneOh(){
        String uid = "072200000020ESF";
        String action = "TP";
        String apiVersion = GstnVersion.ONE_OH.getVersion();
        String flynnVersion = FlynnVersion.ONE_OH.getVersion();
        String url = String.format("/basic/gstn/%s/commonapi/%s/unregistered-applicants?action=%s&uid=%s", flynnVersion, apiVersion, action, uid);

      Map<String, String> headers = new HashMap<>();
      headers.put("X-FLYNN-N-GSTIN", "33GSPTN0792G1Z4");

        try {
            LOG.debug(EntityUtils.toString(client.get(url, headers)));
        } catch (IOException ioe) {
            LOG.error("Error : " + ioe.getMessage());
        }
  }
}
