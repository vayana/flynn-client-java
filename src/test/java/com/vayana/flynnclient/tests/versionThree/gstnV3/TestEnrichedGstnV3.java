package com.vayana.flynnclient.tests.versionThree.gstnV3;

import com.vayana.flynnclient.core.TestBase;
import com.vayana.flynnclient.request.FlynnGstnOperations;
import com.vayana.flynnclient.request.FlynnServiceType;
import com.vayana.flynnclient.request.FlynnVersion;
import com.vayana.flynnclient.request.GstnVersion;
import com.vayana.flynnclient.request.urlbuilder.impl.FlynnPostGstnRequestBuilder;
import com.vayana.flynnclient.util.DataMapper;
import org.apache.http.util.EntityUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.vayana.flynnclient.types.headers.FlynnApiHeaders.GSTIN_HEADER_NAME;
import static com.vayana.flynnclient.util.GstnUtil.getTaskIdV3;

public class TestEnrichedGstnV3 extends TestBase {
  private static final Logger LOG = LoggerFactory.getLogger(TestEnrichedGstnV3.class);

  private static ExecutorService executorService;

  @BeforeClass
  public static void classSetup() {
    executorService = Executors.newSingleThreadExecutor();
  }

  @AfterClass
  public static void tearDown() {
    if (!executorService.isShutdown())
      executorService.shutdown();
  }

    private void logoutSession(String gstin, String sessionId) throws IOException {
        String logOutResponse = EntityUtils.toString(
                new FlynnPostGstnRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.ONE_OH)
                        .setApiVersion(GstnVersion.OH_TWO)
                        .setOperation(FlynnGstnOperations.Logout)
                        .setHeaders(Map.of(GSTIN_HEADER_NAME, gstin))
                        .setBody(String.format("{\"gst-session-id\": \"%s\"}", sessionId))
                        .getResponse()
        );
        LOG.debug("Log Out response {}", logOutResponse);
    }

    private String callDownloadGstrApi(String downGstrReq, String gstin, FlynnVersion flynnVersion) throws IOException {
        String downGstrRes = EntityUtils.toString(
                new FlynnPostGstnRequestBuilder(client, FlynnServiceType.ENRICHED, flynnVersion)
                        .setOperation(FlynnGstnOperations.DownloadGstr)
                        .setHeaders(Map.of(GSTIN_HEADER_NAME, gstin))
                        .setBody(downGstrReq)
                        .getResponse()
        );
        return downGstrRes;
    }

    private String authToken(String reqId) throws IOException {
        String otp  = "575757";

        String authTokenResponse = EntityUtils.toString(
                new FlynnPostGstnRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.ONE_OH)
                        .setApiVersion(GstnVersion.OH_TWO)
                        .setOperation(FlynnGstnOperations.AuthToken)
                        .setBody(String.format("{\"otp-request-id\": \"%s\", \"otp\": \"%s\"}", reqId, otp))
                        .getResponse()
        );
        LOG.debug("Auth token response {}", authTokenResponse);
        return authTokenResponse;
    }

    private String requestOtp(String gstin, String username) throws IOException {
        String requestOTPResponse = EntityUtils.toString(
                new FlynnPostGstnRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.ONE_OH)
                        .setApiVersion(GstnVersion.OH_TWO)
                        .setOperation(FlynnGstnOperations.RequestOTP)
                        .setBody(String.format("{\"gstin\":\"%s\",\"username\":\"%s\"}", gstin, username))
                        .getResponse()
        );
        LOG.debug("Request OTP response {}", requestOTPResponse);
        return requestOTPResponse;
    }

  @Test
  public void testDownloadGstrApisVThreeOh() throws IOException, InterruptedException {

    //------------------------Request OTP------------------------

    String gstin = "33GSPTN0791G1Z5";
    String username = "vayana.tn.1";
    String requestOTPResponse = requestOtp(gstin, username);

      //------------------------Auth token------------------------

    String reqId = DataMapper.mapperSlash.readTree(requestOTPResponse).get("data").get("otp-request-id").textValue();
    String authTokenResponse = authToken(reqId);
    String sessionId = DataMapper.mapperSlash.readTree(authTokenResponse).get("data").get("gst-session-id").textValue();

    List<String> responseList=new ArrayList<String>();

    //-----------------Download GSTR Bulk v3.0-------------------

    String downGstrBulkReqV3 = "[{\n" +
              "            \"fromRetPeriod\":\"012019\",\n" +
              "            \"toRetPeriod\":\"012019\",\n" +
              "            \"returns\":{\n" +
              "               \"gstr1\": null,\n" +
              "               \"gstr2a\":{\n" +
              "                  \"amdhist\":true\n" +
              "               },\n" +
              "               \"gstr2b\": null,\n" +
              "               \"gstr3b\": null\n" +
              "            },\n" +
              "            \"parameters\":{\n" +
              "               \"gstr2a\":{\n" +
              "                  \"amdhist\":{\n" +
              "                     \"mandatoryParameters\":{\n" +
              "                        \"portCode\":\"INBLR4\",\n" +
              "                        \"billOfEntryNumber\":\"8108363\",\n" +
              "                        \"dateOfBill\":\"01-04-2020\"\n" +
              "                     }\n" +
              "                  }\n" +
              "               }\n" +
              "            }\n" +
              "          },\n" +
              "          {\n" +
              "              \"fromRetPeriod\": \"012021\",\n" +
              "              \"toRetPeriod\": \"022021\",\n" +
              "              \"returns\": {\n" +
              "                \"gstr1\": null,\n" +
              "                \"gstr2a\": null,\n" +
              "                \"gstr2b\": {\n" +
              "                  \"alldetails\": true\n" +
              "                },\n" +
              "                \"gstr3b\": null\n" +
              "              }\n" +
              "            }\n" +
              "            ]";

      String downGstrBulkResV3 = callDownloadGstrApi(downGstrBulkReqV3, gstin, FlynnVersion.THREE_OH);
      LOG.debug("Download GSTR Bulk V3.0 --> Response {}", downGstrBulkResV3);

      String taskId3 = getTaskIdV3(client, downGstrBulkResV3 );

      responseList.add(taskId3);
      System.out.println(responseList);

      TimeUnit.SECONDS.sleep(60);


      //-----------------------Page API--------------------------
    for (String taskId: responseList)
    {
      String flynnVersion = FlynnVersion.ONE_OH.getVersion();
      String url = String.format("/enriched/tasks/%s/result/%s/page/1", flynnVersion, taskId);

      try {
        LOG.debug(EntityUtils.toString(client.get(url)));
      } catch (IOException ioe) {
        LOG.error("Error : " + ioe.getMessage());
      }
    }

    //------------------------Log Out------------------------
    logoutSession(gstin, sessionId);
  }

}
