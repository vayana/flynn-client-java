package com.vayana.flynnclient.tests.versionThree.einvV3;

import com.vayana.flynnclient.core.TestBase;
import com.vayana.flynnclient.exception.ConfigurationException;
import com.vayana.flynnclient.exception.SerializationException;
import com.vayana.flynnclient.request.*;
import com.vayana.flynnclient.request.urlbuilder.impl.FlynnGetEInvRequestBuilder;
import com.vayana.flynnclient.request.urlbuilder.impl.FlynnPostEInvRequestBuilder;
import com.vayana.flynnclient.types.api.request.GenerateIrnReq;
import com.vayana.flynnclient.types.config.EinvoiceConfig;
import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.vayana.flynnclient.request.FlynnVersion.*;
import static com.vayana.flynnclient.util.EinvoiceUtil.*;
import static org.junit.Assert.*;

public class TestBasicEinvoiceV3 extends TestBase {

  private static final Logger LOG = LoggerFactory.getLogger(TestBasicEinvoiceV3.class);
  private final EinvoiceConfig einvConfig = client.getClientContext().getEinvoiceConfig().orElseThrow(() -> new ConfigurationException("err-einv-config-missing"));

  @Before
  public void setup() {
    authenticate();
  }

  /**
   * Given an E-invoice authentication request body, fetch the API response for E-invoice Authentication API
   */
  private void authenticate() {

    try {
      // Http Status 200 signifies successful authentication
      String authenticate = EntityUtils.toString(
        new FlynnPostEInvRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.ONE_OH)
          .setOperation(FlynnEInvoiceOperations.Authenticate)
          .setApiVersion(EInvoiceVersion.ONE_OH_FOUR)
          .getResponse()
      );
      assertEquals("", authenticate);
    } catch (IOException cause) {
      LOG.error(String.format("err authenticating to E-way Bill. %s", cause));
    }
  }

  /**
   * Given an E-invoice request body, fetch the API response for Create E-invoice API
   */
  private String createEinvoice(String body, FlynnVersion flynnVersion) throws IOException {
    return EntityUtils.toString(
      new FlynnPostEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
        .setOperation(FlynnEInvoiceOperations.GenerateEInvoice)
        .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
        .setBody(body)
        .getResponse()
    );
  }

  /**
   * Given an E-invoice number, fetch the API response for Get E-invoice details API
   */
  private String getEinvoiceByIrn(String irn, FlynnVersion flynnVersion) {
    Map<PathParams, String> params = new HashMap<>();
    params.put(PathParams.EInvoiceNumber, irn);

    HttpEntity response = new FlynnGetEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
      .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
      .setOperation(FlynnEInvoiceOperations.GetEInvoice)
      .setParam(params)
      .getResponse();

    try {
      return EntityUtils.toString(response);
    } catch (IOException cause) {
      LOG.error(String.format("err fetching get E-ewb response. %s", cause));
      throw new SerializationException(String.format("Error fetching get E-ewb response. %s", cause.getMessage()), cause);
    }
  }

  /**
   * Given an Irn, fetches the API response for Get Ewb Details By Irn API
   */
  private String getEwbByIrn(String irn, FlynnVersion flynnVersion ) {
    Map<PathParams, String> params = new HashMap<>();
    params.put(PathParams.EInvoiceNumber, irn );

    HttpEntity response = new FlynnGetEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
      .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
      .setOperation(FlynnEInvoiceOperations.GetEwbDetailsByIrn)
      .setParam(params)
      .getResponse();

    try {
      return EntityUtils.toString(response);
    } catch (IOException cause) {
      LOG.error(String.format("err fetching get E-ewb response. %s", cause));
      throw new SerializationException(String.format("Error fetching get E-ewb response. %s", cause.getMessage()), cause);
    }
  }

  /**
   * Given an E-way bill number, fetch the API response for Get E-way bill details API
   */
  private String generateEwayBill(String body, FlynnVersion flynnVersion) {
    HttpEntity response = new FlynnPostEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
      .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
      .setOperation(FlynnEInvoiceOperations.GenerateEWB)
      .setBody(body)
      .getResponse();

    try {
      return EntityUtils.toString(response);
    } catch (IOException cause) {
      LOG.error(String.format("err fetching get E-ewb response. %s", cause));
      throw new SerializationException(String.format("Error fetching get E-ewb response. %s", cause.getMessage()), cause);
    }
  }

  /**
   * Given an E-way Bill cancel request body, fetch the API response for Cancel E-way Bill API
   */
  private String cancelEwb(String body, FlynnVersion flynnVer) throws IOException {

      FlynnEInvoiceOperations eInvoiceOperation = null;

      switch (flynnVer) {
          case ONE_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.CancelEwbVOneOh;
              break;
          case TWO_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.CancelEwbVTwoOh;
              break;
          case THREE_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.CancelEwbVThreeOh;
              break;
      }

    return EntityUtils.toString(
      new FlynnPostEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVer)
        .setOperation(eInvoiceOperation)
        .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
        .setBody(body)
        .getResponse()
    );
  }

  /**
   * Given an E-way bill number, fetch the API response for Get E-way bill details API
   */
  private String getEwb(String ewbNumber, FlynnVersion flynnVersion) throws IOException {
    return EntityUtils.toString(
      new FlynnGetEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
        .setOperation(FlynnEInvoiceOperations.GetIrpEwbDetails)
        .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
        .setParam(Map.of(PathParams.EwbNumber, ewbNumber))
        .getResponse()
    );
  }

  /**
   * Given an Update part-B request body, fetch the API response for Update part-B API
   */
  private String updatePartB(String body, FlynnVersion flynnVersion) throws IOException {

      FlynnEInvoiceOperations eInvoiceOperation = null;

      switch (flynnVersion) {
          case ONE_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.UpdatePartBvOneOh;
              break;
          case TWO_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.UpdatePartBvTwoOh;
              break;
          case THREE_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.UpdatePartBvThreeOh;
              break;
      }


    return EntityUtils.toString(
      new FlynnPostEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
        .setOperation(eInvoiceOperation)
        .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
        .setBody(body)
        .getResponse()
    );
  }

  /**
   * Given an Extend ewb request body, fetch the API response for Extend ewb API
   */
  private String extendEwayBill(String body, FlynnVersion flynnVersion) throws IOException {

      FlynnEInvoiceOperations eInvoiceOperation = null;

      switch (flynnVersion) {
          case ONE_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.ExtendEwbVOneOh;
              break;
          case TWO_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.ExtendEwbVTwoOh;
              break;
          case THREE_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.ExtendEwbVThreeOh;
              break;
      }


      return EntityUtils.toString(
      new FlynnPostEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
        .setOperation(eInvoiceOperation)
        .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
        .setBody(body)
        .getResponse()
    );
  }


  /**
   * Fetch the API response for E-invoice error list API
   */
  private String getErrorList(FlynnVersion flynnVer) throws IOException {
    FlynnEInvoiceOperations eInvoiceOperation = flynnVer == ONE_OH? FlynnEInvoiceOperations.GetErrorListVOneOh : FlynnEInvoiceOperations.GetErrorListVTwoOh;
    return EntityUtils.toString(
      new FlynnGetEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVer)
        .setOperation(eInvoiceOperation)
        .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
        .getResponse()
    );
  }

  /**
   * Given an E-invoice cancel request body, fetch the API response for Cancel E-invoice API
   */
  private String cancelIrn(String body, FlynnVersion flynnVersion) throws IOException {
    return EntityUtils.toString(
      new FlynnPostEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
        .setOperation(FlynnEInvoiceOperations.CancelEInvoice)
        .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
        .setBody(body)
        .getResponse()
    );
  }

  /**
   * Given a GSTIN number, fetch the API response for Get GSTIN details API
   */
  private String getGstinDetails(String gstin, FlynnVersion flynnVersion) throws IOException {
    Map<PathParams, String> params = new HashMap<>();
    params.put(PathParams.GSTINNumber, gstin);

    return EntityUtils.toString(
      new FlynnGetEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
        .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
        .setOperation(FlynnEInvoiceOperations.GetGstinDetails)
        .setParam(params)
        .getResponse()
    );
  }

  @Test
  public void testCreateBasicEInvoiceVThreeOh() throws IOException {
      String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", false);
      String createResponse = createEinvoice(einvRequestBody, THREE_OH);
      assertNotNull(createResponse);
      LOG.info("Got Create EInvoice response " + createResponse);
  }

  @Test
  public void testCancelEInvoiceVThreeOh() throws IOException {
    String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", false);
    String createEinvResponse = createEinvoice(einvRequestBody, THREE_OH);
    assertNotNull(createEinvResponse);

    String einvNumber = getEinvoiceNumberV3(client, createEinvResponse);
    String cancelEinvRequestBody = String.format("{\n" +
            "  \"Irn\": \"%s\",\n" +
            "  \"CnlRsn\": \"1\",\n" +
            "  \"CnlRem\": \"Cancelled the order\"\n" +
            "}", einvNumber);

    String cancelResponse = cancelIrn(cancelEinvRequestBody, THREE_OH);
    assertNotNull(cancelResponse);

    LOG.info("Got Cancel EInvoice response " + cancelResponse);
  }

  @Test
  public void testCreateAndGetBasicEinvoiceWithEwbVThreeOh() throws IOException {
        String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", true);
        String createResponse = createEinvoice(einvRequestBody, THREE_OH);
        assertNotNull(createResponse);
        LOG.info("Got Create EWB response " + createResponse);

        String ewbNo = Long.toString(getEwbNumberV3(client, createResponse));

        String url = String.format("/basic/einv/%s/nic/v1.03/ewayapi/GetEwayBill?ewbNo=%s", THREE_OH.getVersion(), ewbNo);

        try {
            LOG.debug(EntityUtils.toString(client.get(url)));
        } catch (IOException ioe) {
            LOG.error("Error : " + ioe.getMessage());
        }
  }

  @Test
  public void testGetEWBByDateVThreeOh() throws IOException {

     String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", true);
     GenerateIrnReq req = client.getServices().getMapper().readValue(einvRequestBody, GenerateIrnReq.class);
     String createEinvResponse = createEinvoice(einvRequestBody, THREE_OH);
     assertNotNull(createEinvResponse);

     String flynnVersion = THREE_OH.getVersion();
     String apiVersion = EInvoiceVersion.ONE_OH_THREE.getVersion();
     String date = req.getDocumentDetails().getDate();

     String url = String.format("/basic/einv/%s/nic/%s/ewayapi/GetEwayBillsByDate?date=%s", flynnVersion, apiVersion, date);

     try {
         LOG.debug(EntityUtils.toString(client.get(url)));
     } catch (IOException ioe) {
         LOG.error("Error : " + ioe.getMessage());
     }
  }

  @Test
  public void testGetEwbForTransporterByDateVThreeOh() throws IOException {
      String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", true);
      GenerateIrnReq req = client.getServices().getMapper().readValue(einvRequestBody, GenerateIrnReq.class);
      String createEinvResponse = createEinvoice(einvRequestBody, THREE_OH);
      assertNotNull(createEinvResponse);
      String flynnVersion = THREE_OH.getVersion();
      String apiVersion = EInvoiceVersion.ONE_OH_THREE.getVersion();
      String date = req.getDocumentDetails().getDate();
      String url = String.format("/basic/einv/%s/nic/%s/ewayapi/GetEwayBillsForTransporter?date=%s", flynnVersion, apiVersion, date);
      try {
          LOG.debug(EntityUtils.toString(client.get(url)));
      } catch (IOException ioe) {
          LOG.error("Error : " + ioe.getMessage());
      }
  }

  @Test
  public void testGetEinvoiceByDocDetailsVThreeOh() throws IOException {
      String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", false);
      GenerateIrnReq req = client.getServices().getMapper().readValue(einvRequestBody, GenerateIrnReq.class);
      String createEinvResponse = createEinvoice(einvRequestBody, THREE_OH);
      assertNotNull(createEinvResponse);

      String flynnVersion = THREE_OH.getVersion();
      String apiVersion = EInvoiceVersion.ONE_OH_THREE.getVersion();
      String docType = req.getDocumentDetails().getDocType();
      String docNumber = req.getDocumentDetails().getInvoiceNumber();
      String docDate = req.getDocumentDetails().getDate();

      String url = String.format("/basic/einv/%s/nic/eicore/%s/Invoice/irnbydocdetails?doctype=%s&docnum=%s&docdate=%s" ,
              flynnVersion, apiVersion, docType, docNumber, docDate);

      try {
          LOG.debug(EntityUtils.toString(client.get(url)));
      } catch (IOException ioe) {
          LOG.error("Error : " + ioe.getMessage());
      }
  }

  @Test
  public void testGetEwbByIrnVThreeOh() throws IOException {
      String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", true);
      String createEinvResponse = createEinvoice(einvRequestBody, THREE_OH);
      assertNotNull(createEinvResponse);
      String eInvoiceNumber = getEinvoiceNumberV3(client, createEinvResponse);
      String getResponse = getEwbByIrn(eInvoiceNumber, THREE_OH);
      assertNotNull(getResponse);
      LOG.info("Got Get EInvoice response " + getResponse);
  }

  @Test
  public void testGetEInvoiceVThreeOh() throws IOException {
      String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", false);
      String createEinvResponse = createEinvoice(einvRequestBody, THREE_OH);
      assertNotNull(createEinvResponse);
      String eInvoiceNumber = getEinvoiceNumberV3(client, createEinvResponse);
      String getResponse = getEinvoiceByIrn(eInvoiceNumber, THREE_OH);

      assertNotNull(getResponse);
      LOG.info("Got Get EInvoice response " + getResponse);
  }

  @Test
  public void testExtendEwayBillVThreeOh() throws IOException {
      String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "29AEKPV7203E1Z9", true);
      String createEinvResponse = createEinvoice(einvRequestBody, THREE_OH);
      assertNotNull(createEinvResponse);
      LOG.info("Got Create EWB response " + createEinvResponse);
      String extendEwbReqBody = String.format("{\n" +
              "  \"ewbNo\" : %s,\n" +
              "  \"vehicleNo\" : \"PVC1239\",\n" +
              "  \"fromPlace\" : \"FRAZER TOWN\",\n" +
              "  \"fromState\" : 27,\n" +
              "  \"remainingDistance\" : 757,\n" +
              "  \"transDocNo\" : \"tr_doc_123\",\n" +
              "  \"transDocDate\" : \"02/05/2018\",\n" +
              "  \"transMode\" : \"1\",\n" +
              "  \"extnRsnCode\" : 1,\n" +
              "  \"extnRemarks\" : \"Flood\",\n" +
              "  \"fromPincode\" : \"413512\",\n" +
              "  \"consignmentStatus\" : \"M\",\n" +
              "  \"transitType\" : \"\"\n" +
              "}", getEwbNumberV3(client, createEinvResponse));
      try {
          String extendEwayBillResponse = extendEwayBill(extendEwbReqBody, THREE_OH);
          LOG.info("Got Extend Eway Bill  response " + extendEwayBillResponse);
      } catch (Exception ex) {
          assertTrue(ex.toString().contains("382"));
      }
  }

  @Test
  public void testCreateGetBasicEinvoiceVThreeOh() throws IOException {
      String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", false);
      String createEinvResponse = createEinvoice(einvRequestBody, THREE_OH);
      assertNotNull(createEinvResponse);
      LOG.info("Got Create EWB response " + createEinvResponse);
      String einvoiceNumber = getEinvoiceNumberV3(client, createEinvResponse);
      String getResponse = getEinvoiceByIrn(einvoiceNumber, THREE_OH);
      assertNotNull(getResponse);
      LOG.info("Got Get EWB response " + getResponse);
  }

  @Test
  public void testGetGstinDetailsVThreeOh() throws IOException {
      String gstinResponse = getGstinDetails("24AAAPI3182M002", THREE_OH);
      assertNotNull(gstinResponse);
      LOG.debug("Got Get Gstin Response " + gstinResponse);
  }

  @Test
  public void testGetErrorListVThreeOh() throws IOException {
      String errorList = getErrorList(THREE_OH);
      LOG.debug("Error List " + errorList);
  }

  @Test
  public void testGenerateEwbByIrnVThreeOh() throws Exception {
      String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", false);
      String createEinvResponse = createEinvoice(einvRequestBody, THREE_OH);
      assertNotNull(createEinvResponse);
      LOG.info("Got Create EWB response " + createEinvResponse);

      String einvoiceNumber = getEinvoiceNumberV3(client, createEinvResponse);
      String requestBody = getBasicEwbRequestBody(einvoiceNumber, "19BIQPS4654H1ZI", null, null);

      String res = generateEwayBill(requestBody, THREE_OH);
      System.out.println(res);
  }

  @Test
  public void testCancelEwbVThreeOh() throws Exception {
      String eInvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", false);
      String createEinvResponse = createEinvoice(eInvRequestBody, THREE_OH);
      assertNotNull(createEinvResponse);
      LOG.info("Got Create EWB response " + createEinvResponse);

      String eInvoiceNumber = getEinvoiceNumberV3(client, createEinvResponse);
      String requestBody = getBasicEwbRequestBody(eInvoiceNumber, "19BIQPS4654H1ZI", null, null);

      String generateEwbResponse = generateEwayBill(requestBody, THREE_OH);

      String ewb = String.valueOf(getEwbNumberV3(client, generateEwbResponse));
      String cancelEwbRequestBody = String.format("{\"ewbNo\":%s,\"cancelRsnCode\": 3,\"cancelRmrk\":\"Cancelled the order\"}", ewb);
      String cancelEwbResponse = cancelEwb(cancelEwbRequestBody, THREE_OH);

      LOG.info("Got Cancel Ewb response " + cancelEwbResponse);

  }

  @Test
  public void testCreateInvoiceAndUpdatePartBVThreeOh() throws IOException {
      String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", true);
      String createEinvResponse = createEinvoice(einvRequestBody, THREE_OH);
      assertNotNull(createEinvResponse);
      LOG.info("Got Create EWB response " + createEinvResponse);
      String einvoiceNumber = String.valueOf(getEwbNumberV3(client, createEinvResponse));
      String updatedPartB = updatePartB(String.format("{\n" +
              "\"ewbNo\": \"%s\",\n" +
              "\"vehicleNo\": \"PQR1234\",\n" +
              "\"fromPlace\": \"BANGALORE\",\n" +
              "\"fromState\": 29,\n" +
              "\"reasonCode\": \"1\",\n" +
              "\"reasonRem\": \"vehicle broke down\",\n" +
              "\"transMode\": \"1\",\n" +
              "\"vehicleType\": null,\n" +
              "\"transDocDate\": null,\n" +
              "\"transDocNo\": null\n" +
              "}", einvoiceNumber), THREE_OH);
      LOG.info("Got Update Part B response " + updatedPartB);
  }

  @Test
  public void testAuth() {
    System.out.println("Done");
  }
}
