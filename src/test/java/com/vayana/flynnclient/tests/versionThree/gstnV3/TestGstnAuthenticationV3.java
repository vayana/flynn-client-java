package com.vayana.flynnclient.tests.versionThree.gstnV3;

import com.vayana.flynnclient.core.TestBase;
import com.vayana.flynnclient.request.FlynnGstnOperations;
import com.vayana.flynnclient.request.FlynnServiceType;
import com.vayana.flynnclient.request.FlynnVersion;
import com.vayana.flynnclient.request.GstnVersion;
import com.vayana.flynnclient.request.urlbuilder.impl.FlynnPostGstnRequestBuilder;
import com.vayana.flynnclient.util.DataMapper;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

import static com.vayana.flynnclient.types.headers.FlynnApiHeaders.GSTIN_ACTION_NAME;
import static com.vayana.flynnclient.types.headers.FlynnApiHeaders.GSTIN_HEADER_NAME;

public class TestGstnAuthenticationV3 extends TestBase {
  private static final Logger LOG = LoggerFactory.getLogger(TestGstnAuthenticationV3.class);

  @Test
  public void testGstnAuthenticationApisVThreeOh() throws IOException {

        //------------------------Request OTP------------------------

        String gstin = "33AOZPD0347J2ZR";
        String username = "TN_NT2.1228";

        String requestOTPResponse = EntityUtils.toString(
                new FlynnPostGstnRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.THREE_OH)
                        .setApiVersion(GstnVersion.OH_TWO)
                        .setOperation(FlynnGstnOperations.Authentication)
                        .setBody(String.format("{\"gstin\":\"%s\",\"username\":\"%s\"}", gstin, username))
                        .setHeaders(Map.of(GSTIN_ACTION_NAME, "OTPREQUEST"))
                        .getResponse()
        );
        LOG.debug("Request OTP response {}", requestOTPResponse);

        //------------------------Auth token------------------------

        String x = client.getServices().getMapper().readTree(requestOTPResponse).get("data").get("otp-request-id").textValue();
        System.out.println(x);
        String reqId = DataMapper.mapperSlash.readTree(requestOTPResponse).get("data").get("otp-request-id").textValue();
        String otp  = "575757";

        String authTokenResponse = EntityUtils.toString(
                new FlynnPostGstnRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.THREE_OH)
                        .setApiVersion(GstnVersion.OH_TWO)
                        .setOperation(FlynnGstnOperations.Authentication)
                        .setBody(String.format("{\"otp-request-id\": \"%s\", \"otp\": \"%s\"}", reqId, otp))
                        .setHeaders(Map.of(GSTIN_ACTION_NAME, "AUTHTOKEN"))
                        .getResponse()
        );
        LOG.debug("Auth token response {}", authTokenResponse);

        //------------------------Refresh token------------------------

        String sessionId = DataMapper.mapperSlash.readTree(authTokenResponse).get("data").get("gst-session-id").textValue();


        String refreshTokenResponse = EntityUtils.toString(
                new FlynnPostGstnRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.THREE_OH)
                        .setApiVersion(GstnVersion.OH_TWO)
                        .setOperation(FlynnGstnOperations.Authentication)
                        .setHeaders(Map.of(GSTIN_ACTION_NAME, "REFRESHTOKEN", GSTIN_HEADER_NAME, gstin))
                        .setBody(String.format("{\"gst-session-id\": \"%s\"}", sessionId))
                        .getResponse()
        );
        LOG.debug("Refresh token response {}", refreshTokenResponse);

        //------------------------Log Out------------------------

        String logOutResponse = EntityUtils.toString(
                new FlynnPostGstnRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.THREE_OH)
                        .setApiVersion(GstnVersion.OH_TWO)
                        .setOperation(FlynnGstnOperations.Authentication)
                        .setHeaders(Map.of(GSTIN_ACTION_NAME, "LOGOUT", GSTIN_HEADER_NAME, gstin))
                        .setBody(String.format("{\"gst-session-id\": \"%s\"}", sessionId))
                        .getResponse()
        );
        LOG.debug("Log Out response {}", logOutResponse);
    }
}
