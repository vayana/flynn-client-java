package com.vayana.flynnclient.tests.versionThree.gstnV3;

import com.vayana.flynnclient.core.TestBase;
import com.vayana.flynnclient.request.FlynnVersion;
import com.vayana.flynnclient.request.GstnVersion;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class TestViewAndTrackReturnsV3 extends TestBase {
    private static final Logger LOG = LoggerFactory.getLogger(TestViewAndTrackReturnsV3.class);

    @Test
    public void testViewAndTrackReturnsWithReturnTypeVThreeOh(){
        String gstin = "27AOZPD0347J1ZL";
        String financialYear = "2021-22";
        String returnType = "R1";
        String action = "RETTRACK";

        String apiVersion = GstnVersion.ONE_OH.getVersion();
        String flynnVersion = FlynnVersion.THREE_OH.getVersion();

        String url = String.format("/basic/gstn/%s/commonapi/%s/returns?gstin=%s&fy=%s&type=%s&action=%s", flynnVersion, apiVersion, gstin, financialYear, returnType, action);

        try {
            LOG.debug(EntityUtils.toString(client.get(url)));
        } catch (IOException ioe) {
            LOG.error("Error : " + ioe.getMessage());
        }
    }

    @Test
    public void testViewAndTrackReturnsWithoutReturnTypeVThreeOh(){
        String gstin = "27AOZPD0347J1ZL";
        String financialYear = "2021-22";
        String action = "RETTRACK";

        String apiVersion = GstnVersion.ONE_OH.getVersion();
        String flynnVersion = FlynnVersion.THREE_OH.getVersion();

        String url = String.format("/basic/gstn/%s/commonapi/%s/returns?gstin=%s&fy=%s&action=%s", flynnVersion, apiVersion, gstin, financialYear, action);

        try {
            LOG.debug(EntityUtils.toString(client.get(url)));
        } catch (IOException ioe) {
            LOG.error("Error : " + ioe.getMessage());
        }
    }

}
