package com.vayana.flynnclient.tests.versionThree.ewbV3;

import com.fasterxml.jackson.core.type.TypeReference;
import com.vayana.flynnclient.core.TestBase;
import com.vayana.flynnclient.exception.ConfigurationException;
import com.vayana.flynnclient.exception.SerializationException;
import com.vayana.flynnclient.request.*;
import com.vayana.flynnclient.request.urlbuilder.impl.FlynnGetEwbReqBuilder;
import com.vayana.flynnclient.request.urlbuilder.impl.FlynnPostEwbRequestBuilder;
import com.vayana.flynnclient.types.api.response.StatusResponse;
import com.vayana.flynnclient.types.config.ApiServerConfig;
import com.vayana.flynnclient.types.config.EwbConfig;
import com.vayana.flynnclient.util.DataMapper;
import com.vayana.flynnclient.util.GstnUtil;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static com.vayana.flynnclient.Constants.AES_CRYPTO_TRANSFORM;
import static com.vayana.flynnclient.util.CommonUtil.generateRek;
import static com.vayana.flynnclient.util.EncryptionUtil.encrypt;
import static com.vayana.flynnclient.util.EncryptionUtil.getEncryptedString;
import static com.vayana.flynnclient.util.FileUtil.getProjectRoot;
import static org.junit.Assert.*;

public class TestEnrichedEwbV3 extends TestBase {

    private static final Logger LOG = LoggerFactory.getLogger(TestEnrichedEwbV3.class);
    private static ExecutorService executorService;

    private final EwbConfig ewbConfig = client.getClientContext().getEwbConfig().orElseThrow(() -> new ConfigurationException("err-ewb-config-missing"));

    @BeforeClass
    public static void classSetup() {
        executorService = Executors.newSingleThreadExecutor();
    }

    @AfterClass
    public static void tearDown() {
        if (!executorService.isShutdown())
            executorService.shutdown();
    }

    @Before
    public void setup(){
    }

    public String verifyEwbCredential(FlynnVersion flynnVer) throws IOException {
        //Test case for failed scenario, so overriding the actual value of headers
        String genRek = generateRek();
        String pwd = getEncryptedString("abc", genRek, AES_CRYPTO_TRANSFORM);
        String rek = encrypt(client.getClientContext().getApiServerConfig().getPublicKey(), genRek);
        Map<String, String> map = Map.ofEntries(
                Map.entry("X-FLYNN-S-EWB-PWD", pwd),
                Map.entry("X-FLYNN-S-REK", rek)
        );
        FlynnEwbOperations ewbOperation = null;

        switch (flynnVer) {
            case ONE_OH:
                ewbOperation = FlynnEwbOperations.VerifyCredentialsVOneOh;
                break;
            default:
                LOG.error("Flynn-version not supported");

        }

        return EntityUtils.toString(
                new FlynnPostEwbRequestBuilder(client, FlynnServiceType.ENRICHED, flynnVer)
                        .setOperation(ewbOperation)
                        .setApiVersion(EwbVersion.ONE_OH_THREE)
                        .setHeaders(map)
                        .getResponse()
        );
    }

    public UUID callGetCewbPdf() {
        String cewbNumber = "2710011822";
        String url = String.format("/enriched/ewb/%s/eway-bills/download-cewb-pdf/%s", FlynnVersion.ONE_OH.getVersion(), cewbNumber);
        try {
            String response = EntityUtils.toString(client.get(url));
            String task = GstnUtil.getTaskIdV3(client, response);
            return UUID.fromString(task);
        } catch (IOException cause) {
            LOG.error(String.format("err fetching get cewb pdf response. %s", cause));
            throw new SerializationException(String.format("get cewb pdf response failed. %s", cause.getMessage()), cause);
        }
    }

    public StatusResponse getCewbPdfStatus(UUID refId) {
        ApiServerConfig apiServerConfig = client.getClientContext().getApiServerConfig();
        String url = String.format("/enriched/tasks/%s/status/%s", apiServerConfig.getVersion(), refId);

        Map<String, String> headers = new HashMap<>();

        headers.put("accept", "application/json");

        try {
            String statusResponse = EntityUtils.toString(client.get(url, headers));
            String data = DataMapper.mapperSlash.readTree(statusResponse).get("data").textValue();
            return client.getServices().getMapper().readValue(data, new TypeReference<StatusResponse>() {
            });
        } catch (IOException cause) {
            LOG.error(String.format("err fetching status response. %s", cause));
            throw new SerializationException(String.format("get status response failed. %s", cause.getMessage()), cause);
        }
    }

    @Test
  public void testGetEwbAsPdfFromBuilderVThreeOh(){
        String ewbNumber = "231010025316";

        Map<PathParams, String> params = new HashMap<>();
        params.put(PathParams.EwbNumber, ewbNumber);

        HttpEntity response = new FlynnGetEwbReqBuilder(client, FlynnServiceType.ENRICHED, FlynnVersion.THREE_OH)
                .setOperation(FlynnEwbOperations.GetEwbAsPdf)
                .setParam(params)
                .getResponse();
        try {
            InputStream content = response.getContent();

            Paths.get(getProjectRoot(), "transient", "pdfDownload", "ewb", ewbNumber).toFile().mkdirs();

            File file = Paths.get(getProjectRoot(), "transient", "pdfDownload", "ewb", ewbNumber, "ewb-pdf.pdf").toFile();
            FileOutputStream writer = new FileOutputStream(file);

            IOUtils.copy(content, writer);
            writer.close();
        }catch (IOException cause) {
            LOG.error(String.format("err fetching create E-invoice response. %s", cause));
            throw new SerializationException(String.format("create e-invoice response failed. %s", cause.getMessage()), cause);
        }
  }

    @Test
    public void testVerifyEwbCredential() throws IOException {
        try {
            FlynnEwbOperations ewbOperation = null;
            FlynnVersion flynnVer = FlynnVersion.ONE_OH;
            switch (flynnVer) {
                case ONE_OH:
                    ewbOperation = FlynnEwbOperations.VerifyCredentialsVOneOh;
                    break;
                default:
                    LOG.error("Flynn-version not supported");

            }

            String response = EntityUtils.toString(
                    new FlynnPostEwbRequestBuilder(client, FlynnServiceType.ENRICHED, flynnVer)
                            .setOperation(ewbOperation)
                            .setApiVersion(EwbVersion.ONE_OH_THREE)
                            .getResponse()
            );
            assertEquals("", response);
        } catch (Exception e) {
            boolean response = e.toString().contains("502");
            assertFalse(response);

        }
    }

    @Test
    public void testVerifyEwbCredentialWithWrongPassword() throws IOException {
        try {
            String response = verifyEwbCredential(FlynnVersion.ONE_OH);
            assertNotEquals("", response);
        } catch (Exception e) {
            boolean response = e.toString().contains("502");
            assertTrue(response);
        }
    }

  @Test
  public void testGetEwbAsPdfVThreeOh() {
        ApiServerConfig apiConfig = client.getClientContext().getApiServerConfig();

        String ewbNumber = "231010025316";

        String url = String.format("/enriched/ewb/%s/eway-bills/download-pdf/%s", FlynnVersion.THREE_OH.getVersion(), ewbNumber);

        try {
            HttpEntity response = client.get(url);

            InputStream content = response.getContent();

            Paths.get(getProjectRoot(), "transient", "pdfDownload", "ewb", ewbNumber).toFile().mkdirs();

            File file = Paths.get(getProjectRoot(), "transient", "pdfDownload", "ewb", ewbNumber, "ewb-pdf.pdf").toFile();
            FileOutputStream writer = new FileOutputStream(file);

            IOUtils.copy(content, writer);
            writer.close();
        } catch (IOException cause) {
            LOG.error(String.format("err fetching create E-invoice response. %s", cause));
            throw new SerializationException(String.format("create e-invoice response failed. %s", cause.getMessage()), cause);
        }
  }

    @Test
    public void testGetConsolidateEwbAsPdfVOneOh() {
        try {
            UUID taskId = callGetCewbPdf();
            long startTime = System.nanoTime();
            Callable<StatusResponse> statusJob = () -> {
                StatusResponse status = getCewbPdfStatus(taskId);

                do {
                    LOG.debug(String.format("There are %d pending jobs", status.getJobs().getPending()));
                    LOG.debug("Sleeping for 40 sec");
                    Thread.sleep(40000);

                    status = getCewbPdfStatus(taskId);
                } while (status.getJobs().getPending() != 0);

                return status;
            };
            // Future Task, which gives a Completed Status Response
            Future<StatusResponse> statusResponse = executorService.submit(statusJob);

            while (!statusResponse.isDone()) {
                LOG.debug("Job is not completed yet. Sleeping for 40 seconds");
                Thread.sleep(40000);

                if ((System.nanoTime() - startTime) / 1000000000.0 > 60) {
                    LOG.debug("Elapsed time is more than 1 Min. Cancelling the Call.");
                    statusResponse.cancel(true);

                    fail("Task failed. Time Limit Exceeded.");
                }
            }

            byte[] zipResponse = getDownloadResponse(taskId);
            assertNotNull(zipResponse);

            saveFiles(taskId.toString(), zipResponse);
            LOG.debug("Got Consolidate Ewb As Pdf Response Task-Id >>> {}", taskId);
        } catch (Exception cause) {
            LOG.error(String.format("err fetching Get Consolidate Ewb As Pdf response. %s", cause));
            throw new RuntimeException(cause);
        }

    }

}
