package com.vayana.flynnclient.tests.versionThree.gstnV3;

import com.vayana.flynnclient.core.TestBase;
import com.vayana.flynnclient.request.FlynnVersion;
import com.vayana.flynnclient.request.GstnVersion;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;



public class TestUnregisteredApplicantsValidationAPIV1 extends TestBase {
  private static final Logger LOG = LoggerFactory.getLogger(TestUnregisteredApplicantsValidationAPIV1.class);

  @Test
  public void testUnregisteredApplicantsAPIVOneOh(){
        String uid = "072200000020ESF";
        String action = "TP";
        String email = "abc@gmail.com";
        String mobile = "9898989898";
        String apiVersion = GstnVersion.ONE_OH.getVersion();
        String flynnVersion = FlynnVersion.ONE_OH.getVersion();
        String url = String.format("/basic/gstn/%s/commonapi/%s/unregistered-applicants-validation?action=%s&uid=%s&mobile=%s&email=%s", flynnVersion, apiVersion, action, uid, mobile, email);

        Map<String, String> headers = new HashMap<>();
        headers.put("X-FLYNN-N-GSTIN", "33GSPTN0792G1Z4");

      try {
            LOG.debug(EntityUtils.toString(client.get(url, headers)));
        } catch (IOException ioe) {
            LOG.error("Error : " + ioe.getMessage());
        }
  }

}
