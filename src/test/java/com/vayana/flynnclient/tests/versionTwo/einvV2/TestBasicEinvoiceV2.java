package com.vayana.flynnclient.tests.versionTwo.einvV2;

import com.vayana.flynnclient.core.TestBase;
import com.vayana.flynnclient.exception.ConfigurationException;
import com.vayana.flynnclient.exception.SerializationException;
import com.vayana.flynnclient.request.EInvoiceVersion;
import com.vayana.flynnclient.request.FlynnEInvoiceOperations;
import com.vayana.flynnclient.request.FlynnServiceType;
import com.vayana.flynnclient.request.FlynnVersion;
import com.vayana.flynnclient.request.urlbuilder.impl.FlynnGetEInvRequestBuilder;
import com.vayana.flynnclient.request.urlbuilder.impl.FlynnPostEInvRequestBuilder;
import com.vayana.flynnclient.types.api.request.GenerateIrnReq;
import com.vayana.flynnclient.types.config.EinvoiceConfig;
import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import static com.vayana.flynnclient.request.FlynnVersion.ONE_OH;
import static com.vayana.flynnclient.request.FlynnVersion.TWO_OH;
import static com.vayana.flynnclient.util.EinvoiceUtil.*;
import static org.junit.Assert.*;

public class TestBasicEinvoiceV2 extends TestBase {

  private static final Logger LOG = LoggerFactory.getLogger(TestBasicEinvoiceV2.class);
  private final EinvoiceConfig einvConfig = client.getClientContext().getEinvoiceConfig().orElseThrow(() -> new ConfigurationException("err-einv-config-missing"));

  @Before
  public void setup() {
    authenticate();
  }

  /**
   * Given an E-invoice authentication request body, fetch the API response for E-invoice Authentication API
   */
  private void authenticate() {

    try {
      // Http Status 200 signifies successful authentication
      String authenticate = EntityUtils.toString(
        new FlynnPostEInvRequestBuilder(client, FlynnServiceType.BASIC, FlynnVersion.ONE_OH)
          .setOperation(FlynnEInvoiceOperations.Authenticate)
          .setApiVersion(EInvoiceVersion.ONE_OH_FOUR)
          .getResponse()
      );
      assertEquals("", authenticate);
    } catch (IOException cause) {
      LOG.error(String.format("err authenticating to E-way Bill. %s", cause));
    }
  }

  /**
   * Given an E-invoice request body, fetch the API response for Create E-invoice API
   */
  private String createEinvoice(String body, FlynnVersion flynnVersion) throws IOException {
    return EntityUtils.toString(
      new FlynnPostEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
        .setOperation(FlynnEInvoiceOperations.GenerateEInvoice)
        .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
        .setBody(body)
        .getResponse()
    );
  }


  /**
   * Given an E-way bill number, fetch the API response for Get E-way bill details API
   */
  private String generateEwayBill(String body, FlynnVersion flynnVersion) {
    HttpEntity response = new FlynnPostEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
      .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
      .setOperation(FlynnEInvoiceOperations.GenerateEWB)
      .setBody(body)
      .getResponse();

    try {
      return EntityUtils.toString(response);
    } catch (IOException cause) {
      LOG.error(String.format("err fetching get E-ewb response. %s", cause));
      throw new SerializationException(String.format("Error fetching get E-ewb response. %s", cause.getMessage()), cause);
    }
  }

  /**
   * Given an E-way Bill cancel request body, fetch the API response for Cancel E-way Bill API
   */
  private String cancelEwb(String body, FlynnVersion flynnVer) throws IOException {

      FlynnEInvoiceOperations eInvoiceOperation = null;

      switch (flynnVer) {
          case ONE_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.CancelEwbVOneOh;
              break;
          case TWO_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.CancelEwbVTwoOh;
              break;
          case THREE_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.CancelEwbVThreeOh;
              break;
      }

    return EntityUtils.toString(
      new FlynnPostEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVer)
        .setOperation(eInvoiceOperation)
        .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
        .setBody(body)
        .getResponse()
    );
  }

  /**
   * Given an Update part-B request body, fetch the API response for Update part-B API
   */
  private String updatePartB(String body, FlynnVersion flynnVersion) throws IOException {

      FlynnEInvoiceOperations eInvoiceOperation = null;

      switch (flynnVersion) {
          case ONE_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.UpdatePartBvOneOh;
              break;
          case TWO_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.UpdatePartBvTwoOh;
              break;
          case THREE_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.UpdatePartBvThreeOh;
              break;
      }


    return EntityUtils.toString(
      new FlynnPostEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
        .setOperation(eInvoiceOperation)
        .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
        .setBody(body)
        .getResponse()
    );
  }

  /**
   * Given an Extend ewb request body, fetch the API response for Extend ewb API
   */
  private String extendEwayBill(String body, FlynnVersion flynnVersion) throws IOException {
      FlynnEInvoiceOperations eInvoiceOperation = null;

      switch (flynnVersion) {
          case ONE_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.ExtendEwbVOneOh;
              break;
          case TWO_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.ExtendEwbVTwoOh;
              break;
          case THREE_OH:
              eInvoiceOperation = FlynnEInvoiceOperations.ExtendEwbVThreeOh;
              break;
      }


      return EntityUtils.toString(
      new FlynnPostEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVersion)
        .setOperation(eInvoiceOperation)
        .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
        .setBody(body)
        .getResponse()
    );
  }


  /**
   * Fetch the API response for E-invoice error list API
   */
  private String getErrorList(FlynnVersion flynnVer) throws IOException {
    FlynnEInvoiceOperations eInvoiceOperation = flynnVer == ONE_OH? FlynnEInvoiceOperations.GetErrorListVOneOh : FlynnEInvoiceOperations.GetErrorListVTwoOh;
    return EntityUtils.toString(
      new FlynnGetEInvRequestBuilder(client, FlynnServiceType.BASIC, flynnVer)
        .setOperation(eInvoiceOperation)
        .setApiVersion(EInvoiceVersion.ONE_OH_THREE)
        .getResponse()
    );
  }


  @Test
  public void testCreateAndGetBasicEinvoiceWithEwbVTwoOh() throws IOException {
      String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", true);
      String createResponse = createEinvoice(einvRequestBody, ONE_OH);
      assertNotNull(createResponse);
      LOG.info("Got Create EWB response " + createResponse);

      String ewbNo = Long.toString(getEwbNumber(client, createResponse));

      String url = String.format("/basic/einv/%s/nic/v1.03/ewayapi/GetEwayBill?ewbNo=%s", TWO_OH.getVersion(), ewbNo);

      try {
          LOG.debug(EntityUtils.toString(client.get(url)));
      } catch (IOException ioe) {
          LOG.error("Error : " + ioe.getMessage());
      }
  }

  @Test
  public void testGetEWBByDateVTwoOh() throws IOException {
    String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", true);
    GenerateIrnReq req = client.getServices().getMapper().readValue(einvRequestBody, GenerateIrnReq.class);
    String createEinvResponse = createEinvoice(einvRequestBody, ONE_OH);
    assertNotNull(createEinvResponse);

    String flynnVersion = FlynnVersion.TWO_OH.getVersion();
    String apiVersion = EInvoiceVersion.ONE_OH_THREE.getVersion();
    String date = req.getDocumentDetails().getDate();

    String url = String.format("/basic/einv/%s/nic/%s/ewayapi/GetEwayBillsByDate?date=%s", flynnVersion, apiVersion, date);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  public void testGetEwbForTransporterByDateVTwoOh() throws IOException {
    String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", true);
    GenerateIrnReq req = client.getServices().getMapper().readValue(einvRequestBody, GenerateIrnReq.class);
    String createEinvResponse = createEinvoice(einvRequestBody, ONE_OH);
    assertNotNull(createEinvResponse);

    String flynnVersion = FlynnVersion.TWO_OH.getVersion();
    String apiVersion = EInvoiceVersion.ONE_OH_THREE.getVersion();
    String date = req.getDocumentDetails().getDate();

    String url = String.format("/basic/einv/%s/nic/%s/ewayapi/GetEwayBillsForTransporter?date=%s", flynnVersion, apiVersion, date);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  public void testGetEinvoiceByDocDetailsVTwoOh() throws IOException {
    String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", false);
    GenerateIrnReq req = client.getServices().getMapper().readValue(einvRequestBody, GenerateIrnReq.class);
    String createEinvResponse = createEinvoice(einvRequestBody, ONE_OH);
    assertNotNull(createEinvResponse);

    String flynnVersion = TWO_OH.getVersion();
    String apiVersion = EInvoiceVersion.ONE_OH_THREE.getVersion();
    String docType = req.getDocumentDetails().getDocType();
    String docNumber = req.getDocumentDetails().getInvoiceNumber();
    String docDate = req.getDocumentDetails().getDate();

    String url = String.format("/basic/einv/%s/nic/eicore/%s/Invoice/irnbydocdetails?doctype=%s&docnum=%s&docdate=%s" , flynnVersion, apiVersion, docType, docNumber, docDate);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  public void testExtendEwayBillVTwoOh() throws IOException {
      String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "29AEKPV7203E1Z9", true);
      String createEinvResponse = createEinvoice(einvRequestBody, ONE_OH);
      assertNotNull(createEinvResponse);
      LOG.info("Got Create EWB response " + createEinvResponse);
      String extendEwbReqBody = String.format("{\n" +
              "  \"ewbNo\" : %s,\n" +
              "  \"vehicleNo\" : \"PVC1239\",\n" +
              "  \"fromPlace\" : \"FRAZER TOWN\",\n" +
              "  \"fromState\" : 27,\n" +
              "  \"remainingDistance\" : 757,\n" +
              "  \"transDocNo\" : \"tr_doc_123\",\n" +
              "  \"transDocDate\" : \"02/05/2018\",\n" +
              "  \"transMode\" : \"1\",\n" +
              "  \"extnRsnCode\" : 1,\n" +
              "  \"extnRemarks\" : \"Flood\",\n" +
              "  \"fromPincode\" : \"413512\",\n" +
              "  \"consignmentStatus\" : \"M\",\n" +
              "  \"transitType\" : \"\"\n" +
              "}", getEwbNumber(client, createEinvResponse));

      try {
          String extendEwayBillResponse = extendEwayBill(extendEwbReqBody, TWO_OH);
          LOG.info("Got Extend Eway Bill response " + extendEwayBillResponse);
      } catch (Exception ex) {
          assertTrue(ex.toString().contains("382"));
      }
  }

  @Test
  public void testGetErrorListVTwoOh() throws IOException {
    String errorList = getErrorList(TWO_OH);
    LOG.debug("Error List " + errorList);
  }

  @Test
  public void testCancelEwbVTwoOh() throws Exception {
    String eInvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", false);
    String createEinvResponse = createEinvoice(eInvRequestBody, ONE_OH);
    assertNotNull(createEinvResponse);
    LOG.info("Got Create EWB response " + createEinvResponse);

    String eInvoiceNumber = getEinvoiceNumber(client, createEinvResponse);
    String requestBody = getBasicEwbRequestBody(eInvoiceNumber, "19BIQPS4654H1ZI", null, null);

    String generateEwbResponse = generateEwayBill(requestBody, ONE_OH);

    String ewb = String.valueOf(getEwbNumber(client, generateEwbResponse));
    String cancelEwbRequestBody = String.format("{\"ewbNo\":%s,\"cancelRsnCode\": 3,\"cancelRmrk\":\"Cancelled the order\"}", ewb);
    String cancelEwbResponse = cancelEwb(cancelEwbRequestBody, TWO_OH);

    LOG.info("Got Cancel Ewb response " + cancelEwbResponse);

  }

  @Test
  public void testCreateInvoiceAndUpdatePartBVTwoOh() throws IOException {
      String einvRequestBody = getBasicEinvoiceRequestBody(einvConfig.getGstin(), "27AAFCV6085L1ZO", true);
      String createEinvResponse = createEinvoice(einvRequestBody, ONE_OH);
      assertNotNull(createEinvResponse);
      LOG.info("Got Create EWB response " + createEinvResponse);

      String einvoiceNumber = String.valueOf(getEwbNumber(client, createEinvResponse));

      String updatedPartB = updatePartB(String.format("{\n" +
              "\"ewbNo\": \"%s\",\n" +
              "\"vehicleNo\": \"PQR1234\",\n" +
              "\"fromPlace\": \"BANGALORE\",\n" +
              "\"fromState\": 29,\n" +
              "\"reasonCode\": \"1\",\n" +
              "\"reasonRem\": \"vehicle broke down\",\n" +
              "\"transMode\": \"1\",\n" +
              "\"vehicleType\": null,\n" +
              "\"transDocDate\": null,\n" +
              "\"transDocNo\": null\n" +
              "}", einvoiceNumber), TWO_OH);

      LOG.info("Got Update Part B response " + updatedPartB);
  }

  @Test
  public void testAuth() {
    System.out.println("Done");
  }
}
