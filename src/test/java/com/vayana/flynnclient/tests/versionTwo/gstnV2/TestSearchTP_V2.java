package com.vayana.flynnclient.tests.versionTwo.gstnV2;

import com.vayana.flynnclient.core.TestBase;
import com.vayana.flynnclient.request.*;
import org.apache.http.util.EntityUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class TestSearchTP_V2 extends TestBase {
  private static final Logger LOG = LoggerFactory.getLogger(TestSearchTP_V2.class);

  private static ExecutorService executorService;

  @BeforeClass
  public static void classSetup() {
    executorService = Executors.newSingleThreadExecutor();
  }

  @AfterClass
  public static void tearDown() {
    if(!executorService.isShutdown())
      executorService.shutdown();
  }

  @Test
  public void testSearchTaxPayerVTwoOh(){
    String gstin = "33GSPTN0791G1Z5";
    String action = "TP";
    String apiVersion = GstnVersion.OH_THREE.getVersion();
    String flynnVersion = FlynnVersion.TWO_OH.getVersion();
    String url = String.format("/basic/gstn/%s/commonapi/%s/search?gstin=%s&action=%s", flynnVersion, apiVersion, gstin, action);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }
}
