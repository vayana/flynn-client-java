package com.vayana.flynnclient.tests.versionTwo.gstnV2;

import com.vayana.flynnclient.core.TestBase;
import com.vayana.flynnclient.request.FlynnVersion;
import com.vayana.flynnclient.request.GstnVersion;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class TestViewAndTrackReturnsV2 extends TestBase {
    private static final Logger LOG = LoggerFactory.getLogger(TestViewAndTrackReturnsV2.class);

    @Test
    public void testViewAndTrackReturnsWithReturnTypeVTwoOh(){
        String gstin = "27AOZPD0347J1ZL";
        String financialYear = "2021-22";
        String returnType = "R1";

        String apiVersion = GstnVersion.ONE_OH.getVersion();
        String flynnVersion = FlynnVersion.TWO_OH.getVersion();

        String url = String.format("/basic/gstn/%s/commonapi/%s/returns?gstin=%s&fy=%s&type=%s", flynnVersion, apiVersion, gstin, financialYear, returnType);

        try {
            LOG.debug(EntityUtils.toString(client.get(url)));
        } catch (IOException ioe) {
            LOG.error("Error : " + ioe.getMessage());
        }
    }

    @Test
    public void testViewAndTrackReturnsWithoutReturnTypeVTwoOh(){
        String gstin = "27AOZPD0347J1ZL";
        String financialYear = "2021-22";

        String apiVersion = GstnVersion.ONE_OH.getVersion();
        String flynnVersion = FlynnVersion.TWO_OH.getVersion();

        String url = String.format("/basic/gstn/%s/commonapi/%s/returns?gstin=%s&fy=%s", flynnVersion, apiVersion, gstin, financialYear);

        try {
            LOG.debug(EntityUtils.toString(client.get(url)));
        } catch (IOException ioe) {
            LOG.error("Error : " + ioe.getMessage());
        }
    }

}
