package com.vayana.flynnclient.tests.versionTwo.ewbV2;

import com.vayana.flynnclient.core.TestBase;
import com.vayana.flynnclient.exception.ConfigurationException;
import com.vayana.flynnclient.exception.SerializationException;
import com.vayana.flynnclient.request.*;
import com.vayana.flynnclient.request.urlbuilder.impl.FlynnGetEwbReqBuilder;
import com.vayana.flynnclient.request.urlbuilder.impl.FlynnPostEwbRequestBuilder;
import com.vayana.flynnclient.types.config.ApiServerConfig;
import com.vayana.flynnclient.types.config.EwbConfig;
import com.vayana.flynnclient.util.DataMapper;
import com.vayana.flynnclient.util.EwayBillUtil;
import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

import static com.vayana.flynnclient.Constants.AES_CRYPTO_TRANSFORM;
import static com.vayana.flynnclient.request.FlynnVersion.*;
import static com.vayana.flynnclient.types.headers.FlynnApiHeaders.EWB_PASSWORD_HEADER_NAME;
import static com.vayana.flynnclient.types.headers.FlynnApiHeaders.MASTER_SECRET_HEADER_NAME;
import static com.vayana.flynnclient.util.CommonUtil.generateRek;
import static com.vayana.flynnclient.util.EncryptionUtil.encrypt;
import static com.vayana.flynnclient.util.EncryptionUtil.getEncryptedString;
import static com.vayana.flynnclient.util.EwayBillUtil.dateFormat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestBasicEwbV2 extends TestBase {

  private static final Logger LOG = LoggerFactory.getLogger(TestBasicEwbV2.class);

  private final EwbConfig ewbConfig = client.getClientContext().getEwbConfig().orElseThrow(() -> new ConfigurationException("err-ewb-config-missing"));

  @Before
  public void setup() {
    // Authenticate before calling Basic APIs
    newAuthenticate();
  }

  /**
   * Given an E-way bill authentication request body, fetch the API response for E-way bill Authentication API
   */
  public void newAuthenticate() {
    final EwbConfig ewbConfig = client.getClientContext().getEwbConfig().orElseThrow(() -> new ConfigurationException("err-ewb-config-missing"));

    String rek = generateRek();

    Map<String, String> ewbHeaders = new HashMap<>();
    ewbHeaders.put(MASTER_SECRET_HEADER_NAME, encrypt(client.getClientContext().getApiServerConfig().getPublicKey(), rek));
    ewbHeaders.put(EWB_PASSWORD_HEADER_NAME, getEncryptedString(ewbConfig.getPassword(), rek, AES_CRYPTO_TRANSFORM));

    try {
      // Http Status 200 signifies successful authentication
      String newAuthenticate = EntityUtils.toString(new FlynnPostEwbRequestBuilder(client, FlynnServiceType.BASIC, ONE_OH)
        .setOperation(FlynnEwbOperations.NewAuthenticate)
        .setApiVersion(EwbVersion.ONE_OH_THREE)
        .setHeaders(ewbHeaders)
        .getResponse());
      assertEquals("", newAuthenticate);
    } catch (IOException cause) {
      LOG.error(String.format("err authenticating to E-way Bill. %s", cause));
    }
  }


  /**
   * Given an E-way bill Generate request body, fetch the API response for E-way bill Generation API
   * (Using Builder)
   */
  public String createEwbNewWay(String body, FlynnVersion flynnVer) throws IOException {

      FlynnEwbOperations ewbOperation = null;

      switch (flynnVer) {
          case ONE_OH:
              ewbOperation = FlynnEwbOperations.GenerateEwbVOneOh;
              break;
          case TWO_OH:
              ewbOperation = FlynnEwbOperations.GenerateEwbVTwoOh;
              break;
          case THREE_OH:
              ewbOperation = FlynnEwbOperations.GenerateEwbVThreeOh;
              break;
      }

    return EntityUtils.toString(
      new FlynnPostEwbRequestBuilder(client, FlynnServiceType.BASIC, flynnVer)
        .setOperation(ewbOperation)
        .setApiVersion(EwbVersion.ONE_OH_THREE)
        .setHeaders(Collections.emptyMap())
        .setBody(body)
        .getResponse()
    );
  }

  public String extendEwbWay(String body, FlynnVersion flynnVer) throws IOException {

      FlynnEwbOperations ewbOperation = null;

      switch (flynnVer) {
          case ONE_OH:
              ewbOperation = FlynnEwbOperations.ExtendValidityVOneOh;
              break;
          case TWO_OH:
              ewbOperation = FlynnEwbOperations.ExtendValidityVTwoOh;
              break;
          case THREE_OH:
              ewbOperation = FlynnEwbOperations.ExtendValidityVThreeOh;
              break;
      }

    return EntityUtils.toString(
      new FlynnPostEwbRequestBuilder(client, FlynnServiceType.BASIC, flynnVer)
        .setOperation(ewbOperation)
        .setApiVersion(EwbVersion.ONE_OH_THREE)
        .setHeaders(Collections.emptyMap())
        .setBody(body)
        .getResponse()
    );
  }

  /**
   * Given an E-way bill Cancel request body, fetch the API response for E-way bill Cancel API
   * (Using Builder)
   */
  private String cancelEwb(String body, FlynnVersion flynnVer) throws IOException {

      FlynnEwbOperations ewbOperation = null;

      switch (flynnVer) {
          case ONE_OH:
              ewbOperation = FlynnEwbOperations.CancelEwbVOneOh;
              break;
          case TWO_OH:
              ewbOperation = FlynnEwbOperations.CancelEwbVTwoOh;
              break;
          case THREE_OH:
              ewbOperation = FlynnEwbOperations.CancelEwbVThreeOh;
              break;
      }

    return EntityUtils.toString(
      new FlynnPostEwbRequestBuilder(client, FlynnServiceType.BASIC, flynnVer)
        .setOperation(ewbOperation)
        .setApiVersion(EwbVersion.ONE_OH_THREE)
        .setHeaders(Collections.emptyMap())
        .setBody(body)
        .getResponse()
    );
  }

  /**
   * Given an E-way bill initiate Multiple Vehicle Movement request body, fetch the API response for E-way bill initiate Multiple Vehicle Movement API
   * (Using Builder)
   */
  private String initiateMultiVehicle(String body, FlynnVersion flynnVer) throws IOException {

      FlynnEwbOperations ewbOperation = null;

      switch (flynnVer) {
          case ONE_OH:
              ewbOperation = FlynnEwbOperations.InitiateMultiVehicleVOneOh;
              break;
          case TWO_OH:
              ewbOperation = FlynnEwbOperations.InitiateMultiVehicleVTwoOh;
              break;
          case THREE_OH:
              ewbOperation = FlynnEwbOperations.InitiateMultiVehicleVThreeOh;
              break;
      }

    return EntityUtils.toString(
      new FlynnPostEwbRequestBuilder(client, FlynnServiceType.BASIC, flynnVer)
        .setOperation(ewbOperation)
        .setApiVersion(EwbVersion.ONE_OH_THREE)
        .setHeaders(Collections.emptyMap())
        .setBody(body)
        .getResponse()
    );
  }

  /**
   * Given an E-way bill add Multiple Vehicle To Group request body, fetch the API response for E-way bill add Multiple Vehicle To Group API
   * (Using Builder)
   */
  private String addMultiVehicle(String body, FlynnVersion flynnVer) throws IOException {

      FlynnEwbOperations ewbOperation = null;

      switch (flynnVer) {
          case ONE_OH:
              ewbOperation = FlynnEwbOperations.AddMultiVehicleVOneOh;
              break;
          case TWO_OH:
              ewbOperation = FlynnEwbOperations.AddMultiVehicleVTwoOh;
              break;
          case THREE_OH:
              ewbOperation = FlynnEwbOperations.AddMultiVehicleVThreeOh;
              break;
      }

    return EntityUtils.toString(
      new FlynnPostEwbRequestBuilder(client, FlynnServiceType.BASIC, flynnVer)
        .setOperation(ewbOperation)
        .setApiVersion(EwbVersion.ONE_OH_THREE)
        .setHeaders(Collections.emptyMap())
        .setBody(body)
        .getResponse()
    );
  }


  /**
   * Given Reject Ewb request body, fetch the API response for Reject EwbAPI
   * (Using Builder)
   */
  private String rejectEwb(String body, FlynnVersion flynnVer) throws IOException {

    Map<String, String> map = Map.ofEntries(
      Map.entry("X-FLYNN-N-EWB-GSP-CODE", "clayfin"),
      Map.entry("X-FLYNN-N-EWB-GSTIN", "29AAACW4202F1ZM"),
      Map.entry("X-FLYNN-N-EWB-USERNAME", "test_dlr519"),
      Map.entry("X-FLYNN-N-EWB-PWD", "test_dlr519"),
      Map.entry("X-FLYNN-S-REK", "uFvKeeKDGypcy/2S6eQ1nga3Gc+ftpp4W/rBXzB3QjCogPXYFMnI/lKhhgu9cN0XKkmxEyjzK0BMtU8QScK35/TCWDhfx8siI39F2Dorc0TAecna2yAT2Ti2mMwrUPmbmqBPo8FQ3ZdXC8MNNCBsdrZ21jN8llKBRCWRiQGRv8QPXJVzqb8y0ijUos0X2i7UFNEdu9LEu0WBHMnshIsXPUCAZ2Yt41huTeE60Vm9vEkOjkFaN/kHxUZveEg2Z2jwNOLFU1LZT9zxoTNgSXoA5ObCoAWCpCeerQVAHc9WjYxjJYlVFm/ITvQYwjCmAQXxdLXIVDPaJPZt51ncCtnI8Q==")
    );

    FlynnEwbOperations ewbOperation = flynnVer == ONE_OH? FlynnEwbOperations.RejectEwbVOneOh : FlynnEwbOperations.RejectEwbVTwoOh;

    return EntityUtils.toString(
      new FlynnPostEwbRequestBuilder(client, FlynnServiceType.BASIC, flynnVer)
        .setOperation(ewbOperation)
        .setApiVersion(EwbVersion.ONE_OH_THREE)
        .setHeaders(map)
        .setBody(body)
        .getResponse()
    );
  }

  /**
   * Given an E-way bill Change Multiple Vehicle request body, fetch the API response for E-way bill Change Multiple Vehicle API
   * (Using Builder)
   */
  private String changeMultiVehicle(String body, FlynnVersion flynnVer) throws IOException {

      FlynnEwbOperations ewbOperation = null;

      switch (flynnVer) {
          case ONE_OH:
              ewbOperation = FlynnEwbOperations.ChangeMultiVehicleVOneOh;
              break;
          case TWO_OH:
              ewbOperation = FlynnEwbOperations.ChangeMultiVehicleVTwoOh;
              break;
          case THREE_OH:
              ewbOperation = FlynnEwbOperations.ChangeMultiVehicleVThreeOh;
              break;
      }

    return EntityUtils.toString(
      new FlynnPostEwbRequestBuilder(client, FlynnServiceType.BASIC, flynnVer)
        .setOperation(ewbOperation)
        .setApiVersion(EwbVersion.ONE_OH_THREE)
        .setHeaders(Collections.emptyMap())
        .setBody(body)
        .getResponse()
    );
  }

  /**
   * Given an E-way bill Consolidate Ewb request body, fetch the API response for E-way bill Consolidate Ewb API
   * (Using Builder)
   */
  private String consolidateEwb(String body, FlynnVersion flynnVer) throws IOException {

      FlynnEwbOperations ewbOperation = null;

      switch (flynnVer) {
          case ONE_OH:
              ewbOperation = FlynnEwbOperations.ConsolidateVOneOh;
              break;
          case TWO_OH:
              ewbOperation = FlynnEwbOperations.ConsolidateVTwoOh;
              break;
          case THREE_OH:
              ewbOperation = FlynnEwbOperations.ConsolidateVThreeOh;
              break;
      }

    return EntityUtils.toString(
      new FlynnPostEwbRequestBuilder(client, FlynnServiceType.BASIC,flynnVer)
        .setOperation(ewbOperation)
        .setApiVersion(EwbVersion.ONE_OH_THREE)
        .setHeaders(Collections.emptyMap())
        .setBody(body)
        .getResponse()
    );
  }

  /**
   * Given an E-way bill Regenerate Consolidate Ewb request body, fetch the API response for E-way bill Regenerate Consolidate Ewb API
   * (Using Builder)
   */
  private String regenerateConsolidateEwb(String body, FlynnVersion flynnVer) throws IOException {

      FlynnEwbOperations ewbOperation = null;

      switch (flynnVer) {
          case ONE_OH:
              ewbOperation = FlynnEwbOperations.RegenConsolidateVOneOh;
              break;
          case TWO_OH:
              ewbOperation = FlynnEwbOperations.RegenConsolidateVTwoOh;
              break;
          case THREE_OH:
              ewbOperation = FlynnEwbOperations.RegenConsolidateVThreeOh;
              break;
      }

    return EntityUtils.toString(
      new FlynnPostEwbRequestBuilder(client, FlynnServiceType.BASIC, flynnVer)
        .setOperation(ewbOperation)
        .setApiVersion(EwbVersion.ONE_OH_THREE)
        .setHeaders(Collections.emptyMap())
        .setBody(body)
        .getResponse()
    );
  }


  /**
   * Given a Update Ewb PartB request body, fetch the API response for update Ewb PartB API
   * (Using Builder)
   */
  private String updateEwbPartB(String body, FlynnVersion flynnVer) throws IOException {

      FlynnEwbOperations ewbOperation = null;

      switch (flynnVer) {
          case ONE_OH:
              ewbOperation = FlynnEwbOperations.UpdatePartBVOneOh;
              break;
          case TWO_OH:
              ewbOperation = FlynnEwbOperations.UpdatePartBVTwoOh;
              break;
          case THREE_OH:
              ewbOperation = FlynnEwbOperations.UpdatePartBVThreeOh;
              break;
      }

    return EntityUtils.toString(
      new FlynnPostEwbRequestBuilder(client, FlynnServiceType.BASIC, flynnVer)
        .setOperation(ewbOperation)
        .setApiVersion(EwbVersion.ONE_OH_THREE)
        .setHeaders(Collections.emptyMap())
        .setBody(body)
        .getResponse()
    );
  }


  /**
   * Given a Update Ewb Transporter request body, fetch the API response for update Ewb Transporter API
   * (Using Builder)
   */
  private String updateEwbTransporter(String body, FlynnVersion flynnVer) throws IOException {

      FlynnEwbOperations ewbOperation = null;

      switch (flynnVer) {
          case ONE_OH:
              ewbOperation = FlynnEwbOperations.UpdateTransporterVOneOh;
              break;
          case TWO_OH:
              ewbOperation = FlynnEwbOperations.UpdateTransporterVTwoOh;
              break;
          case THREE_OH:
              ewbOperation = FlynnEwbOperations.UpdateTransporterVThreeOh;
              break;
      }

    return EntityUtils.toString(
      new FlynnPostEwbRequestBuilder(client, FlynnServiceType.BASIC, flynnVer)
        .setOperation(ewbOperation)
        .setApiVersion(EwbVersion.ONE_OH_THREE)
        .setHeaders(Collections.emptyMap())
        .setBody(body)
        .getResponse()
    );
  }

  @Test
  public void testInitiateMultiVehicleMovementVTwoOh() throws IOException {

    String ewbRequestBody = EwayBillUtil.getEwbRequestBody("G" + System.currentTimeMillis(), ewbConfig.getGstin(), "29AAACW4202F1ZM", "29AAACW6288M1ZH", "KA51AB1234");
    String createResponse = createEwbNewWay(ewbRequestBody, TWO_OH);
    String ewb = EwayBillUtil.getEwayBillNumber(client, createResponse);
    String initiateMultiVehicleMovementRequestBody = String.format("{\n" +
      "\"ewbNo\": %s,\n" +
      "\"reasonCode\": \"1\",\n" +
      "\"reasonRem\": \"vehicle broke down\",\n" +
      "\"fromPlace\": \"BANGALORE\",\n" +
      "\"fromState\": 29,\n" +
      "\"toPlace\": \"Chennai\",\n" +
      "\"toState\": 33,\n" +
      "\"transMode\": \"1\",\n" +
      "\"totalQuantity\": 33,\n" +
      "\"unitCode\": \"NOS\"\n" +
      "}", ewb);

    String initiateMultiVehicleMovementResponse = initiateMultiVehicle(initiateMultiVehicleMovementRequestBody, TWO_OH);
    assertTrue(initiateMultiVehicleMovementResponse.contains(ewb));
    LOG.info("Got initiate Multiple Vehicle Movement Response " + initiateMultiVehicleMovementResponse);
  }

  @Test
  public void testAddMultiVehicleToGroupVTwoOh() throws IOException {

    String ewbRequestBody = EwayBillUtil.getEwbRequestBody("G" + System.currentTimeMillis(), ewbConfig.getGstin(), "29AAACW4202F1ZM", "29AAACW6288M1ZH", "KA51AB1234");
    String createResponse = createEwbNewWay(ewbRequestBody, TWO_OH);
    String ewb = EwayBillUtil.getEwayBillNumber(client, createResponse);

    String initiateMultiVehicleMovementRequestBody = String.format("{\n" +
      "\"ewbNo\": %s,\n" +
      "\"reasonCode\": \"1\",\n" +
      "\"reasonRem\": \"vehicle broke down\",\n" +
      "\"fromPlace\": \"BANGALORE\",\n" +
      "\"fromState\": 29,\n" +
      "\"toPlace\": \"Chennai\",\n" +
      "\"toState\": 33,\n" +
      "\"transMode\": \"1\",\n" +
      "\"totalQuantity\": 33,\n" +
      "\"unitCode\": \"NOS\"\n" +
      "}", ewb);

    String initiateMultiVehicleMovementResponse = initiateMultiVehicle(initiateMultiVehicleMovementRequestBody, TWO_OH);
    LOG.info("Got initiate Multiple Vehicle Movement Response " + initiateMultiVehicleMovementResponse);

    String addMultiVehicleToGroupRequestBody = String.format("{\n" +
      "      \"ewbNo\": \"%s\",\n" +
      "      \"groupNo\": \"1\",\n" +
      "      \"vehicleNo\": \"PQR1234\",\n" +
      "      \"transDocNo\": \"1234\",\n" +
      "      \"transDocDate\": \"12/10/2017\",\n" +
      "      \"quantity\": 15\n" +
      "    }", ewb);


    String addMultiVehicleToGroupResponse = addMultiVehicle(addMultiVehicleToGroupRequestBody, TWO_OH);
    assertTrue(addMultiVehicleToGroupResponse.contains(ewb));
    LOG.info("Got add Multiple Vehicle To Group Response " + addMultiVehicleToGroupResponse);
  }

  @Test
  public void testCancelEwbVTwoOh() throws IOException {
    String ewbRequestBody = EwayBillUtil.getEwbRequestBody("G" + System.currentTimeMillis(), ewbConfig.getGstin(), "29AAACW4202F1ZM", "29AAACW6288M1ZH", "KA51AB1234");
    String createResponse = createEwbNewWay(ewbRequestBody, TWO_OH);
    String ewb = EwayBillUtil.getEwayBillNumber(client, createResponse);
    String cancelEwbRequestBody = String.format("{\"ewbNo\":%s,\"cancelRsnCode\": 3,\"cancelRmrk\":\"Cancelled the order\"}", ewb);

    String cancelResponse = cancelEwb(cancelEwbRequestBody, TWO_OH);
    assertTrue(cancelResponse.contains(ewb));
    LOG.info("Got Cancel EWB response " + cancelResponse);
  }

  @Test
  public void testChangeMultiVehicleVTwoOh() throws IOException {

    String ewbRequestBody = EwayBillUtil.getEwbRequestBody("G" + System.currentTimeMillis(), ewbConfig.getGstin(), "29AAACW4202F1ZM", "", "KA51AB1234");
    String createResponse = createEwbNewWay(ewbRequestBody, TWO_OH);
    String ewb = EwayBillUtil.getEwayBillNumber(client, createResponse);

    String updateEwbPartBRequestBody = String.format("{\n" +
      "\"ewbNo\": %s,\n" +
      "\"vehicleNo\": \"PQR1234\",\n" +
      "\"fromPlace\": \"BANGALORE\",\n" +
      "\"fromState\": 29,\n" +
      "\"reasonCode\": \"1\",\n" +
      "\"reasonRem\": \"vehicle broke down\",\n" +
      "\"transDocNo\": \"1234\",\n" +
      "\"transDocDate\": \"%s\",\n" +
      "\"transMode\": \"1\",\n" +
      "\"vehicleType\": \"R\"\n" +
      "}", ewb, dateFormat.format(new Date()));

    String updateEwbPartBResponse = updateEwbPartB(updateEwbPartBRequestBody, TWO_OH);
    LOG.info("Got Update Ewb Part B Response " + updateEwbPartBResponse);

    String initiateMultiVehicleMovementRequestBody = String.format("{\n" +
      "\"ewbNo\": %s,\n" +
      "\"reasonCode\": \"1\",\n" +
      "\"reasonRem\": \"vehicle broke down\",\n" +
      "\"fromPlace\": \"BANGALORE\",\n" +
      "\"fromState\": 29,\n" +
      "\"toPlace\": \"Chennai\",\n" +
      "\"toState\": 33,\n" +
      "\"transMode\": \"1\",\n" +
      "\"totalQuantity\": 33,\n" +
      "\"unitCode\": \"NOS\"\n" +
      "}", ewb);

    String initiateMultiVehicleMovementResponse = initiateMultiVehicle(initiateMultiVehicleMovementRequestBody, TWO_OH);
    LOG.info("Got initiate Multiple Vehicle Movement Response " + initiateMultiVehicleMovementResponse);

    String addMultiVehicleToGroupRequestBody = String.format("{\n" +
      "\"ewbNo\": \"%s\",\n" +
      "\"groupNo\": \"1\",\n" +
      "\"vehicleNo\": \"PQR1234\",\n" +
      "\"transDocNo\": \"1234\",\n" +
      "\"transDocDate\": \"%s\",\n" +
      "\"quantity\": 15\n" +
      "}", ewb, dateFormat.format(new Date()));


    String addMultiVehicleToGroupResponse = addMultiVehicle(addMultiVehicleToGroupRequestBody, TWO_OH);
    LOG.info("Got add Multiple Vehicle To Group Response " + addMultiVehicleToGroupResponse);


    String changeMultiVehicleRequestBody = String.format("{\n" +
      "  \"ewbNo\": %s,\n" +
      "  \"groupNo\": 1,\n" +
      "  \"oldvehicleNo\": \"PQR1234\",\n" +
      "  \"newVehicleNo\": \" PQR1235\",\n" +
      "  \"oldTranNo\":\"1234\",\n" +
      "  \"newTranNo\": \"PQR123\",\n" +
      "  \"fromPlace\": \"Lucknow\",\n" +
      "  \"fromState\": \"09\",\n" +
      "  \"reasonCode\": \"1\",\n" +
      "  \"reasonRem\": \"vehicle broke down\"\n" +
      "}", ewb);


    String changeMultiVehicleResponse = changeMultiVehicle(changeMultiVehicleRequestBody, TWO_OH);
    assertTrue(changeMultiVehicleResponse.contains(ewb));
    LOG.info("Got Change Multiple Vehicle Response " + changeMultiVehicleResponse);
  }

  @Test
  public void testConsolidateEwbVTwoOh() throws IOException {

    String ewbRequestBody1 = EwayBillUtil.getEwbRequestBody("G" + System.currentTimeMillis(), ewbConfig.getGstin(), "29AAACW4202F1ZM", "29AAACW6288M1ZH", "KA51AB1234");
    String createResponse1 = createEwbNewWay(ewbRequestBody1, TWO_OH);
    String ewb1 = EwayBillUtil.getEwayBillNumber(client, createResponse1);

    String ewbRequestBody2 = EwayBillUtil.getEwbRequestBody("G" + System.currentTimeMillis(), ewbConfig.getGstin(), "29AAACW4202F1ZM", "29AAACW6288M1ZH", "KA51AB1234");
    String createResponse2 = createEwbNewWay(ewbRequestBody2, TWO_OH);
    String ewb2 = EwayBillUtil.getEwayBillNumber(client, createResponse2);

    String consolidateEwbRequestBody = String.format("{\n" +
      "\"fromPlace\":\"BANGALORE SOUTH\",\n" +
      "\"fromState\":\"29\",\n" +
      "\"vehicleNo\":\"KA12AB1234\",\n" +
      "\"transMode\":\"1\",\n" +
      "\"transDocNo\":\"1234\",\n" +
      "\"transDocDate\":\"%s\",\n" +
      "\"tripSheetEwbBills\":[\n" +
      "{ \"ewbNo\":\"%s\" },\n" +
      "{ \"ewbNo\":\"%s\" }\n" +
      "] }",dateFormat.format(new Date()), ewb1, ewb2);

    String consolidateEwbResponse = consolidateEwb(consolidateEwbRequestBody, TWO_OH);
    LOG.info("Got consolidateEwbResponse " + consolidateEwbResponse);
  }

  @Test
  public void testCreateBasicEwbVTwoOh() throws IOException {
    String ewbRequestBody = EwayBillUtil.getEwbRequestBody("G" + System.currentTimeMillis(), ewbConfig.getGstin(), "29AAACW4202F1ZM", "29AAACW6288M1ZH", "KA51AB1234");
    String createResponse = createEwbNewWay(ewbRequestBody, TWO_OH);
    LOG.info("Got Create EWB response " + createResponse);
  }

  @Test
  public void testGetErrorListVTwoOh() {
    HttpEntity response = new FlynnGetEwbReqBuilder(client, FlynnServiceType.BASIC, TWO_OH)
      .setApiVersion(EwbVersion.ONE_OH_THREE)
      .setOperation(FlynnEwbOperations.GetErrorListVTwoOh)
      .getResponse();

    try {
      LOG.debug(EntityUtils.toString(response));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  public void testGetEwbByConsignorVTwoOh() {
    String docType = "INV";
    String docNumber = "G1694837929749";
    String apiVersion = EwbVersion.ONE_OH_THREE.getVersion();
    String flynnVersion = FlynnVersion.TWO_OH.getVersion();

    String url = String.format("/basic/ewb/%s/%s/ewayapi/GetEwayBillGeneratedByConsigner?docType=%s&docNo=%s", flynnVersion, apiVersion, docType, docNumber);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }


  @Test
  public void testGetEwbByDateVTwoOh() {
    String apiVersion = EwbVersion.ONE_OH_THREE.getVersion();
    String flynnVersion = FlynnVersion.TWO_OH.getVersion();
    String sampleDate = "31/12/2020";
    String url = String.format("/basic/ewb/%s/%s/ewayapi/GetEwayBillsByDate?date=%s", flynnVersion, apiVersion, sampleDate);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  public void testExtendEwbVTwoOh() {
    String ewbNumber = "261010044279";
    String extendEwbRequestBody = String.format("{\"ewbNo\":%s,\"vehicleNo\":\"KA51AB1234\",\"fromPlace\":\"FRAZER TOWN\",\"fromState\":27,\"remainingDistance\":5,\"transDocNo\":\"\",\"transDocDate\":\"\",\"transMode\":\"1\",\"extnRsnCode\":1,\"extnRemarks\":\"Flood\",\"fromPincode\":\"411016\",\"consignmentStatus\":\"M\",\"transitType\":\"\"}", ewbNumber);
    try {
      String extendEwbWayResponse = extendEwbWay(extendEwbRequestBody, TWO_OH);
      LOG.info("Got Extend Create EWB response " + extendEwbWayResponse);
    } catch (Exception ex) {
      Assert.assertTrue(ex.toString().contains("702"));
    }
  }

  @Test
  public void testGetEwbTransporterByDateVTwoOh() {

    String apiVersion = EwbVersion.ONE_OH_THREE.getVersion();
    String flynnVersion = FlynnVersion.TWO_OH.getVersion();
    String sampleDate = "31/12/2020";
    String url = String.format("/basic/ewb/%s/%s/ewayapi/GetEwayBillsForTransporter?date=%s", flynnVersion, apiVersion, sampleDate);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  @Ignore("336 - Could not retrieve transporter data by gstin")
  public void testGetEwbTransporterByGstinAndDateVTwoOh(){
    String gstin = "29AEKPV7203E1Z9";
    String sampleDate = "21/06/2021";
    String apiVersion = EwbVersion.ONE_OH_THREE.getVersion();
    String flynnVersion = FlynnVersion.TWO_OH.getVersion();

    String url = String.format("/basic/ewb/%s/%s/ewayapi/GetEwayBillsForTransporterByGstin?Gen_gstin=%s&date=%s", flynnVersion, apiVersion, gstin, sampleDate);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  @Ignore("336 - Could not retrieve transporter data by gstin")
  public void testGetEwbTransporterByStateCodeAndDateVTwoOh() {
    String stateCode = "29";
    String sampleDate = "21/06/2021";
    String apiVersion = EwbVersion.ONE_OH_THREE.getVersion();
    String flynnVersion = FlynnVersion.TWO_OH.getVersion();
    String url = String.format("/basic/ewb/%s/%s/ewayapi/GetEwayBillsForTransporterByState?stateCode=%s&date=%s", flynnVersion, apiVersion, stateCode, sampleDate);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  public void testGetEwbReportByTransporterAssignedDateVTwoOh() {
    String stateCode = "27";
    String sampleDate = "21/06/2021";
    String apiVersion = EwbVersion.ONE_OH_THREE.getVersion();
    String flynnVersion = FlynnVersion.TWO_OH.getVersion();
    String url = String.format("/basic/ewb/%s/%s/ewayapi/GetEwayBillReportByTransporterassignedDate?stateCode=%s&date=%s", flynnVersion, apiVersion, stateCode, sampleDate);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  public void testGetEwbVTwoOh() {
    String ewbNumber = "231010025316";
    String apiVersion = EwbVersion.ONE_OH_THREE.getVersion();
    String flynnVersion = FlynnVersion.TWO_OH.getVersion();
    String url = String.format("/basic/ewb/%s/%s/ewayapi/GetEwayBill?ewbNo=%s", flynnVersion, apiVersion, ewbNumber);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  public void testGetGstinDetailsVTwoOh() {
    String gstin = "29AKLPM8755F1Z2";
    String apiVersion = EwbVersion.ONE_OH_THREE.getVersion();
    String flynnVersion = FlynnVersion.TWO_OH.getVersion();
    String url = String.format("/basic/ewb/%s/%s/Master/GetGSTINDetails?GSTIN=%s", flynnVersion, apiVersion, gstin);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  @Ignore("216 - For valid HSN api give error")
  public void testGetHsnDetailsVTwoOh() {
    String hsnCode = "1001";
    String apiVersion = EwbVersion.ONE_OH_THREE.getVersion();
    String flynnVersion = FlynnVersion.TWO_OH.getVersion();
    String url = String.format("/basic/ewb/%s/%s/Master/GetHsnDetailsByHsnCode?hsncode=%s", flynnVersion, apiVersion, hsnCode);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  public void testGetOtherPartyEwbVTwoOh() {
    String sampleDate = "31/12/2020";
    String apiVersion = EwbVersion.ONE_OH_THREE.getVersion();
    String flynnVersion = FlynnVersion.TWO_OH.getVersion();
    String url = String.format("/basic/ewb/%s/%s/ewayapi/GetEwayBillsofOtherParty?date=%s", flynnVersion, apiVersion, sampleDate);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  @Ignore("418 - No record found")
  public void testGetRejectedEwbByConsignorVTwoOh() {
    String sampleDate = "31/12/2020";

    String apiVersion = EwbVersion.ONE_OH_THREE.getVersion();
    String flynnVersion = FlynnVersion.TWO_OH.getVersion();

    String url = String.format("/basic/ewb/%s/%s/ewayapi/GetEwayBillsRejectedByOthers?date=%s", flynnVersion, apiVersion, sampleDate);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (Exception ex) {
        Assert.assertTrue(ex.toString().contains("418")); //418 - No record found
    }
  }

  @Test
  @Ignore("328 - Could not retrieve transporter details from gstin")
  public void testGetTransporterDetailsVTwoOh() {
    String transinNumber = "01AALFT1663P1ZU";
    String apiVersion = EwbVersion.ONE_OH_THREE.getVersion();
    String flynnVersion = FlynnVersion.TWO_OH.getVersion();
    String url = String.format("/basic/ewb/%s/%s/Master/GetTransporterDetails?trn_no=%s", flynnVersion, apiVersion, transinNumber);

    try {
      LOG.debug(EntityUtils.toString(client.get(url)));
    } catch (IOException ioe) {
      LOG.error("Error : " + ioe.getMessage());
    }
  }

  @Test
  public void testRegenerateConsolidateEwbVTwoOh() throws IOException {

    String ewbRequestBody1 = EwayBillUtil.getEwbRequestBody("G" + System.currentTimeMillis(), ewbConfig.getGstin(), "29AAACW4202F1ZM", "29AAACW6288M1ZH", "KA51AB1234");
    String createResponse1 = createEwbNewWay(ewbRequestBody1, TWO_OH);
    String ewb1 = EwayBillUtil.getEwayBillNumber(client, createResponse1);

    String ewbRequestBody2 = EwayBillUtil.getEwbRequestBody("G" + System.currentTimeMillis(), ewbConfig.getGstin(), "29AAACW4202F1ZM", "29AAACW6288M1ZH", "KA51AB1234");
    String createResponse2 = createEwbNewWay(ewbRequestBody2, TWO_OH);
    String ewb2 = EwayBillUtil.getEwayBillNumber(client, createResponse2);

    String consolidateEwbRequestBody = String.format("{\n" +
      "\"fromPlace\":\"BANGALORE SOUTH\",\n" +
      "\"fromState\":\"29\",\n" +
      "\"vehicleNo\":\"KA12AB1234\",\n" +
      "\"transMode\":\"1\",\n" +
      "\"transDocNo\":\"1234\",\n" +
      "\"transDocDate\":\"%s\",\n" +
      "\"tripSheetEwbBills\":[\n" +
      "{ \"ewbNo\":\"%s\" },\n" +
      "{ \"ewbNo\":\"%s\" }\n" +
      "] }",dateFormat.format(new Date()), ewb1, ewb2);

    String consolidateEwbResponse = consolidateEwb(consolidateEwbRequestBody, TWO_OH);
    LOG.info("Got consolidateEwbResponse " + consolidateEwbResponse);

    String data = DataMapper.mapperSlash.readTree(consolidateEwbResponse).get("data").textValue();
    String tripSheetNo = DataMapper.mapperSlash.readTree(data).get("cEwbNo").textValue();

    LOG.info("tripSheetNo" +tripSheetNo);

    String regenerateConsolidateEwbRequestBody =String.format("{\n" +
      "  \"tripSheetNo\": %s,\n" +
      "  \"vehicleNo\": \"KA51AB1234\",\n" +
      "  \"fromPlace\":\"Bengaluru\",\n" +
      "  \"fromState\":\"29\",\n" +
      "  \"transDocNo\": \"1234\",\n" +
      "  \"transDocDate\": \"26/04/2018\",\n" +
      "  \"transMode\": \"1\",\n" +
      "  \"reasonCode\":1,\n" +
      "  \"reasonRem\":\"Flood\"\n" +
      "}", tripSheetNo);

    String regenerateConsolidateEwbResponse = regenerateConsolidateEwb(regenerateConsolidateEwbRequestBody, TWO_OH);
    LOG.info("Got Regenerate Consolidate Ewb Response " + regenerateConsolidateEwbResponse);
  }

  @Test
  @Ignore("341 This e-way bill is generated by you and hence you cannot reject it")
  public void testRejectEwbVTwoOh() throws IOException {
    String ewbRequestBody = EwayBillUtil.getEwbRequestBody("G" + System.currentTimeMillis(), ewbConfig.getGstin(), "29AAACW4202F1ZM", "29AAACW6288M1ZH", "KA51AB1234");
    String createResponse = createEwbNewWay(ewbRequestBody, TWO_OH);
    String ewb = EwayBillUtil.getEwayBillNumber(client, createResponse);
    String rejectEwbRequestBody = String.format("{\n" +
      "\"ewbNo\": %s\n" +
      "}", ewb);

    String rejectEwbResponse = rejectEwb(rejectEwbRequestBody, TWO_OH);
    LOG.info("Got reject Ewb Response " + rejectEwbResponse);
  }


  @Test
  public void testUpdateEwbPartBVTwoOh() throws IOException {
    String ewbRequestBody = EwayBillUtil.getEwbRequestBody("G" + System.currentTimeMillis(), ewbConfig.getGstin(), "29AAACW4202F1ZM", "", "KA51AB1234");
    String createResponse = createEwbNewWay(ewbRequestBody, TWO_OH);
    String ewb = EwayBillUtil.getEwayBillNumber(client, createResponse);

    String updateEwbPartBRequestBody = String.format("{\n" +
      "\"ewbNo\": %s,\n" +
      "\"vehicleNo\": \"PQR1234\",\n" +
      "\"fromPlace\": \"BANGALORE\",\n" +
      "\"fromState\": 29,\n" +
      "\"reasonCode\": \"1\",\n" +
      "\"reasonRem\": \"vehicle broke down\",\n" +
      "\"transDocNo\": \"1234\",\n" +
      "\"transDocDate\": \"%s\",\n" +
      "\"transMode\": \"1\",\n" +
      "\"vehicleType\": \"R\"\n" +
      "}", ewb, dateFormat.format(new Date()));

    String updateEwbPartBResponse = updateEwbPartB(updateEwbPartBRequestBody, TWO_OH);
    LOG.info("Got Update Ewb Part B Response " + updateEwbPartBResponse);

  }

  @Test
  public void testUpdateEwbTransporterVTwoOh() throws IOException {
    String ewbRequestBody = EwayBillUtil.getEwbRequestBody("G" + System.currentTimeMillis(), ewbConfig.getGstin(), "29AAACW4202F1ZM", "29AAACW6288M1ZH", "KA51AB1234");
    String createResponse = createEwbNewWay(ewbRequestBody, TWO_OH);
    String ewb = EwayBillUtil.getEwayBillNumber(client, createResponse);
    String updateEwbTransporterRequestBody = String.format("{\n" +
      "\"ewbNo\": %s,\n" +
      "\"transporterId\":\"29AAACW6288M1ZH\"\n" +
      "}", ewb);

    String updateEwbTransporterResponse = updateEwbTransporter(updateEwbTransporterRequestBody, TWO_OH);
    LOG.info("Got Update Ewb Transporter Response " + updateEwbTransporterResponse);
  }

  @Test
  public void testAuth() {
    System.out.println("Done");
  }

}
