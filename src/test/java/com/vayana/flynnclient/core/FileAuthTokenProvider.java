package com.vayana.flynnclient.core;

import com.vayana.flynnclient.types.auth.AuthTokenProvider;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static com.vayana.flynnclient.util.FileUtil.getFileContent;
import static com.vayana.flynnclient.util.FileUtil.getProjectRoot;

/**
 * File auth token provider persists the signed jwt token in the file system.
 *
 * @implNote The saved file is names as SHA1 of the logged-in user and the content of the file is the signed jwt token for this user.
 */
public class FileAuthTokenProvider implements AuthTokenProvider {

  private final Path tokenFilePath;

  public FileAuthTokenProvider(String userEmail) {
    this.tokenFilePath = getTokenFilePath(userEmail);
  }

  @Override
  public boolean isTokenPresent() {
    return Files.exists(tokenFilePath);
  }

  @Override
  public String getSignedToken() {
    return getFileContent(tokenFilePath);
  }

  @Override
  public void saveToken(String token) throws IOException {
    Files.write(tokenFilePath, token.getBytes());
  }

  /**
   * Get the path of the transient token file for the user of the Client Context.
   * The token file is saved using the Sha 1 digest of the logged in user's email.
   *
   * @return
   *    {@link Path} of the transient token file for the user
   */
  public static Path getTokenFilePath(String email) {
    String sha = DigestUtils.sha1Hex(email);

    return Paths.get(getProjectRoot(), "transient", "tokens", sha);
  }
}
