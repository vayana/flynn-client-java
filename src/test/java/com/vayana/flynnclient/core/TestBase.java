package com.vayana.flynnclient.core;

import com.fasterxml.jackson.core.type.TypeReference;
import com.vayana.flynnclient.client.FlynnClient;
import com.vayana.flynnclient.context.ClientContext;
import com.vayana.flynnclient.exception.SerializationException;
import com.vayana.flynnclient.types.api.response.FlynnResponse;
import com.vayana.flynnclient.types.api.response.StatusResponse;
import com.vayana.flynnclient.types.config.ApiServerConfig;
import com.vayana.flynnclient.types.config.FlynnConfig;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static com.vayana.flynnclient.Constants.AES_CRYPTO_TRANSFORM;
import static com.vayana.flynnclient.core.Constants.CONFIG_DIRECTORY;
import static com.vayana.flynnclient.types.headers.FlynnApiHeaders.ENCRYPTED_DATA_HEADER_NAME;
import static com.vayana.flynnclient.types.headers.FlynnApiHeaders.MASTER_SECRET_HEADER_NAME;
import static com.vayana.flynnclient.util.EncryptionUtil.encrypt;
import static com.vayana.flynnclient.util.EncryptionUtil.getEncryptedString;
import static com.vayana.flynnclient.util.FileUtil.getBuildConfig;
import static com.vayana.flynnclient.util.FileUtil.getProjectRoot;

/**
 * Base class for all Test cases.
 * This class provides an {@link ClientContext} object that contains Client specific configs.
 * In order to create the {@link ClientContext}, you would need to pass in a valid {@link FlynnConfig}
 *
 * @see com.vayana.flynnclient.util.FileUtil#getBuildConfig(String, String)
 */
public abstract class TestBase {
  protected FlynnClient client;

  private static final Logger LOG = LoggerFactory.getLogger(TestBase.class);

  /**
   * Sets the {@code client} for Test class
   */
  public TestBase(){
    FlynnConfig flynnConfig = getBuildConfig(CONFIG_DIRECTORY, "config.json");
    FileAuthTokenProvider tokenHandler = new FileAuthTokenProvider(flynnConfig.getClientDetails().getOrgUser().getEmail());

    client = new FlynnClient(flynnConfig, tokenHandler);
  }

  /**
   * Test method that could be used to check if client is able to send encrypted data to Flynn Server.
   *
   * @param rek
   *    the request encryption key used to encrypt the message in header with name {@link com.vayana.flynnclient.types.headers.FlynnApiHeaders#ENCRYPTED_DATA_HEADER_NAME}
   * @param message
   *    the message to encrypt
   * @return
   *    response received from Flynn server
   */
  protected String authenticateServerWithSensitiveData(String rek, String message)
    throws IOException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
    String url = "/authenticate-with-secrets";

    Map<String, String> headers = new HashMap<>();
    headers.put(MASTER_SECRET_HEADER_NAME, encrypt(client.getClientContext().getApiServerConfig().getPublicKey(), rek));
    headers.put(ENCRYPTED_DATA_HEADER_NAME, getEncryptedString(message, rek, AES_CRYPTO_TRANSFORM));
    headers.put("accept", "application/json");

    return EntityUtils.toString(client.get(url, headers));
  }

  /**
   * Test method used to get {@link StatusResponse} corresponding to a reference-id of Long running task
   * @param refId
   *    reference id of long running task
   * @return
   *    {@link StatusResponse} for the given task
   */
  protected FlynnResponse<StatusResponse> getStatus(UUID refId) {
    ApiServerConfig apiServerConfig = client.getClientContext().getApiServerConfig();
    String url = String.format("/enriched/tasks/%s/status/%s", apiServerConfig.getVersion(), refId);

    Map<String, String> headers = new HashMap<>();

    headers.put("accept", "application/json");

    try {
      String statusResponse = EntityUtils.toString(client.get(url, headers));
      return client.getServices().getMapper().readValue(statusResponse,
        new TypeReference<FlynnResponse<StatusResponse>>() {});
    } catch (IOException cause) {
      LOG.error(String.format("err fetching status response. %s", cause));
      throw new SerializationException(String.format("get status response failed. %s", cause.getMessage()), cause);
    }
  }

  /**
   * Test method used to get zipped bytes corresponding to a reference-id of Long running task. The zip will contain result for the long running task.
   *
   * @param refId
   *    reference id of long running task
   * @return
   *    byte array for the given task's result
   */
  protected byte[] getDownloadResponse(UUID refId) {
    ApiServerConfig apiServerConfig = client.getClientContext().getApiServerConfig();

    String url = String.format("/enriched/tasks/%s/download/%s", apiServerConfig.getVersion(), refId);

    try {
      return EntityUtils.toByteArray(client.get(url));
    } catch (IOException cause) {
      LOG.error(String.format("err fetching download response. %s", cause));
      throw new SerializationException(String.format("get download response failed. %s", cause.getMessage()), cause);
    }
  }

  /**
   * Test method used to get result corresponding to a reference-id of Long running task.
   *
   * @param refId
   *    reference id of long running task
   * @return
   *    byte array for the given task's result
   */
  protected String getResultResponse(UUID refId) {
    ApiServerConfig apiServerConfig = client.getClientContext().getApiServerConfig();

    String url = String.format("/enriched/tasks/%s/result/%s", apiServerConfig.getVersion(), refId);

    try {
      return EntityUtils.toString(client.get(url));
    } catch (IOException cause) {
      LOG.error(String.format("err fetching download response. %s", cause));
      throw new SerializationException(String.format("get result response failed. %s", cause.getMessage()), cause);
    }
  }

  protected void saveFiles(String folderName, byte[] zipResponse) throws IOException {
    Paths.get(getProjectRoot(), "transient", "files", folderName).toFile().mkdirs();

    ZipInputStream zipStream = new ZipInputStream(new ByteArrayInputStream(zipResponse));
    ZipEntry entry = null;

    while ((entry = zipStream.getNextEntry()) != null) {

      String entryName = URLEncoder.encode(entry.getName());

      FileOutputStream out = new FileOutputStream(Paths.get(getProjectRoot(), "transient", "files", folderName, entryName).toString());

      byte[] byteBuff = new byte[4096];
      int bytesRead = 0;
      while ((bytesRead = zipStream.read(byteBuff)) != -1)
      {
        out.write(byteBuff, 0, bytesRead);
      }

      out.close();
      zipStream.closeEntry();
    }
    zipStream.close();
  }
}
