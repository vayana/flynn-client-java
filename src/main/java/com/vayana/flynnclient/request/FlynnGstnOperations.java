package com.vayana.flynnclient.request;

import com.vayana.flynnclient.request.types.RequestPath;
import com.vayana.flynnclient.request.types.RequestPathEntry;

/**
 * This class represents GSTN specific operations for both {@link FlynnServiceType#BASIC} and {@link FlynnServiceType#ENRICHED}
 */
public enum FlynnGstnOperations {
  SearchTaxPayer("commonapi", "search"),
  SearchTaxPayerDetailsBulk("search-taxpayers" ),
  DownloadGstr("download-gst-returns"),
  ViewAndTrackReturns("commonapi", "search"),
  RequestOTP("taxpayerapi", "authenticate/OTPREQUEST"),
  AuthToken("taxpayerapi", "authenticate/AUTHTOKEN"),
  RefreshToken("taxpayerapi", "authenticate/REFRESHTOKEN"),
  Logout("taxpayerapi", "authenticate/LOGOUT"),

  Authentication("taxpayerapi", "authenticate"),


  ;

  private final RequestPathEntry requestPathEntry;

  FlynnGstnOperations(String prefix, String suffix) {
    this(prefix, suffix, null);
  }

  FlynnGstnOperations(String enrichedPath) {
    this(null, null, enrichedPath);
  }

  FlynnGstnOperations(String prefix, String suffix, String enrichedPath) {
    this.requestPathEntry = new RequestPathEntry(new RequestPath(prefix, suffix), enrichedPath);
  }

  public RequestPathEntry getRequestPathEntry() {
    return requestPathEntry;
  }
}
