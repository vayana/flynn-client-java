package com.vayana.flynnclient.request;

/**
 * Represents parameters that are part of request path in an API call.
 * For eg.
 * <pre>
 *   commonapi/v1.1/search/{gstin}
 * </pre>
 * Here, gstin would be a Path parameter, since this will be different for each incoming API call.
 */
public enum PathParams {
  EInvoiceNumber("EInvNumber"),
  EwbNumber("EwbNumber"),
  GSTINNumber("GstnNumber"),
  Date("Date"),
  DocNumber("DocNumber"),
  DocType("DocType"),
  StateCode("stateCode"),
  HsnCode("hsnCode"),
  TransinNumber("transinNumber")
  ;

  String paramName;

  public String getParamName() {
    return paramName;
  }

  PathParams(String paramName) {
    this.paramName = paramName;
  }
}
