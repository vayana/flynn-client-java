package com.vayana.flynnclient.request;

public enum EwbVersion {
  ONE_OH_ONE("v1.01"),
  ONE_OH_THREE("v1.03"),
  ONE_THREE("v1.3"),
  NULL("");
  private String version;

  EwbVersion(String version) {
    this.version = version;
  }

  public String getVersion() {
    return version;
  }
}
