package com.vayana.flynnclient.request;

public enum EInvoiceVersion {
  ONE_OH_ONE("v1.01"),
  ONE_OH_THREE("v1.03"),
  ONE_THREE("v1.3"),
  ONE_OH_FOUR("v1.04"),
  NULL("");
  private String version;

  EInvoiceVersion(String version) {
    this.version = version;
  }

  public String getVersion() {
    return version;
  }
}
