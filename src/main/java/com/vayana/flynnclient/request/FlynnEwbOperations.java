package com.vayana.flynnclient.request;

import com.vayana.flynnclient.request.types.RequestPath;
import com.vayana.flynnclient.request.types.RequestPathEntry;

/**
 * This class represents E-way bill specific operations for both {@link FlynnServiceType#BASIC} and {@link FlynnServiceType#ENRICHED}
 */
public enum FlynnEwbOperations {
  Authenticate("", "authenticate"),
  NewAuthenticate("", "auth"),

  VerifyCredentialsVOneOh("","gen-ewb","eway-bills/verify-ewb-credential"),
  GenerateEwbVOneOh("", "gen-ewb", "eway-bills/generate-ewb"),
  GenerateEwbVTwoOh("", "ewayapi/GENEWAYBILL", "eway-bills/generate-ewb"),
  GenerateEwbVThreeOh("", "ewayapi/gen-ewb", "eway-bills/generate-ewb"),
  UpdatePartBVOneOh("", "update-part-b"),
  UpdatePartBVTwoOh("", "/ewayapi/VEHEWB"),
  UpdatePartBVThreeOh("", "/ewayapi/update-part-b"),
  CancelEwbVOneOh("", "cancel"),
  CancelEwbVTwoOh("", "ewayapi/CANEWB"),
  CancelEwbVThreeOh("", "ewayapi/cancel"),
  UpdateTransporterVOneOh("", "update-transporter"),
  UpdateTransporterVTwoOh("", "ewayapi/UPDATETRANSPORTER"),
  UpdateTransporterVThreeOh("", "ewayapi/update-transporter"),
  ExtendValidityVOneOh("", "extend-validity"),
  ExtendValidityVTwoOh("", "ewayapi/EXTENDVALIDITY"),
  ExtendValidityVThreeOh("", "ewayapi/extend-validity"),
  RejectEwbVOneOh("", "reject"),
  RejectEwbVTwoOh("", "ewayapi/REJEWB"),

  RejectEwbVThreeOh("", "ewayapi/reject"),

  InitiateMultiVehicleVOneOh("", "initiate-multi-vehicle"),
  InitiateMultiVehicleVTwoOh("", "ewayapi/MULTIVEHMOVINT"),
  InitiateMultiVehicleVThreeOh("", "ewayapi/initiate-multi-vehicle"),
  AddMultiVehicleVOneOh("", "add-multi-vehicle"),

  AddMultiVehicleVTwoOh("", "ewayapi/MULTIVEHADD"),

  AddMultiVehicleVThreeOh("", "ewayapi/add-multi-vehicle"),
  ChangeMultiVehicleVOneOh("", "change-multi-vehicle"),
  ChangeMultiVehicleVTwoOh("", "ewayapi/MULTIVEHUPD"),
  ChangeMultiVehicleVThreeOh("", "ewayapi/change-multi-vehicle"),

  ConsolidateVOneOh("", "consolidate"),
  ConsolidateVTwoOh("", "ewayapi/GENCEWB"),
  ConsolidateVThreeOh("", "ewayapi/consolidate"),
  RegenConsolidateVOneOh("", "regen-consolidated"),
  RegenConsolidateVTwoOh("", "ewayapi/REGENTRIPSHEET"),
  RegenConsolidateVThreeOh("", "ewayapi/regen-consolidated"),


  GetEwb("", ""),
  GetEwbByDate("", "by-date"),
  GetEwbAsPdf( "eway-bills/download-pdf"),
  GetErrorListVOneOh("","error-list"),
  GetErrorListVTwoOh("","Master/GetErrorList"),
  getGstinDetails("","gstin-details"),
  GetEwbForTransporterByDateVOneOh("", "for-transporter-by-date"),
  GetEwbForTransporterByDateVTwoOh("", "ewayapi/GetEwayBillsByDate"),
  GetEwbByConsignor("","by-consigner"),
  GetEwbForTransporterByGstinAndDate("","for-transporter-by-gstin-and-date"),
  GetEwbForTransporterByStateCodeAndDate("","for-transporter-by-statecode-and-date"),
  GetEwbReportByTransporterAssignedDate("","for-transporter-by-transporterassigneddate-and-statecode"),
  GetHsnDetails("","hsn-details"),
  GetOtherPartyEwb("","other-party"),
  GetRejectedEwbByConsignor("","rejected-by-date"),
  GetTransporterDetails("","transin-details")
  ;

  private final RequestPathEntry requestPathEntry;

  FlynnEwbOperations(String prefix, String suffix) {
    this(prefix, suffix, null);
  }

  FlynnEwbOperations(String enrichedPath) {
    this(null, null, enrichedPath);
  }

  FlynnEwbOperations(String prefix, String suffix, String enrichedPath) {
    this.requestPathEntry = new RequestPathEntry(new RequestPath(prefix, suffix), enrichedPath);
  }

  public RequestPathEntry getRequestPathEntry() {
    return requestPathEntry;
  }
}
