package com.vayana.flynnclient.request.types;

import com.vayana.flynnclient.request.FlynnServiceType;

/**
 * {@link RequestPathEntry} Signified {@link FlynnServiceType#BASIC} and {@link FlynnServiceType#ENRICHED} service paths
 * for a specific operation.
 */
public class RequestPathEntry {
  private final RequestPath basic;
  private final String enriched;

  public RequestPathEntry(RequestPath basic, String enriched) {
    this.basic = basic;
    this.enriched = enriched;
  }

  public RequestPath getBasic() {
    return basic;
  }

  public String getEnriched() {
    return enriched;
  }
}
