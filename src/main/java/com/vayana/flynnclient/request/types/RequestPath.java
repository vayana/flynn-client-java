package com.vayana.flynnclient.request.types;

public class RequestPath {
  private String prefix;
  private String suffix;

  public RequestPath(String prefix, String suffix) {
    this.prefix = prefix;
    this.suffix = suffix;
  }

  public String getPrefix() {
    return prefix;
  }

  public String getSuffix() {
    return suffix;
  }
}
