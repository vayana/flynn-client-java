package com.vayana.flynnclient.request;

import com.vayana.flynnclient.request.types.RequestPath;
import com.vayana.flynnclient.request.types.RequestPathEntry;

import static com.vayana.flynnclient.Constants.*;

/**
 * This class represents E-invoice specific operations for both {@link FlynnServiceType#BASIC} and {@link FlynnServiceType#ENRICHED}
 */
public enum FlynnEInvoiceOperations {
  Authenticate(EI_VITAL, "auth"),

  GenerateEInvoice(EI_CORE, "Invoice", "invoices"),
  GenerateIrnWithNotification(EI_CORE, "Invoice", "invoice"),
  CancelEInvoice(EI_CORE, "Invoice/Cancel", "invoices/cancel"),
  VerifyCredentialsVOneOh(EI_CORE, "Invoice", "verify-credential"),
  GenerateEWB(EI_EWB, "ewaybill", "invoices/generate-ewb"),
  CancelEwbVOneOh("", "ewayapi/cancel", "invoices/cancel-ewb"),
  CancelEwbVTwoOh("", "ewayapi/CANEWB", "invoices/cancel-ewb"),
  CancelEwbVThreeOh("", "ewayapi/cancel", "invoices/cancel-ewb"),
  ExtendEwbVOneOh("", "ewayapi/extend"),
  ExtendEwbVTwoOh("", "ewayapi/EXTENDVALIDITY"),
  ExtendEwbVThreeOh("", "ewayapi/extend"),
  GetErrorListVOneOh("", "ewayapi/error-list"),
  GetErrorListVTwoOh("", "Master/GetErrorList"),
  UpdatePartBvOneOh("", "ewayapi/update-part-b"),
  UpdatePartBvTwoOh("", "ewayapi/VEHEWB"),
  UpdatePartBvThreeOh("", "ewayapi/update-part-b"),

  GetEInvoice(EI_CORE, "Invoice/irn"),
  GetEInvoiceByDocDetails(EI_CORE, "Invoice/irnbydocdtls"),
  GetGstinDetails(EI_VITAL, "Master/gstin"),
  SyncGstin(EI_VITAL, "Master/syncgstin"),
  GetEwbDetailsByIrn(EI_EWB, "ewaybill/irn"),
  GetEInvoiceAsPDF("invoices/download-pdf"),
  GetEwbAsPDF("ewayapi/download-pdf"),
  GetIrpEwbDetails("", "ewayapi"),
  GetEwbByDate("", "ewayapi/by-date"),
  GetEwbForTransporterByDate("", "ewayapi/for-transporter-by-date"),
  ;

  private final RequestPathEntry requestPathEntry;

  FlynnEInvoiceOperations(String prefix, String suffix) {
    this(prefix, suffix, null);
  }

  FlynnEInvoiceOperations(String enrichedPath) {
    this(null, null, enrichedPath);
  }

  FlynnEInvoiceOperations(String prefix, String suffix, String enrichedPath) {
    this.requestPathEntry = new RequestPathEntry(new RequestPath(prefix, suffix), enrichedPath);
  }

  public RequestPathEntry getRequestPathEntry() {
    return requestPathEntry;
  }
}
