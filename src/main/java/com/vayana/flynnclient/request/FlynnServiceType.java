package com.vayana.flynnclient.request;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Represents the type of Service in `Flynn` Enriched Api Services
 */
public enum FlynnServiceType {
  BASIC("basic"),
  ENRICHED("enriched");

  private final String name;

  FlynnServiceType(String name) {
    this.name = name;
  }

  @JsonValue
  public String getName() {
    return name;
  }
}
