package com.vayana.flynnclient.request.urlbuilder;

import com.vayana.flynnclient.client.FlynnClient;
import com.vayana.flynnclient.request.FlynnModuleType;
import com.vayana.flynnclient.request.FlynnServiceType;
import com.vayana.flynnclient.request.FlynnVersion;

public abstract class UrlBuilder {
  protected FlynnServiceType type;
  protected FlynnVersion version;
  protected FlynnClient client;

  public UrlBuilder(FlynnClient client, FlynnServiceType type, FlynnVersion version) {
    this.client = client;
    this.type = type;
    this.version = version;
  }
}
