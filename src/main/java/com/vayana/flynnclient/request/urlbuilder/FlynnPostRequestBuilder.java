package com.vayana.flynnclient.request.urlbuilder;

import org.apache.http.HttpEntity;

public interface FlynnPostRequestBuilder {

  <T> T setBody(String body);

  HttpEntity getResponse();
}
