package com.vayana.flynnclient.request.urlbuilder;

import com.vayana.flynnclient.request.PathParams;
import com.vayana.flynnclient.request.urlbuilder.impl.FlynnGetEwbReqBuilder;

import java.util.Map;

public interface FlynnGetEwbRequestBuilder {
  FlynnGetEwbReqBuilder setParam(Map<PathParams, String> params);
}
