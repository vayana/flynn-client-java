package com.vayana.flynnclient.request.urlbuilder;

import com.vayana.flynnclient.client.FlynnClient;
import com.vayana.flynnclient.request.*;

import java.util.Collections;
import java.util.Map;

public abstract class FlynnEwbRequestBuilder extends UrlBuilder {

  protected FlynnEwbOperations operation;
  protected EwbVersion apiVersion = EwbVersion.NULL;
  protected Map<String, String> headers = Collections.emptyMap();

  public FlynnEwbRequestBuilder(FlynnClient client, FlynnServiceType type, FlynnVersion version) {
    super(client, type, version);
  }
}
