package com.vayana.flynnclient.request.urlbuilder;

import com.vayana.flynnclient.client.FlynnClient;
import com.vayana.flynnclient.request.*;
import com.vayana.flynnclient.types.enums.IrpProvider;

import java.util.Collections;
import java.util.Map;

public abstract class FlynnEInvRequestBuilder extends UrlBuilder {
  protected FlynnEInvoiceOperations operation;
  protected EInvoiceVersion apiVersion = EInvoiceVersion.NULL;
  protected IrpProvider irpProvider = IrpProvider.NIC;
  protected Map<String, String> headers = Collections.emptyMap();

  public FlynnEInvRequestBuilder(FlynnClient client, FlynnServiceType type, FlynnVersion version) {
    super(client, type, version);
  }
}
