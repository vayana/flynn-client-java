package com.vayana.flynnclient.request.urlbuilder.impl;

import com.vayana.flynnclient.client.FlynnClient;
import com.vayana.flynnclient.exception.SerializationException;
import com.vayana.flynnclient.request.*;
import com.vayana.flynnclient.request.types.RequestPath;
import com.vayana.flynnclient.request.urlbuilder.FlynnEInvRequestBuilder;
import com.vayana.flynnclient.request.urlbuilder.FlynnPostRequestBuilder;
import com.vayana.flynnclient.types.enums.IrpProvider;
import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

import static com.vayana.flynnclient.util.RequestBuilderUtil.*;

public class FlynnPostEInvRequestBuilder extends FlynnEInvRequestBuilder implements FlynnPostRequestBuilder {

  private static final Logger LOG = LoggerFactory.getLogger(FlynnPostRequestBuilder.class);

  private String body = "";

  public FlynnPostEInvRequestBuilder(FlynnClient client, FlynnServiceType type, FlynnVersion version) {
    super(client, type, version);
  }

  public FlynnPostEInvRequestBuilder setOperation(FlynnEInvoiceOperations operation) {
    this.operation = operation;
    return this;
  }

  public FlynnPostEInvRequestBuilder setIrpProvider(IrpProvider irpProvider) {
    this.irpProvider = irpProvider;
    return this;
  }

  public FlynnPostEInvRequestBuilder setApiVersion(EInvoiceVersion apiVersion) {
    this.apiVersion = apiVersion;
    return this;
  }

  public FlynnPostEInvRequestBuilder setHeaders(Map<String, String> headers) {
    this.headers = headers;
    return this;
  }

  @Override
  public FlynnPostEInvRequestBuilder setBody(String body) {
    this.body = body;
    return this;
  }

  public HttpEntity getResponse() {
    StringBuilder urlBuilder = new StringBuilder();

    add(urlBuilder, type.getName());
    add(urlBuilder, FlynnModuleType.EINVOICE.getName());
    add(urlBuilder, version.getVersion());
    add(urlBuilder, irpProvider.toString());

    addTypeSpecificPath(urlBuilder, type, apiVersion.getVersion(), operation.getRequestPathEntry());

    try {
      return client.post(urlBuilder.toString(), this.headers, this.body);
    } catch (IOException cause) {
      LOG.error(String.format("err fetching create E-inv response. %s", cause));
      throw new SerializationException(String.format("Error fetching create E-inv response. %s", cause.getMessage()), cause);
    }
  }


}
