package com.vayana.flynnclient.request.urlbuilder;

import com.vayana.flynnclient.request.PathParams;
import com.vayana.flynnclient.request.urlbuilder.impl.FlynnGetGstnRequestBuilder;
import org.apache.http.HttpEntity;

import java.util.Map;

public interface FlynnGetGstnReqBuilder {

  FlynnGetGstnRequestBuilder setParam(Map<PathParams, String> params);

  HttpEntity getResponse();
}
