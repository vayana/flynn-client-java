package com.vayana.flynnclient.request.urlbuilder;

import com.vayana.flynnclient.client.FlynnClient;
import com.vayana.flynnclient.request.*;

import java.util.Collections;
import java.util.Map;

public abstract class FlynnGstnRequestBuilder extends UrlBuilder {

  protected FlynnGstnOperations operation;
  protected GstnVersion apiVersion = GstnVersion.EMPTY;
  protected Map<String, String> headers = Collections.emptyMap();

  public FlynnGstnRequestBuilder(FlynnClient client, FlynnServiceType type, FlynnVersion version) {
    super(client, type, version);
  }
}
