package com.vayana.flynnclient.request.urlbuilder.impl;

import com.vayana.flynnclient.client.FlynnClient;
import com.vayana.flynnclient.exception.SerializationException;
import com.vayana.flynnclient.request.*;
import com.vayana.flynnclient.request.urlbuilder.FlynnEwbRequestBuilder;
import com.vayana.flynnclient.request.urlbuilder.FlynnPostRequestBuilder;
import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

import static com.vayana.flynnclient.util.RequestBuilderUtil.*;

public class FlynnPostEwbRequestBuilder extends FlynnEwbRequestBuilder implements FlynnPostRequestBuilder {

  private static final Logger LOG = LoggerFactory.getLogger(FlynnPostEwbRequestBuilder.class);

  String body = "";

  public FlynnPostEwbRequestBuilder(FlynnClient client, FlynnServiceType type, FlynnVersion version) {
    super(client, type, version);
  }

  @Override
  public FlynnPostEwbRequestBuilder setBody(String body) {
    this.body = body;
    return this;
  }

  public FlynnPostEwbRequestBuilder setOperation(FlynnEwbOperations operation) {
    this.operation = operation;
    return this;
  }

//  public FlynnPostEwbRequestBuilder setIrpProvider(IrpProvider irpProvider) {
//    this.irpProvider = irpProvider;
//    return this;
//  }

  public FlynnPostEwbRequestBuilder setApiVersion(EwbVersion apiVersion) {
    this.apiVersion = apiVersion;
    return this;
  }

  public FlynnPostEwbRequestBuilder setHeaders(Map<String, String> headers) {
    this.headers = headers;
    return this;
  }

  public HttpEntity getResponse() {
    StringBuilder urlBuilder = new StringBuilder();

    add(urlBuilder, type.getName());
    add(urlBuilder, FlynnModuleType.EWAYBILL.getName());
    add(urlBuilder, version.getVersion());

    addTypeSpecificPath(urlBuilder, type, apiVersion.getVersion(), operation.getRequestPathEntry());

    try {
      return client.post(urlBuilder.toString(), this.headers, this.body);
    } catch (IOException cause) {
      LOG.error(String.format("err fetching create E-inv response. %s", cause));
      throw new SerializationException(String.format("Error fetching create E-inv response. %s", cause.getMessage()), cause);
    }
  }
}
