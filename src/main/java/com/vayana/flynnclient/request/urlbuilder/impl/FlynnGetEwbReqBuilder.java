package com.vayana.flynnclient.request.urlbuilder.impl;

import com.vayana.flynnclient.client.FlynnClient;
import com.vayana.flynnclient.exception.SerializationException;
import com.vayana.flynnclient.request.*;
import com.vayana.flynnclient.request.urlbuilder.FlynnEwbRequestBuilder;
import com.vayana.flynnclient.request.urlbuilder.FlynnGetEwbRequestBuilder;
import org.apache.http.HttpEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import static com.vayana.flynnclient.util.RequestBuilderUtil.*;

public class FlynnGetEwbReqBuilder extends FlynnEwbRequestBuilder implements FlynnGetEwbRequestBuilder {

  private static final Logger LOG = LoggerFactory.getLogger(FlynnGetEwbReqBuilder.class);

  Map<PathParams, String> params = Collections.emptyMap();

  public FlynnGetEwbReqBuilder(FlynnClient client, FlynnServiceType type) {
    super(client, type, FlynnVersion.ONE_OH);
  }

  public FlynnGetEwbReqBuilder(FlynnClient client, FlynnServiceType type, FlynnVersion version) {
    super(client, type, version);
  }

  @Override
  public FlynnGetEwbReqBuilder setParam(Map<PathParams, String> param) {
    this.params = param;
    return this;
  }

  public FlynnGetEwbReqBuilder setOperation(FlynnEwbOperations operation) {
    this.operation = operation;
    return this;
  }

  public FlynnGetEwbReqBuilder setApiVersion(EwbVersion apiVersion) {
    this.apiVersion = apiVersion;
    return this;
  }

  public FlynnGetEwbReqBuilder setHeaders(Map<String, String> headers) {
    this.headers = headers;
    return this;
  }


  public HttpEntity getResponse() {
    StringBuilder urlBuilder = new StringBuilder();

    add(urlBuilder, type.getName());
    add(urlBuilder, FlynnModuleType.EWAYBILL.getName());
    add(urlBuilder, version.getVersion());

    addTypeSpecificPath(urlBuilder, type, apiVersion.getVersion(), operation.getRequestPathEntry());

    params.forEach((key, value) -> addIfNotEmpty(urlBuilder, value));

    try {
      return client.get(urlBuilder.toString(), this.headers);
    } catch (IOException cause) {
      LOG.error(String.format("err fetching create E-inv response. %s", cause));
      throw new SerializationException(String.format("Error fetching get E-inv response. %s", cause.getMessage()), cause);
    }
  }
}

