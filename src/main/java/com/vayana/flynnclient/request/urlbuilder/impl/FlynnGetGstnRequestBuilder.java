package com.vayana.flynnclient.request.urlbuilder.impl;

import com.vayana.flynnclient.client.FlynnClient;
import com.vayana.flynnclient.exception.SerializationException;
import com.vayana.flynnclient.request.*;
import com.vayana.flynnclient.request.urlbuilder.FlynnGetGstnReqBuilder;
import com.vayana.flynnclient.request.urlbuilder.FlynnGstnRequestBuilder;
import org.apache.http.HttpEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import static com.vayana.flynnclient.util.RequestBuilderUtil.*;

public class FlynnGetGstnRequestBuilder extends FlynnGstnRequestBuilder implements FlynnGetGstnReqBuilder {

  private static final Logger LOG = LoggerFactory.getLogger(FlynnGetGstnRequestBuilder.class);

  private Map<PathParams, String> params = Collections.emptyMap();

  public FlynnGetGstnRequestBuilder(FlynnClient client, FlynnServiceType type, FlynnVersion version) {
    super(client, type, version);
  }

  @Override
  public FlynnGetGstnRequestBuilder setParam(Map<PathParams, String> param) {
    this.params = param;
    return this;
  }

  public FlynnGetGstnRequestBuilder setOperation(FlynnGstnOperations operation) {
    this.operation = operation;
    return this;
  }

  public FlynnGetGstnRequestBuilder setApiVersion(GstnVersion apiVersion) {
    this.apiVersion = apiVersion;
    return this;
  }

  public FlynnGetGstnRequestBuilder setHeaders(Map<String, String> headers) {
    this.headers = headers;
    return this;
  }

  public HttpEntity getResponse() {
    StringBuilder urlBuilder = new StringBuilder();

    add(urlBuilder, type.getName());
    add(urlBuilder, FlynnModuleType.GSTN.getName());
    add(urlBuilder, version.getVersion());

    addTypeSpecificPath(urlBuilder, type, apiVersion.getVersion(), operation.getRequestPathEntry());

    params.forEach((key, value) -> addIfNotEmpty(urlBuilder, value));

    try {
      return client.get(urlBuilder.toString(), this.headers);
    } catch (IOException cause) {
      LOG.error(String.format("err fetching create GSTN response. %s", cause));
      throw new SerializationException(String.format("Error fetching get GSTN response. %s", cause.getMessage()), cause);
    }
  }
}

