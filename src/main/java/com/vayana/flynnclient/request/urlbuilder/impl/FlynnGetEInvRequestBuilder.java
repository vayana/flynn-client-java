package com.vayana.flynnclient.request.urlbuilder.impl;

import com.vayana.flynnclient.client.FlynnClient;
import com.vayana.flynnclient.exception.SerializationException;
import com.vayana.flynnclient.request.*;
import com.vayana.flynnclient.request.urlbuilder.FlynnEInvRequestBuilder;
import com.vayana.flynnclient.request.urlbuilder.FlynnGetEInvoiceRequestBuilder;
import com.vayana.flynnclient.types.enums.IrpProvider;
import org.apache.http.HttpEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import static com.vayana.flynnclient.util.RequestBuilderUtil.*;

public class FlynnGetEInvRequestBuilder extends FlynnEInvRequestBuilder implements FlynnGetEInvoiceRequestBuilder {

  private static final Logger LOG = LoggerFactory.getLogger(FlynnGetEInvRequestBuilder.class);

  private Map<PathParams, String> params = Collections.emptyMap();

  public FlynnGetEInvRequestBuilder(FlynnClient client, FlynnServiceType type) {
    super(client, type, FlynnVersion.ONE_OH);
  }

  public FlynnGetEInvRequestBuilder(FlynnClient client, FlynnServiceType type, FlynnVersion version) {
    super(client, type, version);
  }

  @Override
  public FlynnGetEInvRequestBuilder setParam(Map<PathParams, String> params) {
    this.params = params;
    return this;
  }

  public FlynnGetEInvRequestBuilder setOperation(FlynnEInvoiceOperations operation) {
    this.operation = operation;
    return this;
  }

  public FlynnGetEInvRequestBuilder setIrpProvider(IrpProvider irpProvider) {
    this.irpProvider = irpProvider;
    return this;
  }

  public FlynnGetEInvRequestBuilder setApiVersion(EInvoiceVersion apiVersion) {
    this.apiVersion = apiVersion;
    return this;
  }

  public FlynnGetEInvRequestBuilder setHeaders(Map<String, String> headers) {
    this.headers = headers;
    return this;
  }

  public HttpEntity getResponse() {
    StringBuilder urlBuilder = new StringBuilder();

    add(urlBuilder, type.getName());
    add(urlBuilder, FlynnModuleType.EINVOICE.getName());
    add(urlBuilder, version.getVersion());
    add(urlBuilder, IrpProvider.NIC.getCode());

    addTypeSpecificPath(urlBuilder, type, apiVersion.getVersion(), operation.getRequestPathEntry());

    params.forEach((key, value) -> addIfNotEmpty(urlBuilder, value));

    try {
      return client.get(urlBuilder.toString(), this.headers);
    } catch (IOException cause) {
      LOG.error(String.format("err fetching create E-inv response. %s", cause));
      throw new SerializationException(String.format("Error fetching get E-inv response. %s", cause.getMessage()), cause);
    }
  }
}
