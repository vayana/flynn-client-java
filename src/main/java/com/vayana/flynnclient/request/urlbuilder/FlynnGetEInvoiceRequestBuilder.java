package com.vayana.flynnclient.request.urlbuilder;

import com.vayana.flynnclient.request.PathParams;
import com.vayana.flynnclient.request.urlbuilder.impl.FlynnGetEInvRequestBuilder;

import java.util.Map;

public interface FlynnGetEInvoiceRequestBuilder {
  FlynnGetEInvRequestBuilder setParam(Map<PathParams, String> params);
}
