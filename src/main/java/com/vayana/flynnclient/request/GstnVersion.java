package com.vayana.flynnclient.request;

public enum GstnVersion {
  OH_TWO("v0.2"),
  OH_THREE("v0.3"),
  ONE_OH("v1.0"),
  ONE_OH_ONE("v1.1"),
  ONE_OH_THREE("v1.03"),
  ONE_THREE("v1.3"),
  EMPTY("");
  private final String version;

  GstnVersion(String version) {
    this.version = version;
  }

  public String getVersion() {
    return version;
  }
}
