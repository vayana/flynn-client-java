package com.vayana.flynnclient.request;

import com.fasterxml.jackson.annotation.JsonValue;

public enum FlynnModuleType {
  GSTN("gstn"),
  EWAYBILL("ewb"),
  EINVOICE("einv");

  private final String name;

  private FlynnModuleType(String name) {
    this.name = name;
  }

  @JsonValue
  public String getName() {
    return name;
  }
}
