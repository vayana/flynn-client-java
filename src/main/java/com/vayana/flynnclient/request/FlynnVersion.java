package com.vayana.flynnclient.request;

import com.fasterxml.jackson.annotation.JsonValue;

public enum FlynnVersion {
  ONE_OH("v1.0"),
  TWO_OH("v2.0"),
  THREE_OH("v3.0");

  private final String version;

  FlynnVersion(String version) {
    this.version = version;
  }

  @JsonValue
  public String getVersion() {
    return version;
  }
}
