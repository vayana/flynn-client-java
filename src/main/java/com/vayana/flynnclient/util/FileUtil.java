package com.vayana.flynnclient.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vayana.flynnclient.Constants;
import com.vayana.flynnclient.client.FlynnClient;
import com.vayana.flynnclient.exception.InitializationException;
import com.vayana.flynnclient.exception.InvalidKeyFileException;
import com.vayana.flynnclient.types.config.*;
import com.vayana.flynnclient.types.enums.IrpProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.interfaces.RSAPublicKey;
import java.util.stream.Stream;

import static com.vayana.flynnclient.util.EncryptionUtil.getPublicKey;

public class FileUtil {
  private static final Logger LOG = LoggerFactory.getLogger(FileUtil.class);

  public static FlynnConfig getBuildConfig(String directory, String file) {
    ObjectMapper mapper = DataMapper.mapperSlash;
    Path path = Paths.get(directory, file);
    validateFilePath(path);

    initFolders();

    try {
      JsonBuildConfig jsonBuildConfig = mapper.readValue(path.toFile(), JsonBuildConfig.class);

      FlynnConfig.FlynnConfigBuilder configBuilder = new FlynnConfig.FlynnConfigBuilder(
        jsonBuildConfig.isSandbox(),
        (RSAPublicKey) getPublicKey(jsonBuildConfig.getAuthServerPublicKeyPath()),
        getPublicKey(jsonBuildConfig.getApiServerPublicKeyPath()),
        jsonBuildConfig.getClientDetails()
      );

      jsonBuildConfig.getEinvoiceConfig().ifPresent(configBuilder::setEinvoiceConfig);

      jsonBuildConfig.getEwbConfig().ifPresent(configBuilder::setEwbConfig);

      return configBuilder.build();
    } catch (IOException cause) {
      LOG.error("Error creating Build Config");
      throw new InitializationException(cause.getMessage(), String.format("%s/%s", directory, file), cause);
    }
  }

  public static String getProjectRoot() {
    return System.getProperty("user.dir");
  }

  public static String getFileContent(Path filePath) {
    // Error checking
    validateFilePath(filePath);

    StringBuilder contentBuilder = new StringBuilder();

    try (Stream<String> stream = Files.lines(filePath, StandardCharsets.UTF_8)) {
      stream.forEach(contentBuilder::append);
    } catch (IOException e) {
      LOG.error(String.format("err-fetching-file-content-for-%s", filePath.toString()));
    }

    return contentBuilder.toString();
  }

  public static void validateFilePath(Path path) {
    String filePath = path.toString();

    if (null == filePath || filePath.isEmpty()) {
      throw new InvalidKeyFileException("err-file-path-not-provided", filePath);
    } else if (!Files.exists(path)) {
      throw new InvalidKeyFileException("err-file-doesnt-exist", filePath);
    } else if (!Files.isReadable(path)) {
      throw new InvalidKeyFileException("err-file-not-readable", filePath);
    } else if (!Files.isRegularFile(path)) {
      throw new InvalidKeyFileException("err-not-a-regular-file", filePath);
    } else if (path.toFile().length() == 0) {
      throw new InvalidKeyFileException("err-empty-file-without-content", filePath);
    }
  }

  public static String serialize(FlynnClient client, Object object) throws JsonProcessingException {
    return client.getServices().getMapper().writeValueAsString(object);
  }

  private static void initFolders() {
    Paths.get(getProjectRoot(), Constants.TRANSIENT_DIR, "logs").toFile().mkdirs();
    Paths.get(getProjectRoot(), Constants.TRANSIENT_DIR, "tokens").toFile().mkdirs();
    Paths.get(getProjectRoot(), Constants.TRANSIENT_DIR, "files").toFile().mkdirs();
  }
}
