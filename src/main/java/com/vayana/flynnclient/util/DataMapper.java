package com.vayana.flynnclient.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;

import java.text.SimpleDateFormat;

public class DataMapper {
  private static final SimpleDateFormat dateFormatSlashDDMMYYYY = new SimpleDateFormat("dd/MM/yyyy");
  private static final SimpleDateFormat dateFormatDashYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");

  public static final ObjectMapper mapperSlash = new ObjectMapper();
  public static final ObjectMapper mapperDash = new ObjectMapper();

  static {
    mapperSlash
      .registerModule(new Jdk8Module())
      .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
      .configure(JsonParser.Feature.STRICT_DUPLICATE_DETECTION, true)
      .configure(JsonParser.Feature.IGNORE_UNDEFINED, true)
      .setDateFormat(dateFormatSlashDDMMYYYY);

    mapperDash.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
      .configure(JsonParser.Feature.STRICT_DUPLICATE_DETECTION, true)
      .configure(JsonParser.Feature.IGNORE_UNDEFINED, true)
      .setDateFormat(dateFormatDashYYYYMMDD);
  }
}
