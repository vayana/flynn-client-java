package com.vayana.flynnclient.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.vayana.flynnclient.client.FlynnClient;
import com.vayana.flynnclient.types.auth.response.AuthToken;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;

public class JwtUtil {
  public static AuthToken verifyAndReturnJwtToken(FlynnClient client, String signedToken) throws JsonProcessingException {
    DecodedJWT jwtToken = JWT.decode(signedToken);

    String payload = StringUtils.newStringUtf8(Base64.decodeBase64(jwtToken.getPayload()));
    AuthToken authToken = client.getServices().getMapper().readValue(payload, AuthToken.class);

    JWTVerifier verifier = JWT.require(Algorithm.RSA256(client.getClientContext().getAuthServerConfig().getPublicKey(), null))
      .withIssuer(authToken.getIssuer())
      .build();

    verifier.verify(signedToken);

    return authToken;
  }
}
