package com.vayana.flynnclient.util;

import com.vayana.flynnclient.request.FlynnServiceType;
import com.vayana.flynnclient.request.types.RequestPath;
import com.vayana.flynnclient.request.types.RequestPathEntry;

import java.util.Map;

public class RequestBuilderUtil {
  private static final String SLASH = "/";

  public static void add(StringBuilder builder, String header) {
    builder.append(SLASH);
    builder.append(header);
  }

  public static void addIfNotEmpty(StringBuilder builder, String header) {
    if (!header.isEmpty()) {
      builder.append(SLASH);
      builder.append(header);
    }
  }

  public static void addTypeSpecificPath(StringBuilder builder, FlynnServiceType type, String version, RequestPathEntry requestPathEntry) {
    // TODO: Add Service type specific checks here
    if (type.equals(FlynnServiceType.BASIC)) {
      addIfNotEmpty(builder, requestPathEntry.getBasic().getPrefix());
      addIfNotEmpty(builder, version);
      addIfNotEmpty(builder, requestPathEntry.getBasic().getSuffix());
    } else if (type.equals(FlynnServiceType.ENRICHED)) {
      addIfNotEmpty(builder, requestPathEntry.getEnriched());
    }
  }
}
