package com.vayana.flynnclient.util;

import com.vayana.flynnclient.exception.EncryptionNotPossibleException;
import com.vayana.flynnclient.exception.InitializationException;
import com.vayana.flynnclient.exception.InvalidKeyFileException;
import org.apache.commons.codec.binary.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.stream.Stream;

import static com.vayana.flynnclient.util.FileUtil.validateFilePath;
import static java.util.stream.Collectors.joining;

public final class EncryptionUtil {
  private static final Logger LOG = LoggerFactory.getLogger(EncryptionUtil.class);

  private EncryptionUtil() {
  }

  private static Cipher getAesCypherInstance(String eKey, String transformation, boolean encryptionMode)
    throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
    Cipher cipher = Cipher.getInstance(transformation);
    cipher.init(encryptionMode ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, new SecretKeySpec(eKey.getBytes(StandardCharsets.UTF_8), "AES"));

    return cipher;
  }

  /**
   * Encrypts the {@code message} using the given {@code rek} using AES encryption.
   *
   * @param message        the message to encrypt
   * @param rek            random encryption key used for encryption
   * @param transformation transformation used to get {@link Cipher} instance
   * @return the encrypted message
   */
  public static String getEncryptedString(String message, String rek, String transformation) {
    String encryptedString;
    try {
      Cipher cipher = getAesCypherInstance(rek, transformation, true);

      byte[] encryptedMessage = cipher.doFinal(message.getBytes(StandardCharsets.UTF_8));

      encryptedString = new String(Base64.getEncoder().encode(encryptedMessage)).intern();
    } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException cause) {
      throw new EncryptionNotPossibleException("err-error-encrypting-" + message, cause);
    }

    return encryptedString;
  }

  /**
   * Decrypts the {@code message} using the given {@code rek} using AES encryption.
   *
   * @param message        the message to decrypt
   * @param rek            random encryption key used for encryption
   * @param transformation transformation used to get {@link Cipher} instance
   * @return the decrypted message
   */
  public static String getDecryptedString(String message, String rek, String transformation)
    throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
    Cipher cipher = getAesCypherInstance(rek, transformation, false);


    byte[] encryptedMessage = cipher.doFinal(Base64.getDecoder().decode(message));

    return new String(encryptedMessage);
  }

  /**
   * Converts the pem file at given {@code filePath} to {@link PrivateKey} object
   *
   * @param filePath file path for the private key file
   * @return the {@link PrivateKey} at file path
   */
  public static PrivateKey getPrivateKey(String filePath) {
    String fileContent = getPemFileContent(Paths.get(filePath));

    if (!fileContent.isEmpty()) {
      try {
        return KeyFactory.getInstance("RSA").generatePrivate(
          new X509EncodedKeySpec(Base64.getDecoder().decode(fileContent))
        );
      } catch (InvalidKeySpecException | NoSuchAlgorithmException cause) {
        throw new InitializationException(cause.getMessage(), filePath, cause);
      }
    } else {
      LOG.error(String.format("Key file at path %s is either empty or doesn't exist", filePath));
      throw new InvalidKeyFileException("err-empty-or-null-key-file", filePath);
    }
  }

  /**
   * Converts the pem file at given {@code filePath} to {@link PublicKey} object
   *
   * @param filePath pem file path for the public key file
   * @return the {@link PublicKey} at file path
   */
  public static PublicKey getPublicKey(String filePath) {
    String fileContent = getPemFileContent(Paths.get(filePath));

    try {
      return KeyFactory.getInstance("RSA").generatePublic(
        new X509EncodedKeySpec(Base64.getDecoder().decode(fileContent))
      );
    } catch (InvalidKeySpecException | NoSuchAlgorithmException cause) {
      throw new InitializationException(cause.getMessage(), filePath, cause);
    }
  }

  /**
   * Converts the cert file at given {@code filePath} to {@link PublicKey} object
   *
   * @param filePath cert file path for the public key file
   * @return the {@link PublicKey} at file path
   */
  public static PublicKey loadCertAsPublicKey(String filePath) throws CertificateException, FileNotFoundException {
    Path path = Paths.get(filePath);
    validateFilePath(path);

    return CertificateFactory.getInstance("X509").generateCertificate(new FileInputStream(path.toFile())).getPublicKey();
  }

  /**
   * Encrypt the {@code message} with the given {@link PublicKey}
   *
   * @param key     the {@link PublicKey} object
   * @param message the message to encrypt
   * @return the encrypted message
   */
  public static String encrypt(PublicKey key, String message) {
    String encryptedString;

    try {
      Cipher cipher = Cipher.getInstance("RSA");
      cipher.init(Cipher.ENCRYPT_MODE, key);

      byte[] encryptedMessage = cipher.doFinal(message.getBytes(StandardCharsets.UTF_8));

      encryptedString = new String(Base64.getEncoder().encode(encryptedMessage));
    } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException cause) {
      throw new EncryptionNotPossibleException("err-error-encrypting-" + message, cause);
    }

    return encryptedString;
  }


  /**
   * Given a pem file path, fetch the content of the file
   *
   * @param filePath the {@link Path} of the pem file
   * @return content of the pem file, barring the first and last lines.
   */
  private static String getPemFileContent(Path filePath) {
    // Error checking
    validateFilePath(filePath);

    try (Stream<String> stream = Files.lines(filePath, StandardCharsets.UTF_8)) {
      return stream.map(line -> line
        .replace("-----BEGIN PUBLIC KEY-----", "")
        .replace("-----END PUBLIC KEY-----", "")
        .replace("-----BEGIN RSA PUBLIC KEY-----", "")
        .replace("-----END RSA PUBLIC KEY-----", "")
        .replace("\n", "")
        .replace("\r", "")).collect(joining(""));
    } catch (IOException cause) {
      LOG.error(String.format("Error fetching file content for %s, cause = %s", filePath.toString(), cause.getMessage()));
      throw new InvalidKeyFileException(cause.getMessage(), filePath.toString(), cause);
    }
  }
}
