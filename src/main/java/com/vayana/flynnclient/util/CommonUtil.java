package com.vayana.flynnclient.util;

import com.vayana.flynnclient.context.ClientContext;
import com.vayana.flynnclient.types.config.FlynnConfig;
import com.vayana.flynnclient.types.enums.Environment;

import java.util.UUID;

public class CommonUtil {
  public static final String AUTH_SERVER_SBOX_URL = "https://sandbox.services.vayananet.com/theodore";
  public static final String AUTH_SERVER_PROD_URL = "https://services.vayana.com/theodore";

  /**
   * Get the Auth Server url based on the {@link Environment} mentioned in {@link ClientContext}.
   * Currently supports only {@link Environment#SANDBOX}.
   *
   * @param ac the {@link ClientContext} object
   * @return String representation of the base URL of Auth server
   */
  public static String getAuthBaseUrl(ClientContext ac) {
    return ac.getEnv() == Environment.PRODUCTION ? AUTH_SERVER_PROD_URL : AUTH_SERVER_SBOX_URL;
  }

  /**
   * Get the Auth Server url based on the {@link Environment} mentioned in {@link FlynnConfig}.
   * Currently supports only {@link Environment#SANDBOX}.
   *
   * @param config the {@link FlynnConfig} object
   * @return String representation of the base URL of Auth server
   */
  public static String getAuthBaseUrl(FlynnConfig config) {
    return config.isSandbox() ? AUTH_SERVER_SBOX_URL : AUTH_SERVER_PROD_URL;
  }

  /**
   * Get the API Server url based on the {@link Environment} mentioned in {@link FlynnConfig}.
   *
   * @param config the {@link FlynnConfig} object
   * @return String representation of the base URL of API server
   */
  public static String getApiBaseUrl(FlynnConfig config) {
    return String.format("https://%s.enriched-api.vayana.com", config.isSandbox() ? "solo" : "live");
  }

  /**
   * Generates a random Request Encryption Key(REK).
   *
   * @return String representation of a random rek
   */
  public static String generateRek() {
    return UUID.randomUUID().toString().replaceAll("-", "");
  }
}
