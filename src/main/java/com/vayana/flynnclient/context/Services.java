package com.vayana.flynnclient.context;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Services {
  private ObjectMapper mapper;

  public Services(ObjectMapper mapper) {
    this.mapper = mapper;
  }

  public Services() {
    this(new ObjectMapper());
  }

  public ObjectMapper getMapper() {
    return mapper;
  }
}
