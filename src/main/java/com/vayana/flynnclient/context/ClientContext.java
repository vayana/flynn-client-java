package com.vayana.flynnclient.context;

import com.vayana.flynnclient.types.config.*;
import com.vayana.flynnclient.types.enums.ApiServerVersions;
import com.vayana.flynnclient.types.enums.AuthServerVersions;
import com.vayana.flynnclient.types.enums.Environment;

import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Optional;

import static com.vayana.flynnclient.util.CommonUtil.getApiBaseUrl;
import static com.vayana.flynnclient.util.CommonUtil.getAuthBaseUrl;

public class ClientContext {

  private AuthServerConfig authServerConfig;
  private ApiServerConfig apiServerConfig;
  private ClientDetails clientDetails;
  private Environment env;
  private Optional<EwbConfig> ewbConfig;
  private Optional<EinvoiceConfig> einvoiceConfig;

  public ClientContext(FlynnConfig config) {
    this.authServerConfig = new AuthServerConfig(getAuthBaseUrl(config), AuthServerVersions.ONE, config.getAuthServerPublicKey());

    this.apiServerConfig = new ApiServerConfig(getApiBaseUrl(config), ApiServerVersions.ONE_OH, config.getApiServerPublicKey());

    this.clientDetails = config.getClientDetails();
    this.env = config.isSandbox() ? Environment.SANDBOX : Environment.PRODUCTION;

    ewbConfig = config.getEwbConfig();
    einvoiceConfig = config.getEinvoiceConfig();
  }

  public AuthServerConfig getAuthServerConfig() {
    return authServerConfig;
  }

  public ApiServerConfig getApiServerConfig() {
    return apiServerConfig;
  }

  public ClientDetails getClientDetails() {
    return clientDetails;
  }

  public Environment getEnv() {
    return env;
  }

  public Optional<EwbConfig> getEwbConfig() {
    return ewbConfig;
  }

  public Optional<EinvoiceConfig> getEinvoiceConfig() {
    return einvoiceConfig;
  }
}
