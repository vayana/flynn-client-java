package com.vayana.flynnclient.types.auth;

import com.vayana.flynnclient.client.FlynnClient;

import java.io.IOException;

/**
 * Auth Token Provider is the interface for mechanism, that facilitates the save and fetch of the token irrespective of the saving mechanism used.
 *
 * @see FlynnClient#getAuthToken()
 */
public interface AuthTokenProvider {
  /**
   * Returns true if the token is present
   *
   * @return true if token present, false otherwise
   */
  boolean isTokenPresent();

  /**
   * Provides the jwt string for the saved token
   *
   * @return jwt token string
   */
  String getSignedToken();

  /**
   * Saves the provided token
   *
   * @param token signed jwt token
   */
  void saveToken(String token) throws IOException;
}
