package com.vayana.flynnclient.types.auth.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class AuthToken {
  private String iat;

  private String sub;

  @JsonProperty("iss")
  private String issuer;

  @JsonProperty("exp")
  private Long expiry;

  @JsonProperty("orgs")
  private List<String> orgDetails;

  @JsonProperty("uid")
  private String userId;

  @JsonProperty("name")
  private String name;

  @JsonProperty("eml")
  private String email;

  @JsonProperty("mob")
  private String mobile;

  @JsonProperty("iat")
  private String issuedAt;

  public String getSub() {
    return sub;
  }

  public String getIssuer() {
    return issuer;
  }

  public Long getExpiry() {
    return expiry;
  }

  public List<String> getOrgDetails() {
    return orgDetails;
  }

  public String getUserId() {
    return userId;
  }

  public String getName() {
    return name;
  }

  public String getEmail() {
    return email;
  }

  public String getMobile() {
    return mobile;
  }

  public String getIssuedAt() {
    return issuedAt;
  }
}
