package com.vayana.flynnclient.types.auth.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Represents a DTO for Authentication response received from Theodore Auth Server
 */
public class AuthResponse {
  @JsonProperty("data")
  private AuthenticatedUserInfoResp data;

  public AuthenticatedUserInfoResp getData() {
    return data;
  }

  public void setData(AuthenticatedUserInfoResp data) {
    this.data = data;
  }
}

