package com.vayana.flynnclient.types.auth.response;

import java.util.List;

public class OrgResponse {
  private String id;
  private String name;
  private String taxIdentifier;
  private String taxIdentifierType;
  private String country;
  private String active;
  private List<ServiceResponse> services;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getTaxIdentifier() {
    return taxIdentifier;
  }

  public void setTaxIdentifier(String taxIdentifier) {
    this.taxIdentifier = taxIdentifier;
  }

  public String getTaxIdentifierType() {
    return taxIdentifierType;
  }

  public void setTaxIdentifierType(String taxIdentifierType) {
    this.taxIdentifierType = taxIdentifierType;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getActive() {
    return active;
  }

  public void setActive(String active) {
    this.active = active;
  }

  public List<ServiceResponse> getServices() {
    return services;
  }

  public void setServices(List<ServiceResponse> services) {
    this.services = services;
  }
}
