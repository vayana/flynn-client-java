package com.vayana.flynnclient.types.auth.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vayana.flynnclient.client.FlynnClient;

/**
 * {@link AuthReq} is the DTO for Auth API call to Authentication server.
 * The {@code handleType} value is either "email" or "mobile" and {@code handle} is the corresponding value.
 * {@code password} is the associated password for the user.
 *
 * @see FlynnClient#getParsedAuthResponse()
 */
public class AuthReq {
  @JsonProperty("handle")
  private String handle;

  @JsonProperty("handleType")
  private String handleType;

  @JsonProperty("password")
  private String password;

  public AuthReq(String handle, String handleType, String password) {
    this.handle = handle;
    this.handleType = handleType;
    this.password = password;
  }

  public String getHandle() {
    return handle;
  }

  public String getHandleType() {
    return handleType;
  }

  public String getPassword() {
    return password;
  }
}
