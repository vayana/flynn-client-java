package com.vayana.flynnclient.types.auth.response;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import java.util.List;

public class AuthenticatedUserInfoResp {
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss z")
  private Date expiry;
  private String token;
  private String userId;
  private String email;
  private String mobile;
  private String givenName;
  private String lastName;
  private String state;
  private String verificationStatus;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss z")
  private Date passwordLastModified;
  private List<AssociatedOrgInfoResp> associatedOrgs;
  private String timezone;
  private String locale;
  private Date createdOn;
  private Date lastUpdated;

  public Date getExpiry() {
    return expiry;
  }

  public void setExpiry(Date expiry) {
    this.expiry = expiry;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getGivenName() {
    return givenName;
  }

  public void setGivenName(String givenName) {
    this.givenName = givenName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public List<AssociatedOrgInfoResp> getAssociatedOrgs() {
    return associatedOrgs;
  }

  public void setAssociatedOrgs(List<AssociatedOrgInfoResp> associatedOrgs) {
    this.associatedOrgs = associatedOrgs;
  }

  public String getVerificationStatus() {
    return verificationStatus;
  }

  public Date getPasswordLastModified() {
    return passwordLastModified;
  }

  public String getTimezone() {
    return timezone;
  }

  public String getLocale() {
    return locale;
  }

  public Date getCreatedOn() {
    return createdOn;
  }

  public Date getLastUpdated() {
    return lastUpdated;
  }
}
