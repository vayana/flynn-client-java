package com.vayana.flynnclient.types.auth.response;

public class UserAccessInfo {
  private boolean primary;
  private boolean admin;

  public boolean isPrimary() {
    return primary;
  }

  public void setPrimary(boolean primary) {
    this.primary = primary;
  }

  public boolean isAdmin() {
    return admin;
  }

  public void setAdmin(boolean admin) {
    this.admin = admin;
  }
}
