package com.vayana.flynnclient.types.auth.response;

public class AssociatedOrgInfoResp {
  private OrgResponse organisation;
  private UserAccessInfo userAccessInfo;

  public OrgResponse getOrganisation() {
    return organisation;
  }

  public void setOrganisation(OrgResponse organisation) {
    this.organisation = organisation;
  }

  public UserAccessInfo getUserAccessInfo() {
    return userAccessInfo;
  }

  public void setUserAccessInfo(UserAccessInfo userAccessInfo) {
    this.userAccessInfo = userAccessInfo;
  }
}
