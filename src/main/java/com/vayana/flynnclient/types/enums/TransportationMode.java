package com.vayana.flynnclient.types.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum TransportationMode {
  ROAD("1"),
  RAIL("2"),
  AIR("3"),
  SHIP("4");

  private final String code;

  TransportationMode(String code) {
    this.code = code;
  }

  @JsonValue
  public String getCode() {
    return code;
  }

  @Override
  public String toString() {
    return code;
  }
}
