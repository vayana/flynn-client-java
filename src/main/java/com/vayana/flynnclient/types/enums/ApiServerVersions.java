package com.vayana.flynnclient.types.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ApiServerVersions {
  ONE_OH("v1.0");
  private final String version;

  ApiServerVersions(String version) {
    this.version = version;
  }

  @JsonValue
  public String getVersion() {
    return version;
  }

  @Override
  public String toString() {
    return version;
  }
}
