package com.vayana.flynnclient.types.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum VehicleType {
  ODC("O"),
  REGULAR("R");

  private final String code;

  VehicleType(String code) {
    this.code = code;
  }

  @JsonValue
  public String getCode() {
    return code;
  }

  @Override
  public String toString() {
    return code;
  }
}
