package com.vayana.flynnclient.types.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum StateCode {
  OtherCountries("00"),
  JammuAndKashmir("01"),
  HimachalPradesh("02"),
  Punjab("03"),
  Chandigarh("04"),
  Uttarakhand("05"),
  Haryana("06"),
  Delhi("07"),
  Rajasthan("08"),
  UttarPradesh("09"),
  Bihar("10"),
  Sikkim("11"),
  ArunachalPradesh("12"),
  Nagaland("13"),
  Manipur("14"),
  Mizoram("15"),
  Tripura("16"),
  Meghalaya("17"),
  Assam("18"),
  WestBengal("19"),
  Jharkhand("20"),
  Orissa("21"),
  Chhattisgarh("22"),
  MadhyaPradesh("23"),
  Gujarat("24"),
  DamanAndDiu("25"),
  DadarAndNagarHaveli("26"),
  Maharastra("27"),
  Karnataka("29"),
  Goa("30"),
  Lakshadweep("31"),
  Kerala("32"),
  TamilNadu("33"),
  Pondicherry("34"),
  AndamanAndNicobar("35"),
  Telengana("36"),
  AndhraPradesh("37"),
  Ladakh("38"),
  OtherTerritory("97"),
  OtherCountry("96");

  private final String code;

  StateCode(String code) {
    this.code = code;
  }

  @JsonValue
  public String getCode() {
    return code;
  }

  @Override
  public String toString() {
    return code;
  }
}
