package com.vayana.flynnclient.types.enums;

import com.fasterxml.jackson.annotation.JsonValue;

// "Status" will have values as 'ACT' (Active) or 'CNL' (Cancelled) or 'INA' (Inactive) or 'PRO' (Provision). Considered active only if status='ACT'
public enum GstinSyncStatus {
  ACTIVE("ACT"),
  CANCELLED("CNL"),
  INACTIVE("INA"),
  PROVISION("PRO");

  private final String code;

  GstinSyncStatus(String code) {
    this.code = code;
  }

  @JsonValue
  public String getCode() {
    return code;
  }

  @Override
  public String toString() {
    return code;
  }
}




