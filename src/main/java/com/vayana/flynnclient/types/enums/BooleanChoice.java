package com.vayana.flynnclient.types.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum BooleanChoice {
  YES("Y"),
  NO("N");

  @Override
  public String toString() {
    return choice;
  }

  BooleanChoice(String choice) {
    this.choice = choice;
  }

  @JsonValue
  public String getChoice() {
    return choice;
  }

  private final String choice;

}
