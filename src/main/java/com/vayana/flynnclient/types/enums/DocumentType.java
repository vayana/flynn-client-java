package com.vayana.flynnclient.types.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum DocumentType {
  Invoice("INV"),
  CreditNote("CRN"),
  DebitNote("DBN");

  private final String code;

  DocumentType(String code) {
    this.code = code;
  }

  @JsonValue
  public String getCode() {
    return code;
  }

  @Override
  public String toString() {
    return code;
  }

}

