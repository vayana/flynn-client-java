package com.vayana.flynnclient.types.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum AuthServerVersions {
  ONE("v1");

  private final String version;

  AuthServerVersions(String version) {
    this.version = version;
  }

  @JsonValue
  public String getVersion() {
    return version;
  }

  @Override
  public String toString() {
    return version;
  }
}
