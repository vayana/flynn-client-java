package com.vayana.flynnclient.types.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum SupplyType {
  B2B("B2B"),
  SEZWP("SEZWP"),
  SEZWOP("SEZWOP"),
  DEXP("DEXP"),
  EXPWP("EXPWP"),
  EXPWOP("EXPWOP");

  private final String code;

  SupplyType(String code) {
    this.code = code;
  }

  @JsonValue
  public String getCode() {
    return code;
  }

  @Override
  public String toString() {
    return code;
  }

}

