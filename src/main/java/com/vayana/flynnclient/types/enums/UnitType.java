package com.vayana.flynnclient.types.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum UnitType {
  BAG("BAG"),
  BAL("BAL"),
  BDL("BDL"),
  BKL("BKL"),
  BOU("BOU"),
  BOX("BOX"),
  BTL("BTL"),
  BUN("BUN"),
  CAN("CAN"),
  CBM("CBM"),
  CCM("CCM"),
  CMS("CMS"),
  CTN("CTN"),
  DOZ("DOZ"),
  DRM("DRM"),
  GGK("GGK"),
  GMS("GMS"),
  GRS("GRS"),
  GYD("GYD"),
  KGS("KGS"),
  KLR("KLR"),
  KME("KME"),
  LTR("LTR"),
  MTR("MTR"),
  MLT("MLT"),
  MTS("MTS"),
  NOS("NOS"),
  OTH("OTH"),
  PAC("PAC"),
  PCS("PCS"),
  PRS("PRS"),
  QTL("QTL"),
  ROL("ROL"),
  SET("SET"),
  SQF("SQF"),
  SQM("SQM"),
  SQY("SQY"),
  TBS("TBS"),
  TGM("TGM"),
  THD("THD"),
  TON("TON"),
  TUB("TUB"),
  UGS("UGS"),
  UNT("UNT"),
  YDS("YDS");

  private final String code;

  UnitType(String code) {
    this.code = code;
  }

  @JsonValue
  public String getCode() {
    return code;
  }

  @Override
  public String toString() {
    return code;
  }

}
