package com.vayana.flynnclient.types.enums;

import com.fasterxml.jackson.annotation.JsonValue;


public enum EinvoiceSchemaVersions {
  ONE_ONE("1.1");

  @Override
  public String toString() {
    return version;
  }

  private final String version;

  EinvoiceSchemaVersions(String version) {
    this.version = version;
  }

  @JsonValue
  public String getVersion() {
    return version;
    }
}



