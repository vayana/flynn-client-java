package com.vayana.flynnclient.types.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum GenerateIrnPaymentMode {
  CASH("Cash"),
  CASH_CAPS("CASH"),
  CREDIT("Credit"),
  CREDIT_CAPS("CREDIT"),
  DIRECT_TRANSFER("Direct Transfer");

  private final String code;

  GenerateIrnPaymentMode(String code) {
    this.code = code;
  }

  @JsonValue
  public String getCode() {
    return code;
  }

  @Override
  public String toString() {
    return code;
  }
}
