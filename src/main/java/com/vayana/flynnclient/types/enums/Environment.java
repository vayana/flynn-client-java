package com.vayana.flynnclient.types.enums;

public enum Environment {
  SANDBOX, PRODUCTION;
}
