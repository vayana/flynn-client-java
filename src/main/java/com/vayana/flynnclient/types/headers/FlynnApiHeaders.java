package com.vayana.flynnclient.types.headers;

/**
 * Constants for all Flynn API Headers
 */
public final class FlynnApiHeaders {
  public static final String MASTER_SECRET_HEADER_NAME = "X-FLYNN-S-REK";
  public static final String ENCRYPTED_DATA_HEADER_NAME = "X-FLYNN-S-DATA";

  public static final String ORG_IDENTIFIER_HEADER_NAME = "X-FLYNN-N-ORG-ID";
  public static final String USER_IDENTIFIER_HEADER_NAME = "X-FLYNN-N-USER-TOKEN";

  public static final String API_TOKEN_KEY_HEADER_NAME = "X-FLYNN-N-TOKEN-KEY";
  public static final String API_TOKEN_SECRET_HEADER_NAME = "X-FLYNN-S-TOKEN-SECRET";

  public static final String EINVOICE_GSTIN_HEADER_NAME = "X-FLYNN-N-IRP-GSTIN";
  public static final String EINVOICE_USERNAME_HEADER_NAME = "X-FLYNN-N-IRP-USERNAME";
  public static final String EINVOICE_PASSWORD_HEADER_NAME = "X-FLYNN-S-IRP-PWD";
  public static final String EINVOICE_PLAIN_PASSWORD_HEADER_NAME = "X-FLYNN-N-IRP-PWD";
  public static final String EINVOICE_GSP_CODE_HEADER_NAME = "X-FLYNN-N-IRP-GSP-CODE";

  public static final String EWB_GSTIN_HEADER_NAME = "X-FLYNN-N-EWB-GSTIN";
  public static final String EWB_USERNAME_HEADER_NAME = "X-FLYNN-N-EWB-USERNAME";
  public static final String EWB_PASSWORD_HEADER_NAME = "X-FLYNN-S-EWB-PWD";
  public static final String EWB_PLAIN_PASSWORD_HEADER_NAME = "X-FLYNN-N-EWB-PWD";
  public static final String EWB_GSP_CODE_HEADER_NAME = "X-FLYNN-N-EWB-GSP-CODE";

  public static final String GSTIN_HEADER_NAME="X-FLYNN-N-GSTIN";

  public static final String GSTIN_ACTION_NAME="action";

}
