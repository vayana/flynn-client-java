package com.vayana.flynnclient.types.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vayana.flynnclient.types.enums.GspCode;
import com.vayana.flynnclient.types.enums.IrpProvider;

import java.security.PublicKey;

public class EinvoiceConfig {
  @JsonProperty("gstin")
  private String gstin;

  @JsonProperty("username")
  private String username;

  @JsonProperty("password")
  private String password;

  @JsonProperty("gsp_code")
  private GspCode gspCode;

  @JsonProperty("irp_provider")
  private IrpProvider provider;

  private EinvoiceConfig() {
  }

  public EinvoiceConfig(String gstin, String username, String password, GspCode gspCode, IrpProvider provider) {
    this.gstin = gstin;
    this.username = username;
    this.password = password;
    this.gspCode = gspCode;
    this.provider = provider;
  }

  public String getGstin() {
    return gstin;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public GspCode getGspCode() {
    return gspCode;
  }

  public IrpProvider getProvider() {
    return provider;
  }
}
