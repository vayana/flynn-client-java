package com.vayana.flynnclient.types.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vayana.flynnclient.types.enums.GspCode;

import java.util.Optional;

public class JsonBuildConfig {
  @JsonProperty("is_sandbox")
  private boolean isSandbox;

  @JsonProperty("auth_server_public_key_file_path")
  private String authServerPublicKeyPath;

  @JsonProperty("api_server_public_key_file_path")
  private String apiServerPublicKeyPath;

  @JsonProperty("irp_public_key_file_path")
  private Optional<String> irpPublicKeyPath = Optional.empty();

  @JsonProperty("ewb_public_key_file_path")
  private Optional<String> ewbPublicKeyPath = Optional.empty();

  @JsonProperty("client_details")
  private ClientDetails clientDetails;

  @JsonProperty("ewb_details")
  private Optional<EwbConfig> ewbConfig = Optional.empty();

  @JsonProperty("einvoice_details")
  private Optional<EinvoiceConfig> einvoiceConfig = Optional.empty();

  private JsonBuildConfig() {
  }

  public JsonBuildConfig(boolean isSandbox, String authServerPublicKeyPath, String apiServerPublicKeyPath, ClientDetails clientDetails) {
    this.isSandbox = isSandbox;
    this.authServerPublicKeyPath = authServerPublicKeyPath;
    this.apiServerPublicKeyPath = apiServerPublicKeyPath;
    this.clientDetails = clientDetails;
  }

  public JsonBuildConfig(boolean isSandbox, String authServerPublicKeyPath, String apiServerPublicKeyPath, Optional<String> irpPublicKeyPath, Optional<String> ewbPublicKeyPath, ClientDetails clientDetails) {
    this.isSandbox = isSandbox;
    this.authServerPublicKeyPath = authServerPublicKeyPath;
    this.apiServerPublicKeyPath = apiServerPublicKeyPath;
    this.irpPublicKeyPath = irpPublicKeyPath;
    this.ewbPublicKeyPath = ewbPublicKeyPath;
    this.clientDetails = clientDetails;
  }

  public boolean isSandbox() {
    return isSandbox;
  }

  public String getAuthServerPublicKeyPath() {
    return authServerPublicKeyPath;
  }

  public String getApiServerPublicKeyPath() {
    return apiServerPublicKeyPath;
  }

  public Optional<String> getIrpPublicKeyPath() {
    return irpPublicKeyPath;
  }

  public Optional<String> getEwbPublicKeyPath() {
    return ewbPublicKeyPath;
  }

  public ClientDetails getClientDetails() {
    return clientDetails;
  }

  public Optional<EwbConfig> getEwbConfig() {
    return ewbConfig;
  }

  public Optional<EinvoiceConfig> getEinvoiceConfig() {
    return einvoiceConfig;
  }
}
