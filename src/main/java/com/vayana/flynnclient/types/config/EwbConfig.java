package com.vayana.flynnclient.types.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vayana.flynnclient.types.enums.GspCode;
import com.vayana.flynnclient.types.enums.IrpProvider;

import java.security.PublicKey;

public class EwbConfig {
  @JsonProperty("gstin")
  private String gstin;

  @JsonProperty("username")
  private String username;

  @JsonProperty("password")
  private String password;

  @JsonProperty("gsp_code")
  private GspCode gspCode;

  private EwbConfig() {
  }

  public EwbConfig(String gstin, String username, String password, GspCode gspCode) {
    this.gstin = gstin;
    this.username = username;
    this.password = password;
    this.gspCode = gspCode;
  }

  public String getGstin() {
    return gstin;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public GspCode getGspCode() {
    return gspCode;
  }
}
