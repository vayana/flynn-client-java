package com.vayana.flynnclient.types.config;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ClientDetails {
  @JsonProperty("org_id")
  private String orgId;

  @JsonProperty("org_user")
  private OrganisationUser orgUser;

  private ClientDetails() {
  }

  public ClientDetails(String orgId, OrganisationUser orgUser) {
    this.orgId = orgId;
    this.orgUser = orgUser;
  }

  public String getOrgId() {
    return orgId;
  }

  public OrganisationUser getOrgUser() {
    return orgUser;
  }
}
