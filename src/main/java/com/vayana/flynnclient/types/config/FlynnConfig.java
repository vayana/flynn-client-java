package com.vayana.flynnclient.types.config;

import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Optional;

public class FlynnConfig {
  private boolean isSandbox;

  private RSAPublicKey authServerPublicKey;

  private PublicKey apiServerPublicKey;

  private ClientDetails clientDetails;

  private Optional<EwbConfig> ewbConfig = Optional.empty();

  private Optional<EinvoiceConfig> einvoiceConfig = Optional.empty();

  public FlynnConfig(
    boolean isSandbox,
    RSAPublicKey authServerPublicKey,
    PublicKey apiServerPublicKey,
    ClientDetails clientDetails,
    Optional<EwbConfig> ewbConfig,
    Optional<EinvoiceConfig> einvoiceConfig
  ) {
    this.isSandbox = isSandbox;
    this.authServerPublicKey = authServerPublicKey;
    this.apiServerPublicKey = apiServerPublicKey;
    this.clientDetails = clientDetails;
    this.ewbConfig = ewbConfig;
    this.einvoiceConfig = einvoiceConfig;
  }

  public FlynnConfig(
    boolean isSandbox,
    RSAPublicKey authServerPublicKey,
    PublicKey apiServerPublicKey,
    ClientDetails clientDetails
  ) {
    this.isSandbox = isSandbox;
    this.authServerPublicKey = authServerPublicKey;
    this.apiServerPublicKey = apiServerPublicKey;
    this.clientDetails = clientDetails;
  }

  public boolean isSandbox() {
    return isSandbox;
  }

  public RSAPublicKey getAuthServerPublicKey() {
    return authServerPublicKey;
  }

  public PublicKey getApiServerPublicKey() {
    return apiServerPublicKey;
  }

  public ClientDetails getClientDetails() {
    return clientDetails;
  }

  public Optional<EwbConfig> getEwbConfig() {
    return ewbConfig;
  }

  public Optional<EinvoiceConfig> getEinvoiceConfig() {
    return einvoiceConfig;
  }

  public static class FlynnConfigBuilder {
    private boolean isSandbox;

    private RSAPublicKey authServerPublicKey;

    private PublicKey apiServerPublicKey;

    private ClientDetails clientDetails;

    private Optional<EwbConfig> ewbConfig = Optional.empty();

    private Optional<EinvoiceConfig> einvoiceConfig = Optional.empty();

    public FlynnConfigBuilder(boolean isSandbox, RSAPublicKey authServerPublicKey, PublicKey apiServerPublicKey, ClientDetails clientDetails) {
      this.isSandbox = isSandbox;
      this.authServerPublicKey = authServerPublicKey;
      this.apiServerPublicKey = apiServerPublicKey;
      this.clientDetails = clientDetails;
    }

    public FlynnConfigBuilder setEwbConfig(EwbConfig config) {
      this.ewbConfig = Optional.of(config);
      return this;
    }

    public FlynnConfigBuilder setEinvoiceConfig(EinvoiceConfig config) {
      this.einvoiceConfig = Optional.of(config);
      return this;
    }

    public FlynnConfig build() {
      return new FlynnConfig(
        isSandbox, authServerPublicKey, apiServerPublicKey, clientDetails, ewbConfig, einvoiceConfig
      );
    }
  }
}
