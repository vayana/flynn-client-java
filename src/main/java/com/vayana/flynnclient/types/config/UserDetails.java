package com.vayana.flynnclient.types.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vayana.flynnclient.types.enums.GspCode;

public class UserDetails {
  @JsonProperty("gstin")
  private String gstin;

  @JsonProperty("username")
  private String username;

  @JsonProperty("password")
  private String password;

  @JsonProperty("gsp_code")
  private GspCode gspCode;

  public String getGstin() {
    return gstin;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public GspCode getGspCode() {
    return gspCode;
  }
}
