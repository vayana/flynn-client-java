package com.vayana.flynnclient.types.config;

import com.vayana.flynnclient.types.enums.ApiServerVersions;

import java.security.PublicKey;

public class ApiServerConfig {
  private String url;
  private ApiServerVersions version;
  private PublicKey publicKey;

  public ApiServerConfig(String url, ApiServerVersions version, PublicKey publicKey) {
    this.url = url;
    this.version = version;
    this.publicKey = publicKey;
  }

  public String getUrl() {
    return url;
  }

  public ApiServerVersions getVersion() {
    return version;
  }

  public PublicKey getPublicKey() {
    return publicKey;
  }
}
