package com.vayana.flynnclient.types.config;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OrganisationUser {
  @JsonProperty("email")
  private String email;

  @JsonProperty("password")
  private String password;

  private OrganisationUser() {
  }

  public OrganisationUser(String email, String password) {
    this.email = email;
    this.password = password;
  }

  public String getEmail() {
    return email;
  }

  public String getPassword() {
    return password;
  }
}
