package com.vayana.flynnclient.types.config;

import com.vayana.flynnclient.types.enums.AuthServerVersions;

import java.security.interfaces.RSAPublicKey;

public class AuthServerConfig {
  private String url;
  private AuthServerVersions version;
  private RSAPublicKey publicKey;

  public AuthServerConfig(String url, AuthServerVersions version, RSAPublicKey publicKey) {
    this.url = url;
    this.version = version;
    this.publicKey = publicKey;
  }

  public String getUrl() {
    return url;
  }

  public AuthServerVersions getVersion() {
    return version;
  }

  public RSAPublicKey getPublicKey() {
    return publicKey;
  }
}
