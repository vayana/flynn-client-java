package com.vayana.flynnclient.types.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vayana.flynnclient.types.enums.BooleanChoice;
import com.vayana.flynnclient.types.enums.SupplyType;
import com.vayana.flynnclient.types.enums.TaxScheme;

public class TransactionDetails {

  public TaxScheme getTaxScheme() {
    return taxScheme;
  }

  public void setTaxScheme(TaxScheme taxScheme) {
    this.taxScheme = taxScheme;
  }

  public SupplyType getSupplyType() {
    return supplyType;
  }

  public void setSupplyType(SupplyType supplyType) {
    this.supplyType = supplyType;
  }

  public String getReverseCharge() {
    return reverseCharge;
  }

  public void setReverseCharge(String reverseCharge) {
    this.reverseCharge = reverseCharge;
  }

  public BooleanChoice getIgstIntra() {
    return igstIntra;
  }

  public void setIgstIntra(BooleanChoice igstIntra) {
    this.igstIntra = igstIntra;
  }

  public String getEcomGstin() {
    return ecomGstin;
  }

  public void setEcomGstin(String ecomGstin) {
    this.ecomGstin = ecomGstin;
  }

  @JsonProperty("TaxSch")
  TaxScheme taxScheme;

  @JsonProperty("SupTyp")
  SupplyType supplyType;

  @JsonProperty("RegRev")
  String reverseCharge;

  @JsonProperty("IgstOnIntra")
  BooleanChoice igstIntra;

  @JsonProperty("EcmGstin")
  String ecomGstin ;
}


