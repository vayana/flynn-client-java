package com.vayana.flynnclient.types.api.response;

public class JobStatus {
  private int pending;
  private int total;

  public int getPending() {
    return pending;
  }

  public JobStatus setPending(int pending) {
    this.pending = pending;
    return this;
  }

  public int getTotal() {
    return total;
  }

  public JobStatus setTotal(int total) {
    this.total = total;
    return this;
  }
}
