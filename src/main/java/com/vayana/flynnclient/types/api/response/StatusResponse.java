package com.vayana.flynnclient.types.api.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class StatusResponse {
  private UUID id;
  private JobStatus jobs;
  private String status;
  @JsonProperty("last-updated")
  private TaskLastUpdated lastUpdated;

  public UUID getId() {
    return id;
  }

  public StatusResponse setId(UUID id) {
    this.id = id;
    return this;
  }

  public JobStatus getJobs() {
    return jobs;
  }

  public StatusResponse setJobs(JobStatus jobs) {
    this.jobs = jobs;
    return this;
  }

  public String getStatus() {
    return status;
  }

  public StatusResponse setStatus(String status) {
    this.status = status;
    return this;
  }

  public TaskLastUpdated getLastUpdated() {
    return lastUpdated;
  }

  public StatusResponse setLastUpdated(TaskLastUpdated lastUpdated) {
    this.lastUpdated = lastUpdated;
    return this;
  }
}
