package com.vayana.flynnclient.types.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vayana.flynnclient.types.enums.EinvoiceSchemaVersions;
import java.util.List;

public class GenerateIrnReq {

  public EinvoiceSchemaVersions getVersion() {
    return version;
  }

  public void setVersion(EinvoiceSchemaVersions version) {
    this.version = version;
  }

  public String getIrn() {
    return irn;
  }

  public void setIrn(String irn) {
    this.irn = irn;
  }

  public TransactionDetails getTransactionDetails() {
    return transactionDetails;
  }

  public void setTransactionDetails(TransactionDetails transactionDetails) {
    this.transactionDetails = transactionDetails;
  }

  public DocumentDetails getDocumentDetails() {
    return documentDetails;
  }

  public void setDocumentDetails(DocumentDetails documentDetails) {
    this.documentDetails = documentDetails;
  }

  public SellerDetails getSellerDetails() {
    return sellerDetails;
  }

  public void setSellerDetails(SellerDetails sellerDetails) {
    this.sellerDetails = sellerDetails;
  }

  public BuyerDetails getBuyerDetails() {
    return buyerDetails;
  }

  public void setBuyerDetails(BuyerDetails buyerDetails) {
    this.buyerDetails = buyerDetails;
  }

  public ValueDetails getValueDetails() {
    return valueDetails;
  }

  public void setValueDetails(ValueDetails valueDetails) {
    this.valueDetails = valueDetails;
  }

  public DisplayDetails getDisplayDetails() {
    return displayDetails;
  }

  public void setDisplayDetails(DisplayDetails displayDetails) {
    this.displayDetails = displayDetails;
  }

  public ShipmentDetails getShipDetails() {
    return shipDetails;
  }

  public void setShipDetails(ShipmentDetails shipDetails) {
    this.shipDetails = shipDetails;
  }

  public PayeeDetails getPayeeDetails() {
    return payeeDetails;
  }

  public void setPayeeDetails(PayeeDetails payeeDetails) {
    this.payeeDetails = payeeDetails;
  }

  public RefDetails getReferenceDetails() {
    return referenceDetails;
  }

  public void setReferenceDetails(RefDetails referenceDetails) {
    this.referenceDetails = referenceDetails;
  }

  public ExportDetails getExportDetails() {
    return exportDetails;
  }

  public void setExportDetails(ExportDetails exportDetails) {
    this.exportDetails = exportDetails;
  }

  public EwayBillDetails getEwbDetails() {
    return ewbDetails;
  }

  public void setEwbDetails(EwayBillDetails ewbDetails) {
    this.ewbDetails = ewbDetails;
  }

  public List<AdditionalDocDetails> getAdditionalDocDetails() {
    return additionalDocDetails;
  }

  public void setAdditionalDocDetails(List<AdditionalDocDetails> additionalDocDetails) {
    this.additionalDocDetails = additionalDocDetails;
  }

  public List<Items> getItems() {
    return items;
  }

  public void setItems(List<Items> items) {
    this.items = items;
  }

  @JsonProperty("Version")
  private EinvoiceSchemaVersions version;

  @JsonProperty("Irn")
  private  String irn;

  @JsonProperty("TranDtls")
  private  TransactionDetails transactionDetails;

  @JsonProperty("DocDtls")
  private  DocumentDetails documentDetails;

  @JsonProperty("SellerDtls")
  private  SellerDetails sellerDetails;

  @JsonProperty("BuyerDtls")
  private  BuyerDetails buyerDetails;

  @JsonProperty("ValDtls")
  private  ValueDetails valueDetails;

  @JsonProperty("DispDtls")
  private  DisplayDetails displayDetails;

  @JsonProperty("ShipDtls")
  private  ShipmentDetails shipDetails;

  @JsonProperty("PayDtls")
  private  PayeeDetails payeeDetails;

  @JsonProperty("RefDtls")
  private  RefDetails referenceDetails;

  @JsonProperty("ExpDtls")
  private  ExportDetails exportDetails;

  @JsonProperty("EwbDtls")
  private  EwayBillDetails ewbDetails;

  @JsonProperty("AddlDocDtls")
  private  List<AdditionalDocDetails> additionalDocDetails;

  @JsonProperty("ItemList")
  private  List<Items> items;

}

