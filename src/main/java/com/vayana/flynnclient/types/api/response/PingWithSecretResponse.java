package com.vayana.flynnclient.types.api.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PingWithSecretResponse {
  private String message;

  @JsonProperty("client-message")
  private String clientMessage;

  public String getMessage() {
    return message;
  }

  public PingWithSecretResponse setMessage(String message) {
    this.message = message;
    return this;
  }

  public String getClientMessage() {
    return clientMessage;
  }

  public PingWithSecretResponse setClientMessage(String clientMessage) {
    this.clientMessage = clientMessage;
    return this;
  }
}
