package com.vayana.flynnclient.types.api.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class TaskIDResponse {
  @JsonProperty("task-id")
  private UUID refId;

  public UUID getRefId() {
    return refId;
  }

  public void setRefId(UUID refId) {
    this.refId = refId;
  }
}
