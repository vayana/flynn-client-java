package com.vayana.flynnclient.types.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vayana.flynnclient.types.enums.StateCode;

public class SellerDetails {

  public String getSellerGstin() {
    return sellerGstin;
  }

  public void setSellerGstin(String sellerGstin) {
    this.sellerGstin = sellerGstin;
  }

  public String getLegalName() {
    return legalName;
  }

  public void setLegalName(String legalName) {
    this.legalName = legalName;
  }

  public String getTradeName() {
    return tradeName;
  }

  public void setTradeName(String tradeName) {
    this.tradeName = tradeName;
  }

  public String getAddressOne() {
    return addressOne;
  }

  public void setAddressOne(String addressOne) {
    this.addressOne = addressOne;
  }

  public String getAddressTwo() {
    return addressTwo;
  }

  public void setAddressTwo(String addressTwo) {
    this.addressTwo = addressTwo;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public String getPincode() {
    return pincode;
  }

  public void setPincode(String pincode) {
    this.pincode = pincode;
  }

  public StateCode getState() {
    return state;
  }

  public void setState(StateCode state) {
    this.state = state;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @JsonProperty("Gstin")
  String sellerGstin;

  @JsonProperty("LglNm")
  String legalName;

  @JsonProperty("TrdNm")
  String tradeName;

  @JsonProperty("Addr1")
  String addressOne;

  @JsonProperty("Addr2")
  String addressTwo;

  @JsonProperty("Loc")
  String location;

  @JsonProperty("Pin")
  String pincode;

  @JsonProperty("Stcd")
  StateCode state;

  @JsonProperty("Ph")
  String phone;

  @JsonProperty("Em")
  String email;
}
