package com.vayana.flynnclient.types.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vayana.flynnclient.types.enums.BooleanChoice;
import com.vayana.flynnclient.types.enums.CountryCode;
import com.vayana.flynnclient.types.enums.UnitType;

import java.math.BigDecimal;
import java.util.List;

public class Items {
  public BigDecimal getItemNumber() {
    return itemNumber;
  }

  public void setItemNumber(BigDecimal itemNumber) {
    this.itemNumber = itemNumber;
  }

  public String getSerialNumber() {
    return serialNumber;
  }

  public void setSerialNumber(String serialNumber) {
    this.serialNumber = serialNumber;
  }

  public String getProductDescription() {
    return productDescription;
  }

  public void setProductDescription(String productDescription) {
    this.productDescription = productDescription;
  }

  public BooleanChoice getService() {
    return service;
  }

  public void setService(BooleanChoice service) {
    this.service = service;
  }

  public String getHsnCode() {
    return hsnCode;
  }

  public void setHsnCode(String hsnCode) {
    this.hsnCode = hsnCode;
  }

  public BigDecimal getUnitPrice() {
    return unitPrice;
  }

  public void setUnitPrice(BigDecimal unitPrice) {
    this.unitPrice = unitPrice;
  }

  public BigDecimal getTotalAmount() {
    return totalAmount;
  }

  public void setTotalAmount(BigDecimal totalAmount) {
    this.totalAmount = totalAmount;
  }

  public BigDecimal getAssessableAmount() {
    return assessableAmount;
  }

  public void setAssessableAmount(BigDecimal assessableAmount) {
    this.assessableAmount = assessableAmount;
  }

  public BigDecimal getGstRate() {
    return gstRate;
  }

  public void setGstRate(BigDecimal gstRate) {
    this.gstRate = gstRate;
  }

  public BigDecimal getTotalItemAmount() {
    return totalItemAmount;
  }

  public void setTotalItemAmount(BigDecimal totalItemAmount) {
    this.totalItemAmount = totalItemAmount;
  }

  public String getBarCode() {
    return barCode;
  }

  public void setBarCode(String barCode) {
    this.barCode = barCode;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public BigDecimal getFreeQuantity() {
    return freeQuantity;
  }

  public void setFreeQuantity(BigDecimal freeQuantity) {
    this.freeQuantity = freeQuantity;
  }

  public UnitType getUnitType() {
    return unitType;
  }

  public void setUnitType(UnitType unitType) {
    this.unitType = unitType;
  }

  public BigDecimal getDiscountAmount() {
    return discountAmount;
  }

  public void setDiscountAmount(BigDecimal discountAmount) {
    this.discountAmount = discountAmount;
  }

  public BigDecimal getPreTaxValue() {
    return preTaxValue;
  }

  public void setPreTaxValue(BigDecimal preTaxValue) {
    this.preTaxValue = preTaxValue;
  }

  public BigDecimal getSgstAmount() {
    return sgstAmount;
  }

  public void setSgstAmount(BigDecimal sgstAmount) {
    this.sgstAmount = sgstAmount;
  }

  public BigDecimal getCgstAmount() {
    return cgstAmount;
  }

  public void setCgstAmount(BigDecimal cgstAmount) {
    this.cgstAmount = cgstAmount;
  }

  public BigDecimal getIgstAmount() {
    return igstAmount;
  }

  public void setIgstAmount(BigDecimal igstAmount) {
    this.igstAmount = igstAmount;
  }

  public BigDecimal getCessRate() {
    return cessRate;
  }

  public void setCessRate(BigDecimal cessRate) {
    this.cessRate = cessRate;
  }

  public BigDecimal getCessAmount() {
    return cessAmount;
  }

  public void setCessAmount(BigDecimal cessAmount) {
    this.cessAmount = cessAmount;
  }

  public BigDecimal getCgstRate() {
    return cgstRate;
  }

  public void setCgstRate(BigDecimal cgstRate) {
    this.cgstRate = cgstRate;
  }

  public BigDecimal getSgstRate() {
    return sgstRate;
  }

  public void setSgstRate(BigDecimal sgstRate) {
    this.sgstRate = sgstRate;
  }

  public BigDecimal getIgstRate() {
    return igstRate;
  }

  public void setIgstRate(BigDecimal igstRate) {
    this.igstRate = igstRate;
  }

  public BigDecimal getStateCessRate() {
    return stateCessRate;
  }

  public void setStateCessRate(BigDecimal stateCessRate) {
    this.stateCessRate = stateCessRate;
  }

  public BigDecimal getStateCessAmount() {
    return stateCessAmount;
  }

  public void setStateCessAmount(BigDecimal stateCessAmount) {
    this.stateCessAmount = stateCessAmount;
  }

  public BigDecimal getStateCessNonAdvalAmount() {
    return stateCessNonAdvalAmount;
  }

  public void setStateCessNonAdvalAmount(BigDecimal stateCessNonAdvalAmount) {
    this.stateCessNonAdvalAmount = stateCessNonAdvalAmount;
  }

  public BigDecimal getCessNonAdvalAmount() {
    return cessNonAdvalAmount;
  }

  public void setCessNonAdvalAmount(BigDecimal cessNonAdvalAmount) {
    this.cessNonAdvalAmount = cessNonAdvalAmount;
  }

  public BigDecimal getOtherCharges() {
    return otherCharges;
  }

  public void setOtherCharges(BigDecimal otherCharges) {
    this.otherCharges = otherCharges;
  }

  public String getOrderLineReference() {
    return orderLineReference;
  }

  public void setOrderLineReference(String orderLineReference) {
    this.orderLineReference = orderLineReference;
  }

  public CountryCode getOriginCountry() {
    return originCountry;
  }

  public void setOriginCountry(CountryCode originCountry) {
    this.originCountry = originCountry;
  }

  public String getProductSerialNumber() {
    return productSerialNumber;
  }

  public void setProductSerialNumber(String productSerialNumber) {
    this.productSerialNumber = productSerialNumber;
  }

  public List<AttributeDetails> getAttributeDetails() {
    return attributeDetails;
  }

  public void setAttributeDetails(List<AttributeDetails> attributeDetails) {
    this.attributeDetails = attributeDetails;
  }

  public BatchDetails getBatchDetails() {
    return batchDetails;
  }

  public void setBatchDetails(BatchDetails batchDetails) {
    this.batchDetails = batchDetails;
  }

  @JsonProperty("ItemNo")
  BigDecimal itemNumber;

  @JsonProperty("SlNo")
  String serialNumber;

  @JsonProperty("PrdDesc")
  String productDescription;

  @JsonProperty("IsServc")
  BooleanChoice service;

  @JsonProperty("HsnCd")
  String hsnCode;

  @JsonProperty("UnitPrice")
  BigDecimal unitPrice;

  @JsonProperty("TotAmt")
  BigDecimal totalAmount;

  @JsonProperty("AssAmt")
  BigDecimal assessableAmount;

  @JsonProperty("GstRt")
  BigDecimal gstRate;

  @JsonProperty("TotItemVal")
  BigDecimal totalItemAmount;

  @JsonProperty("Barcde")
  String barCode;

  @JsonProperty("Qty")
  BigDecimal quantity;

  @JsonProperty("FreeQty")
  BigDecimal freeQuantity;

  @JsonProperty("Unit")
  UnitType unitType;

  @JsonProperty("Discount")
  BigDecimal discountAmount;

  @JsonProperty("PreTaxVal")
  BigDecimal preTaxValue;

  @JsonProperty("SgstAmt")
  BigDecimal sgstAmount;

  @JsonProperty("CgstAmt")
  BigDecimal cgstAmount;

  @JsonProperty("IgstAmt")
  BigDecimal igstAmount;

  @JsonProperty("CesRt")
  BigDecimal cessRate;

  @JsonProperty("CesAmt")
  BigDecimal cessAmount;

  @JsonProperty("CgstRt")
  BigDecimal cgstRate;

  @JsonProperty("SgstRt")
  BigDecimal sgstRate;

  @JsonProperty("IgstRt")
  BigDecimal igstRate;

  @JsonProperty("StateCesRt")
  BigDecimal stateCessRate;

  @JsonProperty("StateCesAmt")
  BigDecimal stateCessAmount;

  @JsonProperty("StateCesNonAdvlAmt")
  BigDecimal stateCessNonAdvalAmount;

  @JsonProperty("CesNonAdvlAmt")
  BigDecimal cessNonAdvalAmount;

  @JsonProperty("OthChrg")
  BigDecimal otherCharges;

  @JsonProperty("OrdLineRef")
  String orderLineReference;

  @JsonProperty("OrgCntry")
  CountryCode originCountry;

  @JsonProperty("PrdSlNo")
  String productSerialNumber;

  @JsonProperty("AttribDtls")
  List<AttributeDetails> attributeDetails;

  @JsonProperty("BchDtls")
  BatchDetails batchDetails;
}
