package com.vayana.flynnclient.types.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

public class PayeeDetails {

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getBankAccountNumber() {
    return bankAccountNumber;
  }

  public void setBankAccountNumber(String bankAccountNumber) {
    this.bankAccountNumber = bankAccountNumber;
  }

  public String getPaymentMode() {
    return paymentMode;
  }

  public void setPaymentMode(String paymentMode) {
    this.paymentMode = paymentMode;
  }

  public String getIfscCode() {
    return ifscCode;
  }

  public void setIfscCode(String ifscCode) {
    this.ifscCode = ifscCode;
  }

  public String getPaymentTerms() {
    return paymentTerms;
  }

  public void setPaymentTerms(String paymentTerms) {
    this.paymentTerms = paymentTerms;
  }

  public String getPaymentInstructions() {
    return paymentInstructions;
  }

  public void setPaymentInstructions(String paymentInstructions) {
    this.paymentInstructions = paymentInstructions;
  }

  public String getCreditTransfer() {
    return creditTransfer;
  }

  public void setCreditTransfer(String creditTransfer) {
    this.creditTransfer = creditTransfer;
  }

  public String getDebitDirect() {
    return debitDirect;
  }

  public void setDebitDirect(String debitDirect) {
    this.debitDirect = debitDirect;
  }

  public BigDecimal getCreditDays() {
    return creditDays;
  }

  public void setCreditDays(BigDecimal creditDays) {
    this.creditDays = creditDays;
  }

  public BigDecimal getBalanceAmount() {
    return balanceAmount;
  }

  public void setBalanceAmount(BigDecimal balanceAmount) {
    this.balanceAmount = balanceAmount;
  }

  public BigDecimal getPaidAmount() {
    return paidAmount;
  }

  public void setPaidAmount(BigDecimal paidAmount) {
    this.paidAmount = paidAmount;
  }

  @JsonProperty("Nm")
  String name;

  @JsonProperty("AccDet")
  String bankAccountNumber;

  @JsonProperty("Mode")
  String paymentMode;

  @JsonProperty("FinInsBr")
  String ifscCode;

  @JsonProperty("PayTerm")
  String paymentTerms;

  @JsonProperty("PayInstr")
  String paymentInstructions;

  @JsonProperty("CrTrn")
  String creditTransfer;

  @JsonProperty("DirDr")
  String debitDirect;

  @JsonProperty("CrDay")
  BigDecimal creditDays;

  @JsonProperty("PaymtDue")
  BigDecimal balanceAmount;

  @JsonProperty("PaidAmt")
  BigDecimal paidAmount;
}
