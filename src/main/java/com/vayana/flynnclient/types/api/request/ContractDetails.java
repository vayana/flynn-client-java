package com.vayana.flynnclient.types.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;

public class ContractDetails {

  public String getReceiptAdviceNumber() {
    return receiptAdviceNumber;
  }

  public void setReceiptAdviceNumber(String receiptAdviceNumber) {
    this.receiptAdviceNumber = receiptAdviceNumber;
  }

  public Date getReceiptAdviceDate() {
    return receiptAdviceDate;
  }

  public void setReceiptAdviceDate(Date receiptAdviceDate) {
    this.receiptAdviceDate = receiptAdviceDate;
  }

  public String getBatchReferenceNumber() {
    return batchReferenceNumber;
  }

  public void setBatchReferenceNumber(String batchReferenceNumber) {
    this.batchReferenceNumber = batchReferenceNumber;
  }

  public String getContractReferenceNumber() {
    return contractReferenceNumber;
  }

  public void setContractReferenceNumber(String contractReferenceNumber) {
    this.contractReferenceNumber = contractReferenceNumber;
  }

  public String getOtherReferenceNumber() {
    return otherReferenceNumber;
  }

  public void setOtherReferenceNumber(String otherReferenceNumber) {
    this.otherReferenceNumber = otherReferenceNumber;
  }

  public String getProjectReferenceNumber() {
    return projectReferenceNumber;
  }

  public void setProjectReferenceNumber(String projectReferenceNumber) {
    this.projectReferenceNumber = projectReferenceNumber;
  }

  public String getPurchaseReferenceNumber() {
    return purchaseReferenceNumber;
  }

  public void setPurchaseReferenceNumber(String purchaseReferenceNumber) {
    this.purchaseReferenceNumber = purchaseReferenceNumber;
  }

  public Date getVendorReferenceDate() {
    return vendorReferenceDate;
  }

  public void setVendorReferenceDate(Date vendorReferenceDate) {
    this.vendorReferenceDate = vendorReferenceDate;
  }

  @JsonProperty("RecAdvRefr")
  String receiptAdviceNumber;

  @JsonProperty("RecAdvDt")
  Date receiptAdviceDate;

  @JsonProperty("TendRefr")
  String batchReferenceNumber;

  @JsonProperty("ContrRefr")
  String contractReferenceNumber;

  @JsonProperty("ExtRefr")
  String otherReferenceNumber;

  @JsonProperty("ProjRefr")
  String projectReferenceNumber;

  @JsonProperty("PORefr")
  String purchaseReferenceNumber;

  @JsonProperty("PORefDt")
  Date vendorReferenceDate;
}
