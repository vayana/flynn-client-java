package com.vayana.flynnclient.types.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vayana.flynnclient.types.enums.TransportationMode;
import com.vayana.flynnclient.types.enums.VehicleType;
import java.util.Date;

public class EwayBillDetails {

  public String getTransporterGstin() {
    return transporterGstin;
  }

  public void setTransporterGstin(String transporterGstin) {
    this.transporterGstin = transporterGstin;
  }

  public String getTransporterName() {
    return transporterName;
  }

  public void setTransporterName(String transporterName) {
    this.transporterName = transporterName;
  }

  public TransportationMode getTransportationMode() {
    return transportationMode;
  }

  public void setTransportationMode(TransportationMode transportationMode) {
    this.transportationMode = transportationMode;
  }

  public int getDistance() {
    return distance;
  }

  public void setDistance(int distance) {
    this.distance = distance;
  }

  public String getTransportDocNumber() {
    return transportDocNumber;
  }

  public void setTransportDocNumber(String transportDocNumber) {
    this.transportDocNumber = transportDocNumber;
  }

  public Date getTransportDocDate() {
    return transportDocDate;
  }

  public void setTransportDocDate(Date transportDocDate) {
    this.transportDocDate = transportDocDate;
  }

  public String getVehicleNumber() {
    return vehicleNumber;
  }

  public void setVehicleNumber(String vehicleNumber) {
    this.vehicleNumber = vehicleNumber;
  }

  public VehicleType getVehicleType() {
    return vehicleType;
  }

  public void setVehicleType(VehicleType vehicleType) {
    this.vehicleType = vehicleType;
  }

  @JsonProperty("TransId")
  String transporterGstin;

  @JsonProperty("TransName")
  String transporterName;

  @JsonProperty("TransMode")
  TransportationMode transportationMode;

  @JsonProperty("Distance")
  int distance;

  @JsonProperty("TransDocNo")
  String transportDocNumber;

  @JsonProperty("TransDocDt")
  Date transportDocDate;

  @JsonProperty("VehNo")
  String vehicleNumber;

  @JsonProperty("VehType")
  VehicleType vehicleType;

}
