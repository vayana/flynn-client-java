package com.vayana.flynnclient.types.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vayana.flynnclient.types.enums.StateCode;

public class DisplayDetails {

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddressOne() {
    return addressOne;
  }

  public void setAddressOne(String addressOne) {
    this.addressOne = addressOne;
  }

  public String getAddressTwo() {
    return addressTwo;
  }

  public void setAddressTwo(String addressTwo) {
    this.addressTwo = addressTwo;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public int getPincode() {
    return pincode;
  }

  public void setPincode(int pincode) {
    this.pincode = pincode;
  }

  public StateCode getState() {
    return state;
  }

  public void setState(StateCode state) {
    this.state = state;
  }

  @JsonProperty("Nm")
  String name;

  @JsonProperty("Addr1")
  String addressOne;

  @JsonProperty("Addr2")
  String addressTwo;

  @JsonProperty("Loc")
  String location;

  @JsonProperty("Pin")
  int pincode;

  @JsonProperty("Stcd")
  StateCode state;

}
