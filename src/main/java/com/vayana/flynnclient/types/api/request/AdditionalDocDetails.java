package com.vayana.flynnclient.types.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AdditionalDocDetails {

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getDocs() {
    return docs;
  }

  public void setDocs(String docs) {
    this.docs = docs;
  }

  public String getInfoDetails() {
    return infoDetails;
  }

  public void setInfoDetails(String infoDetails) {
    this.infoDetails = infoDetails;
  }

  @JsonProperty("Url")
  String url;

  @JsonProperty("Docs")
  String docs;

  @JsonProperty("Info")
  String infoDetails;
}
