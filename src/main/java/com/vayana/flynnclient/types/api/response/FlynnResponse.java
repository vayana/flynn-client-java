package com.vayana.flynnclient.types.api.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FlynnResponse<T> {
  @JsonProperty("data")
  private T data;

  public T getData() {
    return data;
  }

  public void setData(T data) {
    this.data = data;
  }
}
