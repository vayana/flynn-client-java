package com.vayana.flynnclient.types.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;

public class PrecDocument {

  public String getPrecedingInvoiceNumber() {
    return precedingInvoiceNumber;
  }

  public void setPrecedingInvoiceNumber(String precedingInvoiceNumber) {
    this.precedingInvoiceNumber = precedingInvoiceNumber;
  }

  public Date getPrecedingInvoiceDate() {
    return precedingInvoiceDate;
  }

  public void setPrecedingInvoiceDate(Date precedingInvoiceDate) {
    this.precedingInvoiceDate = precedingInvoiceDate;
  }

  public String getOtherReferenceNumber() {
    return otherReferenceNumber;
  }

  public void setOtherReferenceNumber(String otherReferenceNumber) {
    this.otherReferenceNumber = otherReferenceNumber;
  }

  @JsonProperty("InvNo")
  String precedingInvoiceNumber;

  @JsonProperty("InvDt")
  Date precedingInvoiceDate;

  @JsonProperty("OthRefNo")
  String otherReferenceNumber;

}
