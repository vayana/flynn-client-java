package com.vayana.flynnclient.types.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;


public class BatchDetails {

  public String getBatchName() {
    return batchName;
  }

  public void setBatchName(String batchName) {
    this.batchName = batchName;
  }

  public Date getBatchExpiryDate() {
    return batchExpiryDate;
  }

  public void setBatchExpiryDate(Date batchExpiryDate) {
    this.batchExpiryDate = batchExpiryDate;
  }

  public Date getWarrantyDate() {
    return warrantyDate;
  }

  public void setWarrantyDate(Date warrantyDate) {
    this.warrantyDate = warrantyDate;
  }

  @JsonProperty("Nm")
  String batchName;

  @JsonProperty("ExpDt")
  Date batchExpiryDate;

  @JsonProperty("WrDt")
  Date warrantyDate;
}
