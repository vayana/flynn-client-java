package com.vayana.flynnclient.types.api.response;

public class TaskLastUpdated {
  private String time;

  public String getTime() {
    return time;
  }

  public void setTime(String time) {
    this.time = time;
  }
}
