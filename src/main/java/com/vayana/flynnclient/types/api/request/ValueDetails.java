package com.vayana.flynnclient.types.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

public class ValueDetails {

  public BigDecimal getTotalAssessableValue() {
    return totalAssessableValue;
  }

  public void setTotalAssessableValue(BigDecimal totalAssessableValue) {
    this.totalAssessableValue = totalAssessableValue;
  }

  public BigDecimal getTotalCgstValue() {
    return totalCgstValue;
  }

  public void setTotalCgstValue(BigDecimal totalCgstValue) {
    this.totalCgstValue = totalCgstValue;
  }

  public BigDecimal getTotalSgstValue() {
    return totalSgstValue;
  }

  public void setTotalSgstValue(BigDecimal totalSgstValue) {
    this.totalSgstValue = totalSgstValue;
  }

  public BigDecimal getTotalIgstValue() {
    return totalIgstValue;
  }

  public void setTotalIgstValue(BigDecimal totalIgstValue) {
    this.totalIgstValue = totalIgstValue;
  }

  public BigDecimal getTotalCessValue() {
    return totalCessValue;
  }

  public void setTotalCessValue(BigDecimal totalCessValue) {
    this.totalCessValue = totalCessValue;
  }

  public BigDecimal getTotalStateCessValue() {
    return totalStateCessValue;
  }

  public void setTotalStateCessValue(BigDecimal totalStateCessValue) {
    this.totalStateCessValue = totalStateCessValue;
  }

  public BigDecimal getDiscount() {
    return discount;
  }

  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }

  public BigDecimal getOtherCharges() {
    return otherCharges;
  }

  public void setOtherCharges(BigDecimal otherCharges) {
    this.otherCharges = otherCharges;
  }

  public BigDecimal getRoundedOffAmount() {
    return roundedOffAmount;
  }

  public void setRoundedOffAmount(BigDecimal roundedOffAmount) {
    this.roundedOffAmount = roundedOffAmount;
  }

  public BigDecimal getTotalInvoiceValue() {
    return totalInvoiceValue;
  }

  public void setTotalInvoiceValue(BigDecimal totalInvoiceValue) {
    this.totalInvoiceValue = totalInvoiceValue;
  }

  public BigDecimal getTotalInvValueFinal() {
    return totalInvValueFinal;
  }

  public void setTotalInvValueFinal(BigDecimal totalInvValueFinal) {
    this.totalInvValueFinal = totalInvValueFinal;
  }

  @JsonProperty("AssVal")
  BigDecimal totalAssessableValue;

  @JsonProperty("CgstVal")
  BigDecimal totalCgstValue;

  @JsonProperty("SgstVal")
  BigDecimal totalSgstValue;

  @JsonProperty("IgstVal")
  BigDecimal totalIgstValue;

  @JsonProperty("CesVal")
  BigDecimal totalCessValue;

  @JsonProperty("StCesVal")
  BigDecimal totalStateCessValue;

  @JsonProperty("Discount")
  BigDecimal discount;

  @JsonProperty("OthChrg")
  BigDecimal otherCharges;

  @JsonProperty("RndOffAmt")
  BigDecimal roundedOffAmount;

  @JsonProperty("TotInvVal")
  BigDecimal totalInvoiceValue;

  @JsonProperty("TotInvValFc")
  BigDecimal totalInvValueFinal;

}
