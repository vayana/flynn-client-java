package com.vayana.flynnclient.types.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class RefDetails {

  public String getInvoiceRemarks() {
    return invoiceRemarks;
  }

  public void setInvoiceRemarks(String invoiceRemarks) {
    this.invoiceRemarks = invoiceRemarks;
  }

  public DocPerdDtls getDocPerdDtsl() {
    return docPerdDtsl;
  }

  public void setDocPerdDtsl(DocPerdDtls docPerdDtsl) {
    this.docPerdDtsl = docPerdDtsl;
  }

  public List<PrecDocument> getPrefDocuments() {
    return prefDocuments;
  }

  public void setPrefDocuments(List<PrecDocument> prefDocuments) {
    this.prefDocuments = prefDocuments;
  }

  public List<ContractDetails> getContractDetails() {
    return contractDetails;
  }

  public void setContractDetails(List<ContractDetails> contractDetails) {
    this.contractDetails = contractDetails;
  }

  @JsonProperty("InvRm")
  String invoiceRemarks;

  @JsonProperty("DocPerdDtls")
  DocPerdDtls docPerdDtsl;

  @JsonProperty("PrecDocDtls")
  List<PrecDocument> prefDocuments;

  @JsonProperty("ContrDtls")
  List<ContractDetails> contractDetails;
}
