package com.vayana.flynnclient.types.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;

public class DocPerdDtls {

  public Date getInvoiceStartDate() {
    return invoiceStartDate;
  }

  public void setInvoiceStartDate(Date invoiceStartDate) {
    this.invoiceStartDate = invoiceStartDate;
  }

  public Date getInvoiceEndDate() {
    return invoiceEndDate;
  }

  public void setInvoiceEndDate(Date invoiceEndDate) {
    this.invoiceEndDate = invoiceEndDate;
  }

  @JsonProperty("InvStDt")
  Date invoiceStartDate;

  @JsonProperty("InvEndDt")
  Date invoiceEndDate;
}
