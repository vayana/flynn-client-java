package com.vayana.flynnclient.types.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vayana.flynnclient.types.enums.BooleanChoice;
import com.vayana.flynnclient.types.enums.CountryCode;
import com.vayana.flynnclient.types.enums.PortCode;
import java.math.BigDecimal;
import java.util.Date;

public class ExportDetails {

  public String getShipmentBillNo() {
    return shipmentBillNo;
  }

  public void setShipmentBillNo(String shipmentBillNo) {
    this.shipmentBillNo = shipmentBillNo;
  }

  public Date getShipmentBillDate() {
    return shipmentBillDate;
  }

  public void setShipmentBillDate(Date shipmentBillDate) {
    this.shipmentBillDate = shipmentBillDate;
  }

  public PortCode getPortCode() {
    return portCode;
  }

  public void setPortCode(PortCode portCode) {
    this.portCode = portCode;
  }

  public BooleanChoice getRefund() {
    return refund;
  }

  public void setRefund(BooleanChoice refund) {
    this.refund = refund;
  }

  public String getForeignCurrency() {
    return foreignCurrency;
  }

  public void setForeignCurrency(String foreignCurrency) {
    this.foreignCurrency = foreignCurrency;
  }

  public CountryCode getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(CountryCode countryCode) {
    this.countryCode = countryCode;
  }

  public BigDecimal getExportDuty() {
    return exportDuty;
  }

  public void setExportDuty(BigDecimal exportDuty) {
    this.exportDuty = exportDuty;
  }

  @JsonProperty("ShipBNo")
  String shipmentBillNo;

  @JsonProperty("ShipBDt")
  Date shipmentBillDate;

  @JsonProperty("Port")
  PortCode portCode;

  @JsonProperty("RefClm")
  BooleanChoice refund;

  @JsonProperty("ForCur")
  String foreignCurrency;

  @JsonProperty("CntCode")
  CountryCode countryCode;

  @JsonProperty("ExpDuty")
  BigDecimal exportDuty;
}
