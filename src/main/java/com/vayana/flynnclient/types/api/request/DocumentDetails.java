package com.vayana.flynnclient.types.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DocumentDetails {

  public String getDocType() {
    return docType;
  }

  public void setDocType(String docType) {
    this.docType = docType;
  }

  public String getInvoiceNumber() {
    return InvoiceNumber;
  }

  public void setInvoiceNumber(String invoiceNumber) {
    InvoiceNumber = invoiceNumber;
  }

  public String getDate() {
    return Date;
  }

  public void setDate(String date) {
    Date = date;
  }

  @JsonProperty("Typ")
  String docType ;

  @JsonProperty("No")
  String InvoiceNumber;

  @JsonProperty("Dt")
  String Date;
}


