package com.vayana.flynnclient.types.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vayana.flynnclient.types.enums.StateCode;

public class ShipmentDetails {

  public String getGstin() {
    return gstin;
  }

  public void setGstin(String gstin) {
    this.gstin = gstin;
  }

  public String getLegalName() {
    return legalName;
  }

  public void setLegalName(String legalName) {
    this.legalName = legalName;
  }

  public String getTradeName() {
    return tradeName;
  }

  public void setTradeName(String tradeName) {
    this.tradeName = tradeName;
  }

  public String getAddressOne() {
    return addressOne;
  }

  public void setAddressOne(String addressOne) {
    this.addressOne = addressOne;
  }

  public String getAddressTwo() {
    return addressTwo;
  }

  public void setAddressTwo(String addressTwo) {
    this.addressTwo = addressTwo;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public int getPincode() {
    return pincode;
  }

  public void setPincode(int pincode) {
    this.pincode = pincode;
  }

  public StateCode getState() {
    return state;
  }

  public void setState(StateCode state) {
    this.state = state;
  }

  @JsonProperty("Gstin")
  String gstin;

  @JsonProperty("LglNm")
  String legalName;

  @JsonProperty("TrdNm")
  String tradeName;

  @JsonProperty("Addr1")
  String addressOne;

  @JsonProperty("Addr2")
  String addressTwo;

  @JsonProperty("Loc")
  String location;

  @JsonProperty("Pin")
  int pincode;

  @JsonProperty("Stcd")
  StateCode state;

}
