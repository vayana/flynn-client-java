package com.vayana.flynnclient.exception;

public class EncryptionNotPossibleException extends RuntimeException {

  public EncryptionNotPossibleException(String message, Throwable cause) {
    super(message, cause);
  }

  @Override
  public String toString() {
    return String.format("EncryptionNotPossibleException{ cause = %s }", super.getMessage());
  }
}
