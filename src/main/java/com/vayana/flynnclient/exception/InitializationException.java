package com.vayana.flynnclient.exception;

public class InitializationException extends RuntimeException {
  private final String path;

  public InitializationException(String message, String path) {
    super(message);
    this.path = path;
  }

  public InitializationException(String message, String path, Throwable cause) {
    super(message, cause);
    this.path = path;
  }

  public String getPath() {
    return path;
  }

  @Override
  public String toString() {
    return String.format("InitializationException { cause = %s, file path = %s}", super.getMessage(), path);
  }
}
