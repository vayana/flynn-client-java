package com.vayana.flynnclient.exception;

import java.util.Map;

/**
 * {@code HttpBadRequestException} is an unchecked Exception,
 * which can be thrown where an Http Request has resulted in a Status code other than 200.
 */
public class HttpBadRequestException extends RuntimeException {

  private final RequestDetails details;

  /**
   * Constructs a new {@link HttpBadRequestException} with the specified details. The object will have
   * the information related to the Http Request that caused the exception
   *
   * @param message the detail message (which is saved for later retrieval by the {@link #getMessage()} method)
   * @param details Http Request details object
   * @see org.apache.http.HttpRequest
   */
  public HttpBadRequestException(String message, RequestDetails details) {
    super(message);
    this.details = details;
  }

  public RequestDetails getDetails() {
    return details;
  }

  @Override
  public String toString() {
    return String.format("HttpBadRequestException { cause = %s details = %s }", super.getMessage(), details);
  }

  public static class RequestDetails {
    private final String targetUrl;
    private final Map<String, String> headers;
    private final String requestBody;
    private final int statusCode;
    private final String responseBody;

    /**
     * @param targetUrl    the target url that was being hit
     * @param headers      the headers of the Http Request
     * @param requestBody  the requestBody of the Http Request
     * @param statusCode   the Status code of the Http Response
     * @param responseBody String representation of the response of the Http call
     */
    public RequestDetails(String targetUrl, Map<String, String> headers, String requestBody, int statusCode, String responseBody) {
      this.targetUrl = targetUrl;
      this.headers = headers;
      this.requestBody = requestBody;
      this.statusCode = statusCode;
      this.responseBody = responseBody;
    }

    public RequestDetails(String targetUrl, Map<String, String> headers, int statusCode, String responseBody) {
      this(targetUrl, headers, null, statusCode, responseBody);
    }

    public String getTargetUrl() {
      return targetUrl;
    }

    public Map<String, String> getHeaders() {
      return headers;
    }

    public String getRequestBody() {
      return requestBody;
    }

    public int getStatusCode() {
      return statusCode;
    }

    public String getResponseBody() {
      return responseBody;
    }

    @Override
    public String toString() {
      return String.format("HttpBadRequestDetails { Target Url = %s, statusCode = %d, headers = %s, body= %s, " +
        "responseBody = %s }", targetUrl, statusCode, headers, requestBody, responseBody);
    }
  }
}
