package com.vayana.flynnclient.exception;

public class SerializationException extends RuntimeException {

  public SerializationException(String message) {
    super(message);
  }

  public SerializationException(String message, Throwable cause) {
    super(message, cause);
  }

  @Override
  public String toString() {
    return String.format("SerializationException { cause = %s }", super.getMessage());
  }
}
