package com.vayana.flynnclient.exception;

public class NotImplementedException extends RuntimeException {
  public NotImplementedException(String message) {
    super(message);
  }
}
