package com.vayana.flynnclient.exception;

public class InvalidKeyFileException extends RuntimeException {

  private final String filePath;

  public InvalidKeyFileException(String message, String filePath) {
    super(message);
    this.filePath = filePath;
  }

  public InvalidKeyFileException(String message, String filePath, Throwable cause) {
    super(message, cause);
    this.filePath = filePath;
  }

  public String getFilePath() {
    return filePath;
  }

  @Override
  public String toString() {
    return String.format("InvalidKeyFileException{ cause = %s, filePath = %s}", super.getMessage(), this.filePath);
  }
}
