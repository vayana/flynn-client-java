package com.vayana.flynnclient.exception;

public class ConfigurationException extends RuntimeException {

  public ConfigurationException(String message) {
    super(message);
  }

  @Override
  public String toString() {
    return String.format("ConfigurationException { cause = %s }", super.getMessage());
  }
}
