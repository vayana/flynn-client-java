package com.vayana.flynnclient.client;

import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.vayana.flynnclient.context.ClientContext;
import com.vayana.flynnclient.context.Services;
import com.vayana.flynnclient.exception.ConfigurationException;
import com.vayana.flynnclient.exception.DeserializationException;
import com.vayana.flynnclient.exception.HttpBadRequestException;
import com.vayana.flynnclient.types.auth.AuthTokenProvider;
import com.vayana.flynnclient.types.auth.request.AuthReq;
import com.vayana.flynnclient.types.auth.response.AuthResponse;
import com.vayana.flynnclient.types.config.*;
import com.vayana.flynnclient.util.DataMapper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.*;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.Security;
import java.security.SignatureException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static com.vayana.flynnclient.Constants.*;
import static com.vayana.flynnclient.types.headers.FlynnApiHeaders.*;
import static com.vayana.flynnclient.util.CommonUtil.generateRek;
import static com.vayana.flynnclient.util.EncryptionUtil.encrypt;
import static com.vayana.flynnclient.util.EncryptionUtil.getEncryptedString;
import static com.vayana.flynnclient.util.FileUtil.*;
import static com.vayana.flynnclient.util.JwtUtil.verifyAndReturnJwtToken;

public class FlynnClient {
  private static final Logger LOG = LoggerFactory.getLogger(FlynnClient.class);

  private final ClientContext cc;
  private final CloseableHttpClient httpClient;
  private final Services services;
  private final AuthTokenProvider tokenHandler;

  private static final String ACCEPT_HEADER_NAME = "accept";

  static {
    Security.setProperty("crypto.policy", "unlimited");
    Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
  }

  /**
   * Sets the {@link ClientContext}, initializes the {@link org.apache.http.client.HttpClient}
   *
   * @param config the build config used to create this Client
   */
  public FlynnClient(FlynnConfig config, AuthTokenProvider tokenHandler) {
    httpClient = HttpClients.createDefault();
    cc = new ClientContext(config);
    this.services = new Services(DataMapper.mapperSlash);
    this.tokenHandler = tokenHandler;
  }

  public void close() {
    try {
      httpClient.close();
    } catch (IOException cause) {
      LOG.error("unable to close http client. {}", cause.getMessage());
    }
  }

  public ClientContext getClientContext() {
    return cc;
  }

  public Services getServices() {
    return services;
  }

  /**
   * Util method corresponding to the Http GET method.
   *
   * @param targetUrl the target url for the API call
   * @param headers   the headers used for the API call
   * @return {@link HttpEntity} corresponding to the response received
   * @see HttpGet
   */
  public HttpEntity get(String targetUrl, Map<String, String> headers) throws IOException {
    return sendRequest(new HttpGet(getClientContext().getApiServerConfig().getUrl() + targetUrl), headers);
  }

  /**
   * Util method corresponding to the Http GET method.
   *
   * @param targetUrl the target url for the API call
   * @return {@link HttpEntity} corresponding to the response received
   * @see HttpGet
   */
  public HttpEntity get(String targetUrl) throws IOException {
    return sendRequest(new HttpGet(getClientContext().getApiServerConfig().getUrl() + targetUrl), Collections.emptyMap());
  }

  /**
   * Util method corresponding to the Http PUT method.
   *
   * @param targetUrl the target url for the API call
   * @param headers   the headers used for the API call
   * @param body      the request body as String
   * @return {@link HttpEntity} corresponding to the response received
   * @see HttpPut
   */
  public HttpEntity put(String targetUrl, Map<String, String> headers, String body) throws IOException {
    return sendRequest(new HttpPut(getClientContext().getApiServerConfig().getUrl() + targetUrl), headers, body);
  }

  /**
   * Util method corresponding to the Http PATCH method.
   *
   * @param targetUrl the target url for the API call
   * @param headers   the headers used for the API call
   * @param body      the request body as String
   * @return {@link HttpEntity} corresponding to the response received
   * @see HttpPatch
   */
  public HttpEntity patch(String targetUrl, Map<String, String> headers, String body) throws IOException {
    return sendRequest(new HttpPatch(getClientContext().getApiServerConfig().getUrl() + targetUrl), headers, body);
  }

  /**
   * Util method corresponding to the Http POST method.
   *
   * @param targetUrl the target url for the API call
   * @param headers   the headers used for the API call
   * @param body      the request body as String
   * @return {@link HttpEntity} corresponding to the response received
   * @see HttpPost
   */
  public HttpEntity post(String targetUrl, Map<String, String> headers, String body) throws IOException {
    return sendRequest(new HttpPost(getClientContext().getApiServerConfig().getUrl() + targetUrl), headers, body);
  }

  /**
   * Util method corresponding to the Http POST method.
   *
   * @param targetUrl the target url for the API call
   * @param headers   the headers used for the API call
   * @return {@link HttpEntity} corresponding to the response received
   * @see HttpPost
   */
  public HttpEntity post(String targetUrl, Map<String, String> headers) throws IOException {
    return sendRequest(new HttpPost(getClientContext().getApiServerConfig().getUrl() + targetUrl), headers);
  }

  /**
   * Ping Flynn Server using this client
   *
   * @return the response for Ping API call
   */
  public String pingServer() throws IOException {
    String url = "/ping";

    return EntityUtils.toString(get(url));
  }

  /**
   * Ping Theodore Auth Server using this client
   *
   * @return the response for the Auth server API call
   */
  public String pingAuthServer() throws IOException {
    AuthServerConfig authConfig = cc.getAuthServerConfig();
    String url = String.format("%s/apis/health", authConfig.getUrl());
    return EntityUtils.toString(sendRequest(new HttpGet(url), Collections.emptyMap()));
  }

  /**
   * Authenticated ping for Flynn Server. Given a set of headers, checks if user is able to call authenticated APIs in Flynn.
   *
   * @return the response for the authenticate API
   */
  public String authenticate() throws IOException {
    String url = "/authenticate";

    return EntityUtils.toString(get(url));
  }

  /**
   * Fetch the {@link AuthResponse} for this client. The auth response will contain all data present in Theodore Auth token.
   *
   * @return {@link AuthResponse} received from Theodore Auth Server
   */
  public AuthResponse getParsedAuthResponse() {
    AuthServerConfig authConfig = cc.getAuthServerConfig();

    OrganisationUser user = cc.getClientDetails().getOrgUser();
    AuthReq loginReq = new AuthReq(user.getEmail(), "email", user.getPassword());

    String url = String.format("%s/apis/%s/authtokens", authConfig.getUrl(), authConfig.getVersion());

    try {
      HttpPost request = new HttpPost(url);
      String body = serialize(this, loginReq);

      request.setEntity(EntityBuilder.create().setText(body).build());

      CloseableHttpResponse response = httpClient.execute(request);

      if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
        return services.getMapper().readValue(EntityUtils.toString(response.getEntity()), AuthResponse.class);
      } else {
        String targetUrl = request.getURI().toString();

        HttpBadRequestException.RequestDetails details = new HttpBadRequestException.RequestDetails(
          targetUrl, Collections.emptyMap(), body, response.getStatusLine().getStatusCode(), EntityUtils.toString(response.getEntity())
        );
        logApiErrorResponseDetails(details);
        throw new HttpBadRequestException("Http call failed for " + targetUrl, details);
      }
    } catch (JsonProcessingException cause) {
      LOG.error(String.format("err de-serializing login response. %s", cause));
      throw new DeserializationException(String.format("login response deserialize failed. %s", cause.getMessage()), cause);
    } catch (IOException cause) {
      LOG.error(String.format("err serializing login request. %s", cause));
      throw new DeserializationException(String.format("login request serialize failed. %s", cause.getMessage()), cause);
    }
  }

  /**
   * Get Auth token either from the given {@link AuthTokenProvider} or doing an API call to Auth server if the token is not present.
   * As a side effect, the token received in the response is also saved using the {@link AuthTokenProvider}.
   *
   * @return String representation of the API response of the authentication call
   * @throws IOException if not able to save the token using {@link AuthTokenProvider}
   */
  public final String getAuthToken() throws IOException {
    String signedToken;

    if (tokenHandler.isTokenPresent()) {
      signedToken = tokenHandler.getSignedToken();

      try {
        verifyAndReturnJwtToken(this, signedToken);
      } catch (TokenExpiredException | SignatureVerificationException cause) {
        signedToken = getParsedAuthResponse().getData().getToken();

        tokenHandler.saveToken(signedToken);
      }
    } else {
      signedToken = getParsedAuthResponse().getData().getToken();
      verifyAndReturnJwtToken(this, signedToken);

      tokenHandler.saveToken(signedToken);
    }

    return signedToken;
  }

  /**
   * Utility method that makes a Http Request having only headers and no body.
   * The {@link HttpRequestBase} object defines which Http Method will be used to make the call
   *
   * @param request the {@link HttpRequestBase} object having all request details
   * @param headers a {@link Map<String, String>} of headers
   * @return String response body of the Http Request
   * @throws IOException when there is an error in executing the request using Http client or the response {@link org.apache.http.HttpEntity}
   *                     could not be converted to String
   */
  public final HttpEntity sendRequest(HttpRequestBase request, Map<String, String> headers) throws IOException {
    Map<String, String> authenticatedHeaders = addMandatoryHeaders(request.getURI().getPath(), headers);

    logApiRequestDetails(request, authenticatedHeaders);

    // Prepare Request
    authenticatedHeaders.forEach(request::setHeader);

    // Execute Request
    CloseableHttpResponse response = httpClient.execute(request);

    // Process Response
    if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
      return response.getEntity();
    } else {
      String targetUrl = request.getURI().toString();

      HttpBadRequestException.RequestDetails details = new HttpBadRequestException.RequestDetails(
        targetUrl, authenticatedHeaders, response.getStatusLine().getStatusCode(), EntityUtils.toString(response.getEntity())
      );
      logApiErrorResponseDetails(details);
      throw new HttpBadRequestException("Http call failed for " + targetUrl, details);
    }
  }

  /**
   * Utility method that makes a Http Request having headers and body.
   * The {@link HttpEntityEnclosingRequestBase} object defines which Http Method will be used to make the call
   *
   * @param request the {@link HttpEntityEnclosingRequestBase} object having all request details
   * @param headers a {@link Map<String, String>} of headers
   * @param body    body of the Http request call as {@link String}
   * @return String representation of Http Response
   * @throws IOException when there is an error in executing the request using Http client or the response {@link org.apache.http.HttpEntity}
   *                     could not be converted to String
   */
  public final HttpEntity sendRequest(HttpEntityEnclosingRequestBase request, Map<String, String> headers, String body) throws IOException {
    Map<String, String> authenticatedHeaders = addMandatoryHeaders(request.getURI().getPath(), headers);

    logApiRequestDetails(request, authenticatedHeaders, body);

    // Prepare Request
    authenticatedHeaders.forEach(request::setHeader);

    request.setEntity(EntityBuilder.create().setText(body).build());

    // Execute Request
    CloseableHttpResponse response = httpClient.execute(request);

    // Process Response
    if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
      return response.getEntity();
    } else {
      String targetUrl = request.getURI().toString();

      HttpBadRequestException.RequestDetails details = new HttpBadRequestException.RequestDetails(
        targetUrl, headers, body, response.getStatusLine().getStatusCode(), EntityUtils.toString(response.getEntity())
      );
      logApiErrorResponseDetails(details);
      throw new HttpBadRequestException("Http call failed for " + targetUrl, details);
    }
  }

  private Map<String, String> addMandatoryHeaders(String targetUrl, Map<String, String> existingHeaders) throws IOException {
    String rek = generateRek();

    Map<String, String> headers = getAuthenticatedHeaders(existingHeaders);

    addAcceptHeader(headers, targetUrl);

    if (targetUrl.startsWith("/basic/ewb") || targetUrl.startsWith("/enriched/ewb")) {
      final EwbConfig ewbConfig = cc.getEwbConfig().orElseThrow(() -> new ConfigurationException("err-ewb-config-missing"));

      headers.put(MASTER_SECRET_HEADER_NAME, existingHeaders.getOrDefault(
        MASTER_SECRET_HEADER_NAME, encrypt(cc.getApiServerConfig().getPublicKey(), rek))
      );
      headers.put(EWB_GSTIN_HEADER_NAME, ewbConfig.getGstin());

      headers.put(EWB_USERNAME_HEADER_NAME, ewbConfig.getUsername());
      headers.put(EWB_GSP_CODE_HEADER_NAME, ewbConfig.getGspCode().getCode());

      headers.put(EWB_PASSWORD_HEADER_NAME, existingHeaders.getOrDefault(
        EWB_PASSWORD_HEADER_NAME, getEncryptedString(ewbConfig.getPassword(), rek, AES_CRYPTO_TRANSFORM))
      );
    } else if (targetUrl.startsWith("/basic/einv") || targetUrl.startsWith("/enriched/einv")) {
      final EinvoiceConfig einvConfig = cc.getEinvoiceConfig().orElseThrow(() -> new ConfigurationException("err-einv-config-missing"));

      headers.put(MASTER_SECRET_HEADER_NAME, existingHeaders.getOrDefault(
        MASTER_SECRET_HEADER_NAME, encrypt(cc.getApiServerConfig().getPublicKey(), rek))
      );
      headers.put(EINVOICE_GSTIN_HEADER_NAME, einvConfig.getGstin());
      headers.put(EINVOICE_USERNAME_HEADER_NAME, einvConfig.getUsername());
      headers.put(EINVOICE_PASSWORD_HEADER_NAME, existingHeaders.getOrDefault(
        EINVOICE_PASSWORD_HEADER_NAME, getEncryptedString(einvConfig.getPassword(), rek, AES_CRYPTO_TRANSFORM))
      );
      headers.put(EINVOICE_GSP_CODE_HEADER_NAME, einvConfig.getGspCode().getCode());
    }

    return headers;
  }

  private void addAcceptHeader(Map<String, String> headers, String targetUrl) {
    if (targetUrl.contains("/download/")) {
      headers.put(ACCEPT_HEADER_NAME, "application/zip");
    } else if (targetUrl.contains("/download-pdf/")) {
      headers.put(ACCEPT_HEADER_NAME, "application/pdf");
    } else if (!headers.containsKey(ACCEPT_HEADER_NAME)) {
      headers.put(ACCEPT_HEADER_NAME, ContentType.APPLICATION_JSON.toString());
    }
  }

  private void logApiRequestDetails(HttpRequestBase request, Map<String, String> headers) {
    logApiRequestDetails(request, headers, null);
  }

  private void logApiRequestDetails(HttpRequestBase request, Map<String, String> headers, String body) {
    if (LOG.isDebugEnabled()) {
      LOG.debug("Flynn Client API call details =>");
      LOG.debug("URL :: {}", request.getURI());
      LOG.debug("Headers :: {}", headers.entrySet().stream()
        .map(header -> String.format("%s = %s", header.getKey(), header.getValue()))
        .collect(Collectors.joining(LINE_SEPARATOR)));
      if (body != null) {
        LOG.debug("Body :: {}", body);
      }
    }
  }

  private void logApiErrorResponseDetails(HttpBadRequestException.RequestDetails details) {
    LOG.error("Flynn Client API response call details =>");
    LOG.error("API Request returned status code {}.", details.getStatusCode());
    LOG.error("URL :: {}", details.getTargetUrl());
    LOG.error("Headers :: {}", details.getHeaders().entrySet().stream()
      .map(header -> String.format("%s = %s", header.getKey(), header.getValue()))
      .collect(Collectors.joining(LINE_SEPARATOR)));
    LOG.error("Body :: {}", details.getRequestBody());
    LOG.error("Response :: {}", details.getResponseBody());
  }

  /**
   * Given a set of headers for an API call, returns a new Map with the Authentication headers {@link com.vayana.flynnclient.types.headers.FlynnApiHeaders#ORG_IDENTIFIER_HEADER_NAME} and
   * {@link com.vayana.flynnclient.types.headers.FlynnApiHeaders#USER_IDENTIFIER_HEADER_NAME} along with the given headers
   *
   * @param headers The existing headers
   * @return a new Map with added authentication headers
   */
  private Map<String, String> getAuthenticatedHeaders(Map<String, String> headers) throws IOException {
    Map<String, String> authenticateHeaders = new HashMap<>(headers);
    authenticateHeaders.put(ORG_IDENTIFIER_HEADER_NAME, cc.getClientDetails().getOrgId());
    authenticateHeaders.put(USER_IDENTIFIER_HEADER_NAME, getAuthToken());

    return authenticateHeaders;
  }

  /**
   * Returns a Map with the Authentication headers {@link com.vayana.flynnclient.types.headers.FlynnApiHeaders#ORG_IDENTIFIER_HEADER_NAME} and
   * {@link com.vayana.flynnclient.types.headers.FlynnApiHeaders#USER_IDENTIFIER_HEADER_NAME}
   *
   * @return the map having both the user authentication headers
   */
  private Map<String, String> getAuthenticatedHeaders() throws IOException {
    return getAuthenticatedHeaders(Collections.emptyMap());
  }
}
