package com.vayana.flynnclient;

public final class Constants {

    public static final String TRANSIENT_DIR = "transient";
  public static final String AES_CRYPTO_TRANSFORM = "AES/ECB/PKCS7PADDING";
  public static final String LINE_SEPARATOR = System.getProperty("line.separator");
  public static final String CONFIG_DIRECTORY = "config/";

  public static final String EI_CORE = "eicore";
  public static final String EI_VITAL = "eivital";
  public static final String EI_EWB = "eiewb";
}
