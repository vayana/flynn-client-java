import software.amazon.awssdk.auth.credentials.AwsSessionCredentials
import software.amazon.awssdk.auth.credentials.ProfileProviderCredentialsContext
import software.amazon.awssdk.profiles.ProfileFile
import software.amazon.awssdk.services.sso.auth.SsoProfileCredentialsProviderFactory

plugins {
  java
  `maven-publish`
  `java-library`
}

val projectGroupID = "com.vayana"
val projectArtifactID = "flynn-client-java"
val projectVersion = "0.2.00"

group = projectGroupID
version = projectVersion

val vayanaCommonMavenS3BucketUrl: String by project
val vayanaSCSMavenS3BucketUrl: String by project


buildscript {
  repositories {
    maven {
      url = uri("https://plugins.gradle.org/m2/")
    }
    mavenCentral()
  }
  dependencies {
    classpath("com.github.jengelman.gradle.plugins:shadow:5.0.0")
    classpath(platform("software.amazon.awssdk:bom:2.20.101"))
    classpath("software.amazon.awssdk:auth")
    classpath("software.amazon.awssdk:sso")
    classpath("software.amazon.awssdk:ssooidc")
  }
}

repositories {
  mavenCentral()
  mavenscsVayanaReleases()
  mavencommonVayanaReleases()
  mavenscsVayanaSnapshots()
  mavencommonVayanaSnapshots()
}


dependencies {
  "2.13.1".let { jacksonVersion ->
    api("com.fasterxml.jackson.core", "jackson-databind", jacksonVersion)
    api("com.fasterxml.jackson.core", "jackson-core", jacksonVersion)
  }
  api("com.fasterxml.jackson.datatype", "jackson-datatype-jdk8", "2.13.1")

  api("com.auth0", "java-jwt", "3.18.2")

  ("1.7.32").let { slf4jVersion ->
    implementation("org.slf4j", "slf4j-api", slf4jVersion)
    implementation("org.slf4j", "slf4j-simple", slf4jVersion)
  }
  api("org.bouncycastle", "bcpkix-jdk15on", "1.70")
  api("org.apache.httpcomponents", "httpclient", "4.5.13")
  api("commons-io", "commons-io", "2.11.0")
  testImplementation("junit", "junit", "4.13.2")
}

/*
publishing {
  publications {
    create<MavenPublication>("maven") {
      groupId = projectGroupID
      artifactId = projectArtifactID
      version = projectVersion

      from(components["java"])
    }
  }

  repositories {
    mavenVayanaCommon()
  }
}
*/

java {
  sourceCompatibility = JavaVersion.VERSION_21
  targetCompatibility = JavaVersion.VERSION_21
}

fun awsProfile() = "maven_aws_profile"

fun getProfiles() = ProfileFile.defaultProfileFile()
fun awsCredentials() = SsoProfileCredentialsProviderFactory().create(
  ProfileProviderCredentialsContext.builder()
    .profile(getProfiles().profile(awsProfile()).get())
    .profileFile(getProfiles()).build()
).resolveCredentials()

fun AuthenticationSupported.mavenVayanaCredentials() =
  credentials(AwsCredentials::class.java) {
    val awsCreds = try {
      awsCredentials()
    } catch (e: Exception) {
      throw Exception("${e.message} for profile ${awsProfile()}")
    }


    awsCreds as AwsSessionCredentials


    accessKey = awsCreds.accessKeyId()
    secretKey = awsCreds.secretAccessKey()
    sessionToken = awsCreds.sessionToken()
  }

fun RepositoryHandler.mavenVayanaCommon() {
  maven {
    val repo = uri(vayanaCommonMavenS3BucketUrl)
    url = if (project.version.toString().toLowerCase().endsWith("-snapshot"))
      repo.resolve("snapshots")
    else
      repo.resolve("releases")
    mavenVayanaCredentials()
  }
}

fun RepositoryHandler.mavenVayanaSCS() {
  maven {
    val repo = uri(vayanaSCSMavenS3BucketUrl)
    url = if (project.version.toString().toLowerCase().endsWith("-snapshot"))
      repo.resolve("snapshots")
    else
      repo.resolve("releases")
    mavenVayanaCredentials()
  }
}

fun RepositoryHandler.mavencommonVayanaReleases() {
  maven {
    url = uri("$vayanaCommonMavenS3BucketUrl/releases/")
    mavenVayanaCredentials()
  }
}

fun RepositoryHandler.mavencommonVayanaSnapshots() {
  maven {
    url = uri("$vayanaCommonMavenS3BucketUrl/snapshots/")
    mavenVayanaCredentials()
  }
}

fun RepositoryHandler.mavenscsVayanaReleases() {
  maven {
    url = uri("$vayanaSCSMavenS3BucketUrl/releases/")
    mavenVayanaCredentials()
  }
}

fun RepositoryHandler.mavenscsVayanaSnapshots() {
  maven {
    url = uri("$vayanaSCSMavenS3BucketUrl/snapshots/")
    mavenVayanaCredentials()
  }
}
