# Enriched API (Flynn) Client

This project helps to consume Enriched API Services provided by Vayana Network Services Pvt. Ltd. (VNSPL).

Client library intends to add only all various ping calls,

## Installation

* Clone a project by using `git clone https://bitbucket.org/vayana/flynn-client-java/src/master/`
* Copy the content of `sample.json` in config folder and add the required configurations and name the configurations
  as `config.json`
* Value of `is_sandbox` will be set to true if you want to hit sandbox by using test cases, else it will be false for production
* `auth_server_public_key_file_path` will contain the path of the theodore public key
* `api_server_public_key_file_path` will contain the path of Enriched-API Server's (Flynn) issued Public Key
* `irp_public_key_file_path` will contain the path of irp public key
* `ewb_public_key_file_path` will contain the path of ewb public key
* Under `client_details`, mention your theodore organisation id in front of `org_id` and your theodore login credentials
    under `org_user`
* `ewb_details` will contain the api access credentials of ewb portal and `einvoice_details` will contain the api access
   credentials of e-invoice portal
* After creating `config.json`, run the test cases

### API Server(Flynn) Ping Calls

- `/ping`: a health check call

- `/ping/authenticate`: which uses default header; based on the authentication server (`theodore`)

    - you can check docs for the auth server [here](https://docs-auth.vayananet.com/)

- `/ping/secrets`: which uses extra headers (`X-FLYNN-S-REK` and `X-FLYNN-S-DATA`);
this is to test - sharing of sensitive data to the server

    - `X-FLYNN-S-REK` is the 'Request Encryption Key' (either 32, 64 or 128 char string).
    This key is a dynamic salt for AES encryption of the sensitive data sent either in the request body or in the header.
    The key itself is encrypted with RSA key shared by the server.
    Note: the `.pem` shared with you are different for production and sandbox environments.

    - `X-FLYNN-S-DATA` is the any sensitive data you want to send it to the server.
    This sensitive data is AES encrypted via 'Request Encrypted Key'.
    To test the encryption-decryption of the sensitive data shared between the client and the server - server is sending the original message (AES encrypted) in the response.
    Where encryption key is still the same what was sent in the request header.

### Auth Server(Theodore) Ping Calls

- `/health`: Auth server health check call

### Create Bulk E-invoice API Call

- `/enriched/einv/{flynn_version}/{irpProvider}/invoices`: Create Bulk E-invoice call

    - `X-FLYNN-S-REK` is the 'Request Encryption Key' (either 32, 64 or 128 char string).
    This key is a dynamic salt for AES encryption of the sensitive data sent either in the request body or in the header.
    The key itself is encrypted with RSA key shared by the server.
    Note: the `.pem` shared with you are different for production and sandbox environments.
    - `X-FLYNN-N-ORG-ID` is the organization id on behalf of which you are making the API call.
    Value for this header is fetched from Auth Server's login API call.
    - `X-FLYNN-N-USER-TOKEN` Jwt Token for the user's session.
    This header value is also provided by the Auth server via login API call.
    - `X-FLYNN-N-IRP-GSTIN` GSTIN for the E-invoice system user.
    This Gstin would be the Seller GSTIN in the E-invoices being sent in request.
    - `X-FLYNN-N-IRP-USERNAME` Username for the E-invoice system user.
    - `X-FLYNN-S-IRP-PWD` Encrypted password for the E-invoice system user.
    The password needs to be encrypted using the decrypted `X-FLYNN-S-REK` key being sent in the API call.

The payload for this API should be according to the format of E-Invoice system. Check the documentation [here](https://einv-apisandbox.nic.in/version1.02/generate-irn.html#overview).
Here's an example of Create Einvoice payload for single IRN to Enriched API

```json
{
  "payload": [
    {
      "Version": "1.1",
      "Irn": "",
      "TranDtls": {
        "TaxSch": "GST",
        "SupTyp": "B2B",
        "RegRev": "N",
        "EcmGstin": null
      },
      "DocDtls": {
        "Typ": "INV",
        "No": "<doc-number>",
        "Dt": "07/05/2020"
      },
      "SellerDtls": {
        "Gstin": "<seller-gstin>",
        "LglNm": "Acme Widgets Private Limited",
        "TrdNm": null,
        "Addr1": "2345",
        "Addr2": null,
        "Loc": "Uttar Pradesh",
        "Pin": "560002",
        "State": "Uttar Pradesh",
        "Ph": null,
        "Em": null
      },
      "BuyerDtls": {
        "Gstin": "<buyer-gstin>",
        "LglNm": "Long Term Enterprises LLP",
        "TrdNm": null,
        "Pos": "27",
        "Addr1": "1234",
        "Addr2": null,
        "Loc": "Pune",
        "Pin": "600001",
        "State": "Maharastra",
        "Ph": null,
        "Em": null
      },
      "ValDtls": {
        "AssVal": 10000,
        "TotInvVal": 1200,
        "SgstVal": 1200,
        "CgstVal": 0,
        "IgstVal": 0,
        "CesVal": 0,
        "StCesVal": 0,
        "CesNonAdVal": 0,
        "RndOffAmt": 0,
        "TotInvValFc": 12400
      },
      "DispDtls": null,
      "ShipDtls": null,
      "PayDtls": null,
      "RefDtls": null,
      "ExpDtls": null,
      "EwbDtls": null,
      "AddlDocDtls": null,
      "ItemList": [
        {
          "ItemNo": 1,
          "SlNo": "1",
          "PrdDesc": null,
          "IsServc": "N",
          "HsnCd": "1001",
          "UnitPrice": 100,
          "TotAmt": 10,
          "AssAmt": 10,
          "GstRt": 10,
          "TotItemVal": 10,
          "Barcde": null,
          "Qty": null,
          "FreeQty": null,
          "Unit": null,
          "Discount": null,
          "PreTaxVal": null,
          "SgstAmt": null,
          "CgstAmt": null,
          "IgstAmt": null,
          "CesRt": null,
          "CesAmt": null,
          "CesNonAdval": null,
          "IgstRt": null,
          "StateCesRt": null,
          "StateCesAmt": null,
          "StateCesNonAdvlAmt": null,
          "CesNonAdvlAmt": null,
          "OthChrg": null,
          "OrdLineRef": null,
          "OrgCntry": null,
          "PrdSlNo": null,
          "AttribDtls": null,
          "BchDtls": null
        }
      ]
    }
  ],
  "meta": {
    "generatePdf": "Y",
    "emailRecipientList": []
  }
}
```

- `/enriched/tasks/{flynn_version}/status/{ref_id}`: E-invoices' Creation Status check call
 Status API is part of Long Running Tasks' API suite of Flynn API Server. The response would give information on how
 many tasks are completed and how many are pending. A task would be creation of E-invoice in this case.

 ```json
{
  "data": {
    "task" : {
        "id": "123e4567-e89b-12d3-a456-426655440000",
        "jobs": {
            "total": 3,
            "processed": 1
        },
        "status": "processing",
        "last-updated": {
            "time": "1588150189",
            "message": "Processed '29AKLPM8755F1Z2' with error"
        }
    }
  }
}
```

- `/enriched/tasks/{flynn_version}/download/{ref_id}`: E-invoices' Download results call
Status API is part of Long Running Tasks' API suite of Flynn API Server. The response would be zipped response having all the
files requested in the initial create E-invoice calls.

### Create and Get Basic E-waybill API Call

The client library has test cases for two basic E-way Bill API calls. Note that authentication is called prior to calling these APIs.

- POST `/basic/ewb/{flynn_version}/gen-ewb`: Create Basic E-way Bill

    - `X-FLYNN-S-REK` is the 'Request Encryption Key' (either 32, 64 or 128 char string).
    This key is a dynamic salt for AES encryption of the sensitive data sent either in the request body or in the header.
    The key itself is encrypted with RSA key shared by the server.
    Note: the `.pem` shared with you are different for production and sandbox environments.
    - `X-FLYNN-N-ORG-ID` is the organization id on behalf of which you are making the API call.
    Value for this header is fetched from Auth Server's login API call.
    - `X-FLYNN-N-USER-TOKEN` Jwt Token for the user's session.
    This header value is also provided by the Auth server via login API call.
    - `X-FLYNN-N-EWB-GSTIN` GSTIN for the E-way bill system user.
    - `X-FLYNN-N-EWB-USERNAME` Username for the E-way bill system user.
    - `X-FLYNN-S-EWB-PWD` Encrypted password for the E-way bill system user.
    The password needs to be encrypted using the decrypted `X-FLYNN-S-REK` key being sent in the API call.
    - `X-FLYNN-N-EWB-GSP-CODE` Gsp-Code of your GSP. For Clayfin customers, it's value should be "clayfin".

The payload for this API should be according to the format of E-ewb system. Check the documentation [here](https://docs.ewaybillgst.gov.in/apidocs/version1.03/generate-eway-bill.html#overview).
Here's an example of Create Ewb payload

```json
{
  "actFromStateCode": 29,
  "actToStateCode": 29,
  "cessNonAdvolValue": 1.00,
  "cessValue": 0.00,
  "cgstValue": 336593.34,
  "dispatchFromGSTIN": "29AAAAA1303P1ZV",
  "dispatchFromTradeName": "ABC Traders",
  "docDate": "04/08/2020",
  "docNo": "G1596532198292",
  "docType": "INV",
  "fromAddr1": "2ND CROSS NO 59  19  A",
  "fromAddr2": "GROUND FLOOR OSBORNE ROAD",
  "fromGstin": "29AEKPV7203E1Z9",
  "fromPincode": 577217,
  "fromPlace": "FRAZER TOWN",
  "fromStateCode": 29,
  "fromTrdName": "welton",
  "igstValue": 0.00,
  "itemList": [
    {
      "productName": "Wheat",
      "productDesc": "Wheat",
      "hsnCode": 1001,
      "quantity": 4,
      "qtyUnit": "BOX",
      "cgstRate": 6.00,
      "sgstRate": 6.00,
      "igstRate": 0.00,
      "cessRate": 0.00,
      "cessAdvol": 0.00,
      "taxableAmount": 5609889.00
    }
  ],
  "otherValue": -1.00,
  "sgstValue": 336593.34,
  "shipToGSTIN": "29AAACW4202F1ZM",
  "shipToTradeName": "XYZ Traders",
  "subSupplyDesc": "Some Sub Supply type Desc",
  "subSupplyType": "1",
  "supplyType": "O",
  "toAddr1": "Shree Nilaya",
  "toAddr2": "Dasarahosahalli",
  "toGstin": "29AAACW4202F1ZM",
  "toPincode": 560008,
  "toPlace": "Beml Nagar",
  "toStateCode": 29,
  "toTrdName": "sthuthya",
  "totInvValue": 11556371.34,
  "totalValue": 5609889.00,
  "transDistance": "24",
  "transDocDate": "04/08/2020",
  "transDocNo": "",
  "transMode": "1",
  "transactionType": 1,
  "transporterId": "29AAACW6288M1ZH",
  "transporterName": "",
  "vehicleNo": "KA51AB1234",
  "vehicleType": "R"
}
```

sucessful response for the API would give a structure similar to this
```json
{
  "data": "{\"ewayBillNo\":\"101001728541\",\"ewayBillDate\":\"04/08/2020 02:41:00 AM\",\"validUpto\":\"05/08/2020 11:59:00 AM\",\"alert\":\"The distance between the given pincodes are not available in the system., Total invoice value is more than the sum of total assessible value and tax values, \"}"
}
```

- GET `/basic/ewb/{flynn_version}/{eway-bill-number}`: Get Basic E-way Bill

Headers for the call would be similar to Generate E-way Bill. The successful response of the API will give a similar response like this.

```json
{
  "data": "{\"actFromStateCode\":29,\"actToStateCode\":29,\"actualDist\":24,\"cessValue\":0.00,\"cgstValue\":336593.34,\"docDate\":\"04/08/2020\",\"docNo\":\"G1596532198292\",\"docType\":\"INV\",\"ewayBillDate\":\"04/08/2020 02:41:00 AM\",\"ewbNo\":101001728541,\"extendedTimes\":0,\"fromAddr1\":\"2ND CROSS NO 59  19  A\",\"fromAddr2\":\"GROUND FLOOR OSBORNE ROAD\",\"fromGstin\":\"29AEKPV7203E1Z9\",\"fromPincode\":577217,\"fromPlace\":\"FRAZER TOWN\",\"fromStateCode\":29,\"fromTrdName\":\"welton\",\"genMode\":\"API\",\"igstValue\":0.00,\"noValidDays\":1,\"rejectStatus\":\"N\",\"vehicleType\":\"R\",\"sgstValue\":336593.34,\"status\":\"ACT\",\"subSupplyType\":\"2\",\"supplyType\":\"O\",\"toAddr1\":\"Shree Nilaya\",\"toAddr2\":\"Dasarahosahalli\",\"toGstin\":\"29AAACW4202F1ZM\",\"toPincode\":560008,\"toPlace\":\"Beml Nagar\",\"toStateCode\":29,\"transactionType\":3,\"totalValue\":5609889.00,\"totInvValue\":11556371.34,\"otherValue\":-1.00,\"cessNonAdvolValue\":1.00,\"toTrdName\":\"sthuthya\",\"transporterId\":\"29AAACW6288M1ZH\",\"transporterName\":\"WELBURN CANDLES PRIVATE LIMITED\",\"userGstin\":\"29AEKPV7203E1Z9\",\"validUpto\":\"05/08/2020 11:59:00 AM\",\"itemList\":[{\"productDesc\":\"Wheat\",\"cessRate\":0.000,\"cgstRate\":6.000,\"hsnCode\":1001,\"igstRate\":0.000,\"productId\":\"0\",\"productName\":\"Wheat\",\"qtyUnit\":\"BOX\",\"quantity\":4.00,\"sgstRate\":6.000,\"taxableAmount\":5609889.00,\"itemNo\":1,\"cessNonAdvol\":0}],\"VehiclListDetails\":[{\"updMode\":\"API\",\"vehicleNo\":\"KA51AB1234\",\"fromPlace\":\"FRAZER TOWN\",\"fromState\":29,\"tripshtNo\":0,\"userGSTINTransin\":\"29AEKPV7203E1Z9\",\"enteredDate\":\"04/08/2020 02:41:00 AM\",\"transMode\":1,\"transDocNo\":\"\",\"transDocDate\":\"04/08/2020\",\"groupNo\":\"0\"}]}"
}
```

You can follow links for detailed documentation of the Enriched API Services [here](https://docs.enriched-api.vayana.com/)

### How to use ?

Added Test cases are meant to be used as a reference code for how to use the Client Library. Feel free to raise issues or suggestions on this on eas-dev@vayana.com.
